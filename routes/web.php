<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//--------------------STUDENTS ROUTES--------------------------

Route::get('/add-student', 'HomeController@getAddStudent');
Route::post('/add-student', 'HomeController@postAddStudent');
Route::get('/view-students', 'HomeController@getViewStudents');
Route::get('/view-student/{sida}', 'HomeController@getViewStudent');
Route::get('/student-report', 'HomeController@getStudentReport');
// UPDATE STUDENT
Route::get('/select-student', 'HomeController@getSelectStudent');
Route::get('/update-student/{sida}', 'HomeController@getUpdateStudent');
Route::post('/update-student', 'HomeController@postUpdateStudent');

//---------------------TIMETABLE ROUTES-------------------------------
Route::get('/add-timetable', 'HomeController@getAddTimetable');
Route::get('/view-timetable', 'HomeController@getViewTimetable');
Route::get('/view-timetable/{tid}', 'HomeController@getViewTimetables');
Route::post('/add-timetable', 'HomeController@getPostTimetable');
Route::get('/timetable-report', 'HomeController@getTimetableReport');
Route::get('/generate-pdf-timetable/{tid}', 'HomeController@getGenerateTimetablePdf');
//UPDATE TIMETABLE
Route::get('/select-timetable', 'HomeController@getSelectTimetable');
Route::get('/update-timetable/{tid}', 'HomeController@getUpdateTimetable');
Route::post('/update-timetable/{tid}', 'HomeController@postUpdateTimetable');

//-------------------------INVOICES ROUTES--------------------------------------
Route::get('/add-invoice', 'HomeController@getAddInvoice');
Route::get('/view-invoices', 'HomeController@getViewInvoices');
Route::get('/view-invoice/{invid}', 'HomeController@getViewInvoice');
Route::get('/view-cleared-inv', 'HomeController@getViewClearedInv');
Route::get('/view-uncleared-inv', 'HomeController@getViewUnclearedInv');
Route::post('/add-invoice', 'HomeController@getPostInvoice');
Route::get('/generate-pdf-inv/{invid}', 'HomeController@getGenerateInvoicePdf');
//UPDATE INVOICE
Route::get('/select-invoice', 'HomeController@getSelectInvoice');
Route::get('/update-invoice/{invid}', 'HomeController@getUpdateInvoice');
Route::post('/update-invoice/{invid}', 'HomeController@postUpdateInvoice');

//-------------------------RESULTS ROUTES---------------------------------------
Route::get('/add-result', 'HomeController@getAddResult');
Route::post('/add-result', 'HomeController@getPostResult');
Route::get('/view-results', 'HomeController@getViewResults');
Route::get('/result-report', 'HomeController@getResultReport');
Route::get('/view-result-plain/{rid}', 'HomeController@getViewResultPlain');
Route::get('/view-result/{rid}', 'HomeController@getViewResult');
Route::get('/generate-pdf/{rid}', 'HomeController@getGenerateResultPdf');
//UPDATE RESULT
Route::get('/select-result', 'HomeController@getSelectResult');
Route::get('/update-result/{rid}', 'HomeController@getUpdateResult');
Route::post('/update-result/{rid}', 'HomeController@postUpdateResult');




