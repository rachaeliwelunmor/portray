<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Student</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body result-text">
                        <p><strong>Name: </strong>{{$invoice->student['sFname']}} {{$invoice->student['sSname']}} {{$invoice->student['sOname']}}</p>
                        <p><strong>ID Number: </strong> {{$invoice->student['sid']}} </p>
                        <p><strong>Programme: </strong> {{$invoice->student['programme']}} </p>
                        <p><strong>Stream: </strong> {{$invoice->student['stream']}} </p>
                        <p><strong>Current Level: </strong> {{$invoice->student['sLevel']}} </p>

                        <h4 align="center"><u>STUDENT INVOICE</u></h4>

                        <table id="table" class="table table-hover" align="center">
                            <tr style="text-transform: uppercase;">
                                <th style="text-align: center; border: 1px solid black; padding: 10px;">Level</th>
                                <th style="text-align: center;  border: 1px solid black; padding: 10px;">Semester</th>
                                <th style="text-align: center;  border: 1px solid black; padding: 10px;">Fees payable (USD)</th>
                            </tr>

                            <tr>
                                <td style="text-align: center; border: 1px solid black; padding: 10px;">
                                    {{$invoice->iLevel}}
                                </td>
                                <td style="text-align: center; border: 1px solid black; padding: 10px;">
                                    {{$invoice->iSemester}} <br> (ARREARS)
                                </td>

                                <td style="text-align: center; border: 1px solid black; padding: 10px;">{{$invoice->iFeesPayable}}</td>

                            </tr>

                            <tr>
                                <td style="text-align: center; border: 1px solid black; padding: 10px;">
                                    {{$invoice->iCLevel}}
                                </td>
                                <td style="text-align: center; border: 1px solid black; padding: 10px;">
                                    {{$invoice->iCSemester}}
                                </td>

                                <td style="text-align: center; border: 1px solid black; padding: 10px;">{{$invoice->iCFeesPayable}}</td>

                            </tr>

                            <tr>
                                <td style="border: 1px solid black; padding: 10px;"> </td>
                                <td style="text-align: center; border: 1px solid black; padding: 10px;"> GRAND TOTAL</td>

                                <td style="text-align: center; border: 1px solid black; padding: 10px;">{{$invoice->iTotal}}</td>

                            </tr>

                            <tr>
                                <td style="border: 1px solid black; padding: 10px;"> </td>
                                <td style="text-align: center; border: 1px solid black; padding: 10px;"> STATUS </td>

                                <td style="text-align: center; border: 1px solid black; padding: 10px;"> {{$invoice->status}}</td>

                            </tr>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
