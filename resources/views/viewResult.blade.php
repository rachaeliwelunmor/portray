<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>View Result</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>



<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-2 div-for-nav">
            <p class="dashboardText"><strong>DASHBOARD</strong></p>
            <div>
                <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
            </div>

            <div class="side-nav col-md-12">
                <br/><br><ul class="nav"><strong>ADD</strong></ul>
                <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                <a href="select-student" role="button" class="linkColor"> <li>Update Student</li></a>
                <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                <a href="select-result" role="button" class="linkColor"> <li>Update Result</li></a>

                <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                <a href="view-invoices" role="button" class="linkColor"> <li>View Invoice</li></a>
                <a href="view-timetable" role="button" class="linkColor"> <li>View TimeTable</li></a>
                <a href="view-results" onclick="history.go(-1); return false;" role="button" class="linkColor"> <li>View Result</li></a>
                <br/><br><br/>
            </div>
        </div>

        <div class="col-md-10">
            <!-- Header -->
            <header>
                <div class="row">
                    <div class=" navHeader col-md-12">
                        <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                        </p>

                    </div>
                </div>
            </header>
            <div class="panel panel-default">
                <div class="panel-heading">Results</div>
                <div class="panel-body">

                        <h3 align="center"><strong><u>REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</u></strong></h3>
                        <div>
                            <p style="float: left; width: auto; display: inline-block;margin-right: 370px;">{{$result->student['sid']}} </p>
                            <p style="display: inline-block;">(PROVISIONAL TRANSCRIPT)</p>
                            <p style="display: inline-block; float: right;">8th February, 2017</p>
                        </div>
                        <hr style="background-color: darkred; height: 2px;">
                        <p align="center"><strong><u>{{$result->student['sFname']}} {{$result->student['sSname']}} {{$result->student['sOname']}}</u></strong> <br></p>

                        <p align="center" style="margin: 0px 50px;">
                            {{$result->student['sFname']}} was admitted into this university in October 2013 to pursue a 4-year course in <br> {{$result->student['programme']}}
                            {{$result->student['sFname']}} has taken the following examinations of this university.
                        </p>
                        <hr style="background-color: darkred; height: 2px;">

                        {{--LEVEL 100--}}
                        @if($result->rLevel1== null)
                            <table>

                            </table>
                        @else

                            <table class="table">
                                <tr>
                                    <p>
                                        a) LEVEL {{$result->rLevel1}} Examination in the following subjects: ({{$result->rSession1}})
                                    </p>



                                <tr>
                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester11}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode111}}:{{$result->rCourseName111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode112}}:{{$result->rCourseName112}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark112}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade112}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr112}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode113}}:{{$result->rCourseName113}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark113}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade113}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr113}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode114}}:{{$result->rCourseName114}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark114}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade114}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr114}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode115}}:{{$result->rCourseName115}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark115}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade115}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr115}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode116}}:{{$result->rCourseName116}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark116}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade116}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr116}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa11}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>

                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester12}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode121}}:{{$result->rCourseName121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode122}}:{{$result->rCourseName122}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark122}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade122}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr122}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode123}}:{{$result->rCourseName123}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark123}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade123}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr123}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode124}}:{{$result->rCourseName124}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark124}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade124}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr124}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode125}}:{{$result->rCourseName125}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark125}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade125}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr125}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode126}}:{{$result->rCourseName126}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark126}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade126}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr126}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa12}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>

                        @endif

                        {{--LEVEL 200--}}
                        @if($result->rLevel2 == null)
                            <table>

                            </table>
                        @else

                            <table style="page-break-after: always;" class="table">
                                <tr>
                                    <p>
                                        b) LEVEL {{$result->rLevel2}} Examination in the following subjects: ({{$result->rSession2}})
                                    </p>
                                </tr>
                                <tr>
                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester21}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode211}}:{{$result->rCourseName211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode212}}:{{$result->rCourseName212}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark212}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade212}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr212}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode213}}:{{$result->rCourseName213}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark213}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade213}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr213}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode214}}:{{$result->rCourseName214}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark214}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade214}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr214}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode215}}:{{$result->rCourseName215}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark215}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade215}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr215}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode216}}:{{$result->rCourseName216}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark216}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade216}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr216}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa21}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>

                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester22}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode221}}:{{$result->rCourseName221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode222}}:{{$result->rCourseName222}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark222}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade222}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr222}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode223}}:{{$result->rCourseCode223}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark223}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade223}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr223}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode224}}:{{$result->rCourseName224}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark224}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade224}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr224}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode225}}:{{$result->rCourseName225}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark225}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade225}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr225}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode226}}:{{$result->rCourseName226}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark226}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade226}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr226}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa22}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                            </table>
                        @endif

                        {{--LEVEL 300--}}
                        @if($result->rLevel3 == null)
                            <table>

                            </table>
                        @else
                            <table class="table">
                                <tr>
                                    <p>
                                        c) LEVEL {{$result->rLevel3}} Examination in the following subjects: ({{$result->rSession3}})
                                    </p>
                                </tr>
                                <tr>
                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester31}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode311}}:{{$result->rCourseName311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode312}}:{{$result->rCourseName312}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark312}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade312}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr312}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode313}}:{{$result->rCourseName313}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark313}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade313}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr313}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode314}}:{{$result->rCourseName314}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark314}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade314}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr314}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode315}}:{{$result->rCourseName215}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark315}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade315}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr315}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode316}}:{{$result->rCourseName316}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark316}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade316}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr316}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa31}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>

                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester32}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode321}}:{{$result->rCourseName321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode322}}:{{$result->rCourseName322}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark322}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade322}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr322}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode323}}:{{$result->rCourseCode323}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark323}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade323}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr323}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode324}}:{{$result->rCourseName324}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark324}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade324}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr324}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode325}}:{{$result->rCourseName325}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark325}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade325}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr325}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode326}}:{{$result->rCourseName326}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark326}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade326}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr326}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa32}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                            </table>
                        @endif

                        {{--LEVEL 400--}}
                        @if($result->rLevel4 == null)
                            <table>

                            </table>
                        @else

                            <table class="table">
                                <tr>
                                    <p>
                                        d) LEVEL {{$result->rLevel4}} Examination in the following subjects: ({{$result->rSession4}})
                                    </p>
                                </tr>
                                <tr>
                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester41}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode411}}:{{$result->rCourseName411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode412}}:{{$result->rCourseName412}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark412}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade412}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr412}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode413}}:{{$result->rCourseName413}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark413}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade413}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr413}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode414}}:{{$result->rCourseName414}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark414}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade414}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr414}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode415}}:{{$result->rCourseName415}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark415}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade415}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr415}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode416}}:{{$result->rCourseName416}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark416}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade416}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr416}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa41}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>

                                    <td class="col-md-6" style="border: none;">
                                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester42}}</span></u></strong>
                                        <table class="table">
                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <u>Course Title</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Mark</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Gr.</u>
                                                </td>
                                                <td style="border: none; background-color: white;">
                                                    <u>Cr.</u>
                                                </td>
                                            </tr>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode421}}:{{$result->rCourseName421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode422}}:{{$result->rCourseName422}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark422}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade422}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr422}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode423}}:{{$result->rCourseCode423}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark423}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade423}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr423}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode424}}:{{$result->rCourseName424}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark424}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade424}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr424}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode425}}:{{$result->rCourseName425}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark425}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade425}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr425}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->rCourseCode426}}:{{$result->rCourseName426}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->mark426}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->grade426}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$result->cr426}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border: none; background-color: white;">
                                                    <strong>Semester Weighted Average:</strong>
                                                </td>

                                                <td style="border: none; background-color: white;">
                                                    <strong>{{$result->rSwa42}}</strong>
                                                </td>

                                                <td style="border: none; background-color: white;"></td>
                                                <td style="border: none; background-color: white;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                            </table>
                        @endif
                        <p style="color: blue;" align="center"> Cummulative Weighted Average:- <span style="margin-left: 30px;">{{$result->rCwa}}</span></p>

                        <table class="table" style="margin-left: 20px;">
                            <tr>
                                <th style="border: none;"><u>Key :</u></th>
                                <th style="border: none;"><u>Percentage Range</u></th>
                                <th style="border: none;"><u>Course Grade Equivalent</u></th>
                                <th style="border: none;"><u>Classes of Degree</u></th>
                            </tr>

                            <tr>
                                <td style="border: none;"></td>
                                <td style="border: none;">
                                    <table class="table">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                70% and Above
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                60% - 69%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                50% - 59%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                40% - 49%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                Below 40%
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="border: none;">
                                    <table class="table" style="margin-left: 50px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                A
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                B
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                C
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                D
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                Fail
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="border: none;">
                                    <table class="table" >
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                First Class
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                Second Class (Upper Division)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                Second Class (Lower Division)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                Third Class
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    <a href="{{url('/generate-pdf/'.$result->rid)}}" class="btn btn-primary" role="button" style="float: right;">
                        Generate PDF
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>





