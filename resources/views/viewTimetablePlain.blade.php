<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Student</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                    <div align="center" style="line-height: 10px;">
                        <p style="margin-top: 3px;">REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</p>
                        <p><strong>{{$timetable->tSchool}}</strong></p>
                        <p><strong>{{$timetable->tProgramme}}</strong></p>
                        <p><strong>McCARTHY HILL CAMPUS</strong></p>
                        <p><strong>{{$timetable->tSemester}} TEACHING TIME TABLE</strong></p>
                        <p><strong>LEVEL {{$timetable->tLevel}}-{{$timetable->tStream}} STREAM</strong></p>
                    </div>
                    <table class="table" style="border: 2px solid black; color: black; margin-top: 10px; width: 100%;">

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">Day/weak</th>
                            <th style="border: 2px solid black">8.30AM - 11.15AM</th>
                            <th style="border: 2px solid black">11.15AM - <br>11.30AM</th>
                            <th style="border: 2px solid black">11.30AM - 2.15AM</th>
                            <th style="border: 2px solid black">2.15AM - <br>2.30AM </th>
                            <th style="border: 2px solid black">2.30AM - 5.15AM</th>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->monday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1mc}} <br> {{$timetable->course1mn}} <br> {{$timetable->course1ml}} <br> {{$timetable->course1mnv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">B </td>
                            <td style="border: 2px solid black">{{$timetable->course2mc}} <br> {{$timetable->course2mn}} <br> {{$timetable->course2ml}} <br> {{$timetable->course2mnv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">B </td>
                            <td style="border: 2px solid black">{{$timetable->course3mc}} <br> {{$timetable->course3mn}} <br> {{$timetable->course3ml}} <br> {{$timetable->course3mnv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->tuesday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1tc}} <br> {{$timetable->course1tn}} <br> {{$timetable->course1tl}} <br> {{$timetable->course1tv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">R</td>
                            <td style="border: 2px solid black">{{$timetable->course2tc}} <br> {{$timetable->course2tn}} <br> {{$timetable->course2tl}} <br> {{$timetable->course2tv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">R</td>
                            <td style="border: 2px solid black">{{$timetable->course3tc}} <br> {{$timetable->course3tn}} <br> {{$timetable->course3tl}} <br> {{$timetable->course3tv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->wednesday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1wc}} <br> {{$timetable->course1wn}} <br> {{$timetable->course1wl}} <br> {{$timetable->course1wv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">E</td>
                            <td style="border: 2px solid black">{{$timetable->course2wc}} <br> {{$timetable->course2wn}} <br> {{$timetable->course2wl}} <br> {{$timetable->course2wv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">E</td>
                            <td style="border: 2px solid black">{{$timetable->course3wc}} <br> {{$timetable->course3wn}} <br> {{$timetable->course3wl}} <br> {{$timetable->course3wv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->thursday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1thc}} <br> {{$timetable->course1thn}} <br> {{$timetable->course1thl}} <br> {{$timetable->course1thv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">A</td>
                            <td style="border: 2px solid black">{{$timetable->course2thc}} <br> {{$timetable->course2thn}} <br> {{$timetable->course2thl}} <br> {{$timetable->course2thv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">A</td>
                            <td style="border: 2px solid black">{{$timetable->course3thc}} <br> {{$timetable->course3thn}} <br> {{$timetable->course3thl}} <br> {{$timetable->course3thv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->friday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1fc}} <br> {{$timetable->course1fn}} <br> {{$timetable->course1fl}} <br> {{$timetable->course1fv}}</td>
                            <td style="border: 2px solid black; text-align: center;">K</td>
                            <td style="border: 2px solid black">{{$timetable->course2fc}} <br> {{$timetable->course2fn}} <br> {{$timetable->course2fl}} <br> {{$timetable->course2fv}}</td>
                            <td style="border: 2px solid black; text-align: center;">K</td>
                            <td style="border: 2px solid black">{{$timetable->course3fc}} <br> {{$timetable->course3fn}} <br> {{$timetable->course3fl}} <br> {{$timetable->course3fv}}</td>
                        </tr>
                    </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
