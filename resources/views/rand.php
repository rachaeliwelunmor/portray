<!-------------------------------VIEW INVOICES--------------------------------------------->
@foreach($invoices as $invoice)


<p><strong>Name: </strong>{{$invoice->student['sFname']}} {{$invoice->student['sSname']}} {{$invoice->student['sOname']}}</p>
<p><strong>ID Number: </strong> {{$invoice->student['sid']}} </p>
<p><strong>Programme: </strong> {{$invoice->student['programme']}} </p>
<p><strong>Stream: </strong> {{$invoice->student['stream']}} </p>
<p><strong>Current Level: </strong> {{$invoice->student['sLevel']}} </p>

<h4 align="center"><u>STUDENT INVOICE</u></h4>

<table id="table" class="table table-hover table-bordered" align="center">
    <tr style="text-transform: uppercase;">
        <th style="text-align: center;">Level</th>
        <th style="text-align: center;">Semester</th>
        <th style="text-align: center;">Fees payable (USD)</th>
    </tr>

    <tr>
        <td style="text-align: center;">
            {{$invoice->iLevel}}
        </td>
        <td style="text-align: center;">
            {{$invoice->iSemester}} <br> (ARREARS)
        </td>

        <td style="text-align: center;">{{$invoice->iFeesPayable}}</td>

    </tr>

    <tr>
        <td style="text-align: center;">
            {{$invoice->iCLevel}}
        </td>
        <td style="text-align: center;">
            {{$invoice->iCSemester}}
        </td>

        <td style="text-align: center;">{{$invoice->iCFeesPayable}}</td>

    </tr>

    <tr>
        <td> </td>
        <td style="text-align: center;"> GRAND TOTAL</td>

        <td style="text-align: center;">{{$invoice->iTotal}}</td>

    </tr>

    <tr>
        <td> </td>
        <td style="text-align: center;"> STATUS </td>

        <td style="text-align: center;"> {{$invoice->status}}</td>

    </tr>

</table>

@endforeach
<!-------------------------------VIEW INVOICES ENDS--------------------------------------------->

<!-------------------------------VIEW TIMETABLE--------------------------------------------->
@foreach($timetables as $timetable)

<div align="center" style="line-height: 10px;">
    <p style="margin-top: 3px;">REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</p>
    <p><strong>{{$timetable->tSchool}}</strong></p>
    <p><strong>{{$timetable->tProgramme}}</strong></p>
    <p><strong>McCARTHY HILL CAMPUS</strong></p>
    <p><strong>{{$timetable->tSemester}} TEACHING TIME TABLE</strong></p>
    <p><strong>LEVEL {{$timetable->tLevel}}-{{$timetable->tStream}} STREAM</strong></p>
</div>
<table class="table" style="border: 2px solid black; color: black; margin-top: 10px; width: 100%;">

    <tr style="border: 2px solid black">
        <th style="border: 2px solid black">Day/weak</th>
        <th style="border: 2px solid black">8.30AM - 11.15AM</th>
        <th style="border: 2px solid black">11.15AM - <br>11.30AM</th>
        <th style="border: 2px solid black">11.30AM - 2.15AM</th>
        <th style="border: 2px solid black">2.15AM - <br>2.30AM </th>
        <th style="border: 2px solid black">2.30AM - 5.15AM</th>
    </tr>

    <tr style="border: 2px solid black">
        <th style="border: 2px solid black">{{$timetable->monday}}</th>
        <td style="border: 2px solid black">{{$timetable->course1mc}} <br> {{$timetable->course1mn}} <br> {{$timetable->course1ml}} <br> {{$timetable->course1mnv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">B </td>
        <td style="border: 2px solid black">{{$timetable->course2mc}} <br> {{$timetable->course2mn}} <br> {{$timetable->course2ml}} <br> {{$timetable->course2mnv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">B </td>
        <td style="border: 2px solid black">{{$timetable->course3mc}} <br> {{$timetable->course3mn}} <br> {{$timetable->course3ml}} <br> {{$timetable->course3mnv}}</td>
    </tr>

    <tr style="border: 2px solid black">
        <th style="border: 2px solid black">{{$timetable->tuesday}}</th>
        <td style="border: 2px solid black">{{$timetable->course1tc}} <br> {{$timetable->course1tn}} <br> {{$timetable->course1tl}} <br> {{$timetable->course1tv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">R</td>
        <td style="border: 2px solid black">{{$timetable->course2tc}} <br> {{$timetable->course2tn}} <br> {{$timetable->course2tl}} <br> {{$timetable->course2tv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">R</td>
        <td style="border: 2px solid black">{{$timetable->course3tc}} <br> {{$timetable->course3tn}} <br> {{$timetable->course3tl}} <br> {{$timetable->course3tv}}</td>
    </tr>

    <tr style="border: 2px solid black">
        <th style="border: 2px solid black">{{$timetable->wednesday}}</th>
        <td style="border: 2px solid black">{{$timetable->course1wc}} <br> {{$timetable->course1wn}} <br> {{$timetable->course1wl}} <br> {{$timetable->course1wv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">E</td>
        <td style="border: 2px solid black">{{$timetable->course2wc}} <br> {{$timetable->course2wn}} <br> {{$timetable->course2wl}} <br> {{$timetable->course2wv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">E</td>
        <td style="border: 2px solid black">{{$timetable->course3wc}} <br> {{$timetable->course3wn}} <br> {{$timetable->course3wl}} <br> {{$timetable->course3wv}}</td>
    </tr>

    <tr style="border: 2px solid black">
        <th style="border: 2px solid black">{{$timetable->thursday}}</th>
        <td style="border: 2px solid black">{{$timetable->course1thc}} <br> {{$timetable->course1thn}} <br> {{$timetable->course1thl}} <br> {{$timetable->course1thv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">A</td>
        <td style="border: 2px solid black">{{$timetable->course2thc}} <br> {{$timetable->course2thn}} <br> {{$timetable->course2thl}} <br> {{$timetable->course2thv}}</td>
        <td style="border-bottom: 2px solid white; text-align: center;">A</td>
        <td style="border: 2px solid black">{{$timetable->course3thc}} <br> {{$timetable->course3thn}} <br> {{$timetable->course3thl}} <br> {{$timetable->course3thv}}</td>
    </tr>

    <tr style="border: 2px solid black">
        <th style="border: 2px solid black">{{$timetable->friday}}</th>
        <td style="border: 2px solid black">{{$timetable->course1fc}} <br> {{$timetable->course1fn}} <br> {{$timetable->course1fl}} <br> {{$timetable->course1fv}}</td>
        <td style="border: 2px solid black; text-align: center;">K</td>
        <td style="border: 2px solid black">{{$timetable->course2fc}} <br> {{$timetable->course2fn}} <br> {{$timetable->course2fl}} <br> {{$timetable->course2fv}}</td>
        <td style="border: 2px solid black; text-align: center;">K</td>
        <td style="border: 2px solid black">{{$timetable->course3fc}} <br> {{$timetable->course3fn}} <br> {{$timetable->course3fl}} <br> {{$timetable->course3fv}}</td>
    </tr>
</table>
@endforeach
<!-------------------------------VIEW TIMETABLE ENDS--------------------------------------------->

<!-------------------------------VIEW RESULT--------------------------------------------->
@foreach($results as $result)

<h3 align="center"><strong><u>REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</u></strong></h3>
<div>
    <p style="float: left; width: auto; display: inline-block;margin-right: 370px;">{{$result->student['sid']}} </p>
    <p style="display: inline-block;">(PROVISIONAL TRANSCRIPT)</p>
    <p style="display: inline-block; float: right;">8th February, 2017</p>
</div>
<hr style="background-color: darkred; height: 2px;">
<p align="center"><strong><u>{{$result->student['sFname']}} {{$result->student['sSname']}} {{$result->student['sOname']}}</u></strong> <br></p>

<p align="center" style="margin: 0px 50px;">
    {{$result->student['sFname']}} was admitted into this university in October 2013 to pursue a 4-year course in <br> {{$result->student['programme']}}
    {{$result->student['sFname']}} has taken the following examinations of this university.
</p>
<hr style="background-color: darkred; height: 2px;">

{{--LEVEL 100--}}
@if($result->rLevel1== null)
<table>

</table>
@else

<table class="table">
    <tr>
        <p>
            a) LEVEL {{$result->rLevel1}} Examination in the following subjects: ({{$result->rSession1}})
        </p>


    <tr>
        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester11}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->rCourseCode111}}:{{$result->rCourseName111}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark111}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade111}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr111}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode112}}:{{$result->rCourseName112}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark112}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade112}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr112}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode113}}:{{$result->rCourseName113}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark113}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade113}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr113}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode114}}:{{$result->rCourseName114}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark114}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade114}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr114}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode115}}:{{$result->rCourseName115}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark115}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade115}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr115}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode116}}:{{$result->rCourseName116}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark116}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade116}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr116}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa11}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>

        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester12}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td style="border: none; background-color: white;">
                                {{$result->rCourseCode121}}:{{$result->rCourseName121}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark121}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade121}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr121}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode122}}:{{$result->rCourseName122}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark122}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade122}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr122}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode123}}:{{$result->rCourseCode123}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark123}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade123}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr123}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode124}}:{{$result->rCourseName124}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark124}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade124}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr124}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode125}}:{{$result->rCourseName125}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark125}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade125}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr125}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode126}}:{{$result->rCourseName126}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark126}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade126}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr126}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa12}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>
    </tr>
</table>

@endif

{{--LEVEL 200--}}
@if($result->rLevel2 == null)
<table>

</table>
@else

<table class="table">
    <tr>
        <p>
            a) LEVEL {{$result->rLevel2}} Examination in the following subjects: ({{$result->rSession2}})
        </p>
    </tr>
    <tr>
        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester21}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->rCourseCode211}}:{{$result->rCourseName211}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark211}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade211}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr211}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode212}}:{{$result->rCourseName212}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark212}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade212}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr212}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode213}}:{{$result->rCourseName213}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark213}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade213}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr213}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode214}}:{{$result->rCourseName214}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark214}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade214}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr214}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode215}}:{{$result->rCourseName215}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark215}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade215}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr215}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode216}}:{{$result->rCourseName216}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark216}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade216}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr216}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa21}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>

        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester22}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td style="border: none; background-color: white;">
                                {{$result->rCourseCode221}}:{{$result->rCourseName221}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark221}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade221}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr221}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode222}}:{{$result->rCourseName222}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark222}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade222}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr222}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode223}}:{{$result->rCourseCode223}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark223}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade223}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr223}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode224}}:{{$result->rCourseName224}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark224}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade224}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr224}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode225}}:{{$result->rCourseName225}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark225}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade225}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr225}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode226}}:{{$result->rCourseName226}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark226}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade226}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr226}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa22}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>
    </tr>

</table>
@endif

{{--LEVEL 300--}}

@if($result->rLevel3 == null)
<table>

</table>
@else

<table class="table">
    <tr>
        <p>
            a) LEVEL {{$result->rLevel3}} Examination in the following subjects: ({{$result->rSession3}})
        </p>
    </tr>
    <tr>
        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester31}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->rCourseCode311}}:{{$result->rCourseName311}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark311}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade311}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr311}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode312}}:{{$result->rCourseName312}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark312}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade312}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr312}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode313}}:{{$result->rCourseName313}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark313}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade313}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr313}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode314}}:{{$result->rCourseName314}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark314}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade314}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr314}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode315}}:{{$result->rCourseName215}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark315}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade315}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr315}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode316}}:{{$result->rCourseName316}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark316}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade316}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr316}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa31}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>

        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester32}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td style="border: none; background-color: white;">
                                {{$result->rCourseCode321}}:{{$result->rCourseName321}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark321}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade321}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr321}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode322}}:{{$result->rCourseName322}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark322}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade322}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr322}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode323}}:{{$result->rCourseCode323}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark323}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade323}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr323}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode324}}:{{$result->rCourseName324}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark324}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade324}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr324}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode325}}:{{$result->rCourseName325}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark325}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade325}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr325}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode326}}:{{$result->rCourseName326}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark326}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade326}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr326}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa32}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>
    </tr>

</table>
@endif

{{--LEVEL 400--}}

@if($result->rLevel4 == null)
<table>

</table>
@else

<table class="table">
    <tr>
        <p>
            a) LEVEL {{$result->rLevel4}} Examination in the following subjects: ({{$result->rSession4}})
        </p>
    </tr>
    <tr>
        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester41}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->rCourseCode411}}:{{$result->rCourseName411}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark411}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade411}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr411}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode412}}:{{$result->rCourseName412}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark412}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade412}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr412}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode413}}:{{$result->rCourseName413}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark413}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade413}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr413}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode414}}:{{$result->rCourseName414}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark414}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade414}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr414}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode415}}:{{$result->rCourseName415}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark415}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade415}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr415}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode416}}:{{$result->rCourseName416}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark416}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade416}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr416}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa41}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>

        <td class="col-md-6" style="border: none;">
            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester42}}</span></u></strong>
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        <u>Course Title</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Mark</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Gr.</u>
                    </td>
                    <td style="border: none; background-color: white;">
                        <u>Cr.</u>
                    </td>
                </tr>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td style="border: none; background-color: white;">
                                {{$result->rCourseCode421}}:{{$result->rCourseName421}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->mark421}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->grade421}}
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="border: none; background-color: white;">
                    <table>
                        <tr>
                            <td>
                                {{$result->cr421}}
                            </td>
                        </tr>
                    </table>
                </td>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode422}}:{{$result->rCourseName422}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark422}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade422}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr422}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode423}}:{{$result->rCourseCode423}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark423}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade423}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr423}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode424}}:{{$result->rCourseName424}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark424}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade424}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr424}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode425}}:{{$result->rCourseName425}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark425}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade425}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr425}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->rCourseCode426}}:{{$result->rCourseName426}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->mark426}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->grade426}}
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border: none; background-color: white;">
                        <table>
                            <tr>
                                <td>
                                    {{$result->cr426}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; background-color: white;">
                        <strong>Semester Weighted Average:</strong>
                    </td>

                    <td style="border: none; background-color: white;">
                        <strong>{{$result->rSwa42}}</strong>
                    </td>

                    <td style="border: none; background-color: white;"></td>
                    <td style="border: none; background-color: white;"></td>
                </tr>

            </table>
        </td>
    </tr>

</table>
@endif
<p style="color: blue;" align="center"> Cummulative Weighted Average:- <span style="margin-left: 30px;">{{$result->rCwa}}</span></p>

<table class="table" style="margin-left: 20px;">
    <tr>
        <th style="border: none;"><u>Key :</u></th>
        <th style="border: none;"><u>Percentage Range</u></th>
        <th style="border: none;"><u>Course Grade Equivalent</u></th>
        <th style="border: none;"><u>Classes of Degree</u></th>
    </tr>

    <tr>
        <td style="border: none;"></td>
        <td style="border: none;">
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        70% and Above
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        60% - 69%
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        50% - 59%
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        40% - 49%
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Below 40%
                    </td>
                </tr>
            </table>
        </td>
        <td style="border: none;">
            <table class="table" style="margin-left: 50px;">
                <tr>
                    <td style="border: none; background-color: white;">
                        A
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        B
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        C
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        D
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Fail
                    </td>
                </tr>
            </table>
        </td>
        <td style="border: none;">
            <table class="table" >
                <tr>
                    <td style="border: none; background-color: white;">
                        First Class
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Second Class (Upper Division)
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Second Class (Lower Division)
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Third Class
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>
@endforeach
<!-------------------------------VIEW RESULT END--------------------------------------------->
<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>View Result</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>
<div class="container-fluid con-style">
    <div class="row">

        <div class="col-md-10 col-md-offset-1" style="margin-top: 2px;">
            <div class="panel panel-default">
                <div class="panel-body">


                    <h3 align="center"><strong><u>REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</u></strong></h3>
                    <div>
                        <p style="float: left; width: auto; display: inline-block;margin-right: 320px;">{{$result->student['sid']}} </p>
                        <p style="display: inline-block;">(PROVISIONAL TRANSCRIPT)</p>
                        <p style="display: inline-block; float: right;">8th February, 2017</p>
                    </div>
                    <hr style="background-color: darkred; height: 2px;">
                    <p align="center"><strong><u>{{$result->student['sFname']}} {{$result->student['sSname']}} {{$result->student['sOname']}}</u></strong> <br></p>
                    <p align="center" style="margin: 0px 50px;">
                        {{$result->student['sFname']}} was admitted into this university in October 2013 to pursue a 4-year course in <br> {{$result->student['programme']}}
                        {{$result->student['sFname']}} has taken the following examinations of this university.
                    </p>
                    <hr style="background-color: darkred; height: 2px;">

                    {{--LEVEL 100--}}
                    @if($result->rLevel1== null)
                    <div style="margin: 0; padding: 0;" class="col-md-12">
                        <table>

                        </table>
                    </div>
                    @else
                    <div class="col-md-12">
                        <p>
                            a) LEVEL {{$result->rLevel1}} Examination in the following subjects: ({{$result->rSession1}})
                        </p>

                        <table class="table result-text-uc">
                            <tr>
                                <td rowspan="1" style="border: none;">
                                    <table class="table">
                                        <tr>
                                            <strong><u><span class="sem">{{$result->rSemester11}}</span></u></strong>
                                        </tr>
                                    </table>
                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;" rowspan="1">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;" rowspan="1">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;" rowspan="1">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;" rowspan="1">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1" style="border: none; background-color: white; min-width: 70%;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode111}}:{{$result->rCourseName111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td rowspan="1" style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td rowspan="1" style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td rowspan="1" style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr111}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1" style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode112}}:{{$result->rCourseName112}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td rowspan="1" style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark112}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td rowspan="1" style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade112}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td rowspan="1" style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr112}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode113}}:{{$result->rCourseName113}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark113}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade113}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr113}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode114}}:{{$result->rCourseName114}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark114}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade114}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr114}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode115}}:{{$result->rCourseName115}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark115}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade115}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr115}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode116}}:{{$result->rCourseName116}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark116}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade116}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr116}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa11}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>
                                </td>

                                <td rowspan="1" style="border: none;">
                                    <table class="table">
                                        <tr>
                                            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester12}}</span></u></strong>
                                        </tr>
                                    </table>
                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode121}}:{{$result->rCourseName121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr121}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode122}}:{{$result->rCourseName122}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark122}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade122}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr122}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode123}}:{{$result->rCourseCode123}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark123}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade123}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr123}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode124}}:{{$result->rCourseName124}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark124}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade124}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr124}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode125}}:{{$result->rCourseName125}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark125}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade125}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr125}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode126}}:{{$result->rCourseName126}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark126}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade126}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr126}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa12}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                    @endif

                    {{--LEVEL 200--}}
                    @if($result->rLevel2 == null)
                    <div class="col-md-12">
                        <table>

                        </table>
                    </div>
                    @else
                    <div class="col-md-12">
                        <p>
                            a) LEVEL {{$result->rLevel2}} Examination in the following subjects: ({{$result->rSession2}})
                        </p>

                        <table class="table">
                            <tr>
                                <td rowspan="1" style="border: none;">
                                    <table>
                                        <tr>
                                            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester21}}</span></u></strong>
                                        </tr>
                                    </table>

                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode211}}:{{$result->rCourseName211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr211}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode212}}:{{$result->rCourseName212}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark212}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade212}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr212}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode213}}:{{$result->rCourseName213}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark213}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade213}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr213}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode214}}:{{$result->rCourseName214}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark214}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade214}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr214}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode215}}:{{$result->rCourseName215}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark215}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade215}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr215}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode216}}:{{$result->rCourseName216}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark216}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade216}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr216}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa21}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>

                                </td>

                                <td rowspan="1" style="border: none;">
                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester22}}</span></u></strong>
                                        </tr>
                                    </table>

                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode221}}:{{$result->rCourseName221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr221}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode222}}:{{$result->rCourseName222}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark222}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade222}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr222}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode223}}:{{$result->rCourseCode223}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark223}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade223}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr223}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode224}}:{{$result->rCourseName224}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark224}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade224}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr224}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode225}}:{{$result->rCourseName225}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark225}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade225}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr225}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode226}}:{{$result->rCourseName226}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark226}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade226}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr226}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa22}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    @endif

                    {{--LEVEL 300--}}
                    @if($result->rLevel3 == null)
                    <div class="col-md-12">
                        <table>

                        </table>
                    </div>
                    @else
                    <div class="col-md-12">
                        <p>
                            a) LEVEL {{$result->rLevel3}} Examination in the following subjects: ({{$result->rSession3}})
                        </p>

                        <table class="table">
                            <tr>
                                <td rowspan="1" style="border: none;">
                                    <table>
                                        <tr>
                                            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester31}}</span></u></strong>
                                        </tr>
                                    </table>
                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode311}}:{{$result->rCourseName311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr311}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode312}}:{{$result->rCourseName312}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark312}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade312}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr312}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode313}}:{{$result->rCourseName313}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark313}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade313}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr313}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode314}}:{{$result->rCourseName314}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark314}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade314}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr314}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode315}}:{{$result->rCourseName215}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark315}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade315}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr315}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode316}}:{{$result->rCourseName316}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark316}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade316}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr316}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa31}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>
                                </td>

                                <td rowspan="1" style="border: none;">
                                    <table>
                                        <tr>
                                            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester32}}</span></u></strong>
                                        </tr>
                                    </table>
                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode321}}:{{$result->rCourseName321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr321}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode322}}:{{$result->rCourseName322}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark322}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade322}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr322}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode323}}:{{$result->rCourseCode323}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark323}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade323}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr323}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode324}}:{{$result->rCourseName324}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark324}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade324}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr324}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode325}}:{{$result->rCourseName325}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark325}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade325}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr325}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode326}}:{{$result->rCourseName326}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark326}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade326}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr326}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa32}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    @endif

                    {{--LEVEL 400--}}
                    @if($result->rLevel4 == null)
                    <table>

                    </table>
                    @else
                    <div class="col-md-12">
                        <p>
                            a) LEVEL {{$result->rLevel4}} Examination in the following subjects: ({{$result->rSession4}})
                        </p>
                        <table class="table">
                            <tr>
                                <td rowspan="1" style="border: none;">
                                    <table>
                                        <tr>
                                            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester41}}</span></u></strong>
                                        </tr>
                                    </table>

                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode411}}:{{$result->rCourseName411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr411}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode412}}:{{$result->rCourseName412}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark412}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade412}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr412}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode413}}:{{$result->rCourseName413}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark413}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade413}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr413}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode414}}:{{$result->rCourseName414}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark414}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade414}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr414}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode415}}:{{$result->rCourseName415}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark415}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade415}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr415}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode416}}:{{$result->rCourseName416}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark416}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade416}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr416}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa41}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>
                                </td>

                                <td rowspan="1" style="border: none;">
                                    <table>
                                        <tr>
                                            <strong><u><span style="text-transform: uppercase;">{{$result->rSemester42}}</span></u></strong>
                                        </tr>
                                    </table>

                                    <table style="margin-top: 7px;">
                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <u>Course Title</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Mark</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Gr.</u>
                                            </td>
                                            <td style="border: none; background-color: white;">
                                                <u>Cr.</u>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td style="border: none; background-color: white;">
                                                            {{$result->rCourseCode421}}:{{$result->rCourseName421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr421}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode422}}:{{$result->rCourseName422}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark422}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade422}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr422}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode423}}:{{$result->rCourseCode423}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark423}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade423}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr423}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode424}}:{{$result->rCourseName424}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark424}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade424}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr424}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode425}}:{{$result->rCourseName425}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark425}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade425}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr425}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->rCourseCode426}}:{{$result->rCourseName426}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->mark426}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->grade426}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{$result->cr426}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="border: none; background-color: white;">
                                                <strong>Semester Weighted Average:</strong>
                                            </td>

                                            <td style="border: none; background-color: white;">
                                                <strong>{{$result->rSwa42}}</strong>
                                            </td>

                                            <td style="border: none; background-color: white;"></td>
                                            <td style="border: none; background-color: white;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    @endif

                    <p style="color: blue;" align="center"> Cummulative Weighted Average:- <span style="margin-left: 30px;">{{$result->rCwa}}</span></p>

                    <table class="table" style="margin-left: 20px;">
                        <tr>
                            <th style="border: none;"><u>Key :</u></th>
                            <th style="border: none;"><u>Percentage Range</u></th>
                            <th style="border: none;"><u>Course Grade Equivalent</u></th>
                            <th style="border: none;"><u>Classes of Degree</u></th>
                        </tr>

                        <tr>
                            <td style="border: none;"></td>
                            <td style="border: none;">
                                <table class="table">
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            70% and Above
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            60% - 69%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            50% - 59%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            40% - 49%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            Below 40%
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: none;">
                                <table class="table" style="margin-left: 50px;">
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            A
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            B
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            C
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            D
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            Fail
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border: none;">
                                <table class="table" >
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            First Class
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            Second Class (Upper Division)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            Second Class (Lower Division)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none; background-color: white;">
                                            Third Class
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

{{--LEVEL 100--}}
@if($result->rLevel1== null)
<div class="col-md-12">
    <table>

    </table>
</div>
@else
<div class="col-md-12">
    <p>
        a) LEVEL {{$result->rLevel1}} Examination in the following subjects: ({{$result->rSession1}})
    </p>

    <table class="table">
        <tr>
            <td rowspan="1" style="border: none;">
                <table>
                    <tr>
                        <strong><u><span class="sem">{{$result->rSemester11}}</span></u></strong>
                    </tr>
                </table>
                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;" rowspan="1">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;" rowspan="1">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;" rowspan="1">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;" rowspan="1">
                            <u>Cr.</u>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1" style="border: none; background-color: white; min-width: 70%;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode111}}:{{$result->rCourseName111}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td rowspan="1" style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark111}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td rowspan="1" style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade111}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td rowspan="1" style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr111}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1" style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode112}}:{{$result->rCourseName112}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td rowspan="1" style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark112}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td rowspan="1" style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade112}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td rowspan="1" style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr112}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode113}}:{{$result->rCourseName113}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark113}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade113}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr113}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode114}}:{{$result->rCourseName114}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark114}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade114}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr114}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode115}}:{{$result->rCourseName115}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark115}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade115}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr115}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode116}}:{{$result->rCourseName116}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark116}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade116}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr116}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa11}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>
            </td>

            <td rowspan="1" style="border: none;">
                <table>
                    <tr>
                        <strong><u><span class="sem">{{$result->rSemester12}}</span></u></strong>
                    </tr>
                </table>
                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Cr.</u>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td style="border: none; background-color: white;">
                                        {{$result->rCourseCode121}}:{{$result->rCourseName121}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark121}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade121}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr121}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode122}}:{{$result->rCourseName122}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark122}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade122}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr122}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode123}}:{{$result->rCourseCode123}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark123}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade123}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr123}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode124}}:{{$result->rCourseName124}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark124}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade124}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr124}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode125}}:{{$result->rCourseName125}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark125}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade125}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr125}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode126}}:{{$result->rCourseName126}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark126}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade126}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr126}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa12}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

@endif

{{--LEVEL 200--}}
@if($result->rLevel2 == null)
<div class="col-md-12">
    <table>

    </table>
</div>
@else
<div class="col-md-12">
    <p>
        a) LEVEL {{$result->rLevel2}} Examination in the following subjects: ({{$result->rSession2}})
    </p>

    <table class="table">
        <tr>
            <td rowspan="1" style="border: none;">
                <table>
                    <tr>
                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester21}}</span></u></strong>
                    </tr>
                </table>

                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Cr.</u>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode211}}:{{$result->rCourseName211}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark211}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade211}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr211}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode212}}:{{$result->rCourseName212}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark212}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade212}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr212}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode213}}:{{$result->rCourseName213}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark213}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade213}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr213}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode214}}:{{$result->rCourseName214}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark214}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade214}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr214}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode215}}:{{$result->rCourseName215}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark215}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade215}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr215}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode216}}:{{$result->rCourseName216}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark216}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade216}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr216}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa21}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>

            </td>

            <td rowspan="1" style="border: none;">
                <table style="margin-top: 7px;">
                    <tr>
                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester22}}</span></u></strong>
                    </tr>
                </table>

                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Cr.</u>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td style="border: none; background-color: white;">
                                        {{$result->rCourseCode221}}:{{$result->rCourseName221}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark221}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade221}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr221}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode222}}:{{$result->rCourseName222}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark222}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade222}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr222}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode223}}:{{$result->rCourseCode223}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark223}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade223}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr223}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode224}}:{{$result->rCourseName224}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark224}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade224}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr224}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode225}}:{{$result->rCourseName225}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark225}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade225}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr225}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode226}}:{{$result->rCourseName226}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark226}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade226}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr226}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa22}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
@endif

{{--LEVEL 300--}}
@if($result->rLevel3 == null)
<div class="col-md-12">
    <table>

    </table>
</div>
@else
<div class="col-md-12">
    <p>
        a) LEVEL {{$result->rLevel3}} Examination in the following subjects: ({{$result->rSession3}})
    </p>

    <table class="table">
        <tr>
            <td rowspan="1" style="border: none;">
                <table>
                    <tr>
                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester31}}</span></u></strong>
                    </tr>
                </table>
                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Cr.</u>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode311}}:{{$result->rCourseName311}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark311}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade311}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr311}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode312}}:{{$result->rCourseName312}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark312}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade312}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr312}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode313}}:{{$result->rCourseName313}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark313}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade313}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr313}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode314}}:{{$result->rCourseName314}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark314}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade314}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr314}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode315}}:{{$result->rCourseName215}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark315}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade315}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr315}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode316}}:{{$result->rCourseName316}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark316}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade316}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr316}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa31}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>
            </td>

            <td rowspan="1" style="border: none;">
                <table>
                    <tr>
                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester32}}</span></u></strong>
                    </tr>
                </table>
                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Cr.</u>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td style="border: none; background-color: white;">
                                        {{$result->rCourseCode321}}:{{$result->rCourseName321}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark321}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade321}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr321}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode322}}:{{$result->rCourseName322}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark322}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade322}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr322}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode323}}:{{$result->rCourseCode323}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark323}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade323}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr323}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode324}}:{{$result->rCourseName324}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark324}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade324}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr324}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode325}}:{{$result->rCourseName325}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark325}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade325}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr325}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode326}}:{{$result->rCourseName326}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark326}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade326}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr326}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa32}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
@endif

{{--LEVEL 400--}}
@if($result->rLevel4 == null)
<table>

</table>
@else
<div class="col-md-12">
    <p>
        a) LEVEL {{$result->rLevel4}} Examination in the following subjects: ({{$result->rSession4}})
    </p>
    <table class="table">
        <tr>
            <td rowspan="1" style="border: none;">
                <table>
                    <tr>
                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester41}}</span></u></strong>
                    </tr>
                </table>

                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Cr.</u>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode411}}:{{$result->rCourseName411}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark411}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade411}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr411}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode412}}:{{$result->rCourseName412}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark412}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade412}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr412}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode413}}:{{$result->rCourseName413}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark413}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade413}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr413}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode414}}:{{$result->rCourseName414}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark414}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade414}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr414}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode415}}:{{$result->rCourseName415}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark415}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade415}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr415}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode416}}:{{$result->rCourseName416}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark416}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade416}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr416}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa41}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>
            </td>

            <td rowspan="1" style="border: none;">
                <table>
                    <tr>
                        <strong><u><span style="text-transform: uppercase;">{{$result->rSemester42}}</span></u></strong>
                    </tr>
                </table>

                <table style="margin-top: 7px;">
                    <tr>
                        <td style="border: none; background-color: white;">
                            <u>Course Title</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Mark</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Gr.</u>
                        </td>
                        <td style="border: none; background-color: white;">
                            <u>Cr.</u>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td style="border: none; background-color: white;">
                                        {{$result->rCourseCode421}}:{{$result->rCourseName421}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark421}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade421}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr421}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode422}}:{{$result->rCourseName422}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark422}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade422}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr422}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode423}}:{{$result->rCourseCode423}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark423}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade423}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr423}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode424}}:{{$result->rCourseName424}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark424}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade424}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr424}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode425}}:{{$result->rCourseName425}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark425}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade425}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr425}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->rCourseCode426}}:{{$result->rCourseName426}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->mark426}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->grade426}}
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="border: none; background-color: white;">
                            <table>
                                <tr>
                                    <td>
                                        {{$result->cr426}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none; background-color: white;">
                            <strong>Semester Weighted Average:</strong>
                        </td>

                        <td style="border: none; background-color: white;">
                            <strong>{{$result->rSwa42}}</strong>
                        </td>

                        <td style="border: none; background-color: white;"></td>
                        <td style="border: none; background-color: white;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
@endif

<p style="color: blue;" align="center"> Cummulative Weighted Average:- <span style="margin-left: 30px;">{{$result->rCwa}}</span></p>

<table class="table" style="margin-left: 20px;">
    <tr>
        <th style="border: none;"><u>Key :</u></th>
        <th style="border: none;"><u>Percentage Range</u></th>
        <th style="border: none;"><u>Course Grade Equivalent</u></th>
        <th style="border: none;"><u>Classes of Degree</u></th>
    </tr>

    <tr>
        <td style="border: none;"></td>
        <td style="border: none;">
            <table class="table">
                <tr>
                    <td style="border: none; background-color: white;">
                        70% and Above
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        60% - 69%
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        50% - 59%
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        40% - 49%
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Below 40%
                    </td>
                </tr>
            </table>
        </td>
        <td style="border: none;">
            <table class="table" style="margin-left: 50px;">
                <tr>
                    <td style="border: none; background-color: white;">
                        A
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        B
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        C
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        D
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Fail
                    </td>
                </tr>
            </table>
        </td>
        <td style="border: none;">
            <table class="table" >
                <tr>
                    <td style="border: none; background-color: white;">
                        First Class
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Second Class (Upper Division)
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Second Class (Lower Division)
                    </td>
                </tr>
                <tr>
                    <td style="border: none; background-color: white;">
                        Third Class
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>