<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Update Result</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-2 div-for-nav">
            <p class="dashboardText"><strong>DASHBOARD</strong></p>
            <div>
                <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
            </div>

            <div class="side-nav col-md-12">
                <br/><br><ul class="nav"><strong>ADD</strong></ul>
                <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                <a href="select-student" role="button" class="linkColor"> <li>Update Student</li></a>
                <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                <a href="select-result" onclick="history.go(-1); return false;" role="button" class="linkColor"> <li>Update Result</li></a>

                <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                <a href="view-invoices" role="button" class="linkColor"> <li>View Invoice</li></a>
                <a href="view-timetable" role="button" class="linkColor"> <li>View TimeTable</li></a>
                <a href="view-results" role="button" class="linkColor"> <li>View Result</li></a>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>
                <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/>

            </div>
        </div>

        <div class="col-md-10">
            <!-- Header -->
            <header>
                <div class="row">
                    <div class=" navHeader col-md-12">
                        <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                        </p>

                    </div>
                </div>
            </header>

            <div class="panel panel-default">
                <div class="panel-heading"> Update Result </div>

                @include('notification')

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/update-result/'. $result->rid )  }}">
                        {{ csrf_field() }}

                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    <div class="form-group{{ $errors->has('sida') ? ' has-error' : '' }}">
                                        <label for="sida" class="col-md-3 control-label">Student ID</label>

                                        <div class="col-md-9">
                                            <input id="sida" type="text" class="form-control" name="sida" value="{{ $result->sida}}" required autofocus>

                                            @if ($errors->has('sida'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('sida') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 100%" colspan="2">
                                    {{--Level 100--}}
                                    <div class="form-group{{ $errors->has('rLevel1') ? ' has-error' : '' }}">
                                        <label for="rLevel1" class="col-md-3 control-label">Level</label>

                                        <div class="col-md-9">
                                            <select id="rLevel1" name="rLevel1" class="form-control"  value="{{ $result->rLevel1 }}"  autofocus>

                                                @if ($result->rLevel1 == "100")
                                                <option disabled="disabled">Select a Level</option>
                                                <option value="100">100</option>
                                                    @else
                                                    <option disabled="disabled" selected = "selected">Select a Level</option>
                                                        <option value="100">100</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rLevel1'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rLevel1') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    <div class="form-group{{ $errors->has('rSession1') ? ' has-error' : '' }}">
                                        <label for="rSession1" class="col-md-3 control-label">Session</label>

                                        <div class="col-md-9">
                                            <input id="rSession1" type="text" class="form-control" name="rSession1" value="{{ $result->rSession1}}" autofocus>

                                            @if ($errors->has('rSession1'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSession1') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    {{--First semester--}}
                                    <div class="form-group{{ $errors->has('rSemester11') ? ' has-error' : '' }}">
                                        <label for="rSemester11" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester11" name="rSemester11" class="form-control"  value="{{ $result->rSemester11 }}" autofocus>


                                                @if ($result->rSemester11 == "First Semester")
                                                <option disabled="disabled">Select a Semester</option>
                                                <option value="First Semester">First Semester</option>
                                                    @else
                                                    <option disabled="disabled" selected="selected">Select a Semester</option>
                                                    <option value="First Semester">First Semester</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rSemester11'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester11') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td>
                                {{--Second semester--}}
                                    <div class="form-group{{ $errors->has('rSemester12') ? ' has-error' : '' }}">
                                        <label for="rSemester12" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester12" name="rSemester12" class="form-control"  value="{{  $result->rSemester12}}" autofocus>


                                                @if ($result->rSemester12 == "Second Semester")
                                                <option disabled="disabled" >Select a Semester</option>
                                                <option value="Second Semester">Second Semester</option>
                                                    @else
                                                    <option disabled="disabled" selected="selected">Select a Semester</option>
                                                    <option value="Second Semester">Second Semester</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rSemester12'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester12') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div></td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode111') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode111" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode111" type="text" class="form-control" name="rCourseCode111" value="{{  $result->rCourseCode111 }}" autofocus>

                                                        @if ($errors->has('rCourseCode111'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode111') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName111') ? ' has-error' : '' }}">
                                                    <label for="rCourseName111" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName111" type="text" class="form-control" name="rCourseName111" value="{{$result->rCourseName111}}" autofocus>

                                                        @if ($errors->has('rCourseName111'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName111') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark111') ? ' has-error' : '' }}">
                                                    <label for="mark111" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark111" type="text" class="form-control" name="mark111" value="{{ $result->mark111 }}" autofocus>

                                                        @if ($errors->has('mark111'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark111') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade111') ? ' has-error' : '' }}">
                                                    <label for="grade111" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade111" type="text" class="form-control" name="grade111" value="{{ $result->grade111 }}" autofocus>

                                                        @if ($errors->has('grade111'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade111') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr111') ? ' has-error' : '' }}">
                                                    <label for="cr111" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr111" type="text" class="form-control" name="cr111" value="{{ $result->cr111 }}" autofocus>

                                                        @if ($errors->has('cr111'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr111') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode112') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode112" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode112" type="text" class="form-control" name="rCourseCode112" value="{{ $result->rCourseCode112 }}" autofocus>

                                                        @if ($errors->has('rCourseCode112'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode112') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName112') ? ' has-error' : '' }}">
                                                    <label for="rCourseName112" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName112" type="text" class="form-control" name="rCourseName112" value="{{ $result->rCourseName112 }}" autofocus>

                                                        @if ($errors->has('rCourseName112'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName112') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark112') ? ' has-error' : '' }}">
                                                    <label for="mark112" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark112" type="text" class="form-control" name="mark112" value="{{ $result->mark112 }}" autofocus>

                                                        @if ($errors->has('mark112'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark112') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade112') ? ' has-error' : '' }}">
                                                    <label for="grade112" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade112" type="text" class="form-control" name="grade112" value="{{ $result->grade112 }}" autofocus>

                                                        @if ($errors->has('grade112'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade112') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr112') ? ' has-error' : '' }}">
                                                    <label for="cr112" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr112" type="text" class="form-control" name="cr112" value="{{ $result->cr112 }}" autofocus>

                                                        @if ($errors->has('cr112'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr112') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode113') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode113" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode113" type="text" class="form-control" name="rCourseCode113" value="{{ $result->rCourseCode113 }}" autofocus>

                                                        @if ($errors->has('rCourseCode113'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode113') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName113') ? ' has-error' : '' }}">
                                                    <label for="rCourseName113" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName113" type="text" class="form-control" name="rCourseName113" value="{{ $result->rCourseName113}}" autofocus>

                                                        @if ($errors->has('rCourseName113'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName113') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark113') ? ' has-error' : '' }}">
                                                    <label for="mark113" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark113" type="text" class="form-control" name="mark113" value="{{ $result->mark113 }}" autofocus>

                                                        @if ($errors->has('mark113'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark113') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade113') ? ' has-error' : '' }}">
                                                    <label for="grade113" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade113" type="text" class="form-control" name="grade113" value="{{ $result->grade113 }}" autofocus>

                                                        @if ($errors->has('grade113'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade113') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr113') ? ' has-error' : '' }}">
                                                    <label for="cr113" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr113" type="text" class="form-control" name="cr113" value="{{ $result->cr113 }}" autofocus>

                                                        @if ($errors->has('cr113'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr113') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode114') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode114" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode114" type="text" class="form-control" name="rCourseCode114" value="{{ $result->rCourseCode114 }}" autofocus>

                                                        @if ($errors->has('rCourseCode114'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode114') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName114') ? ' has-error' : '' }}">
                                                    <label for="rCourseName114" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName114" type="text" class="form-control" name="rCourseName114" value="{{ $result->rCourseName114}}" autofocus>

                                                        @if ($errors->has('rCourseName114'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName114') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark114') ? ' has-error' : '' }}">
                                                    <label for="mark114" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark114" type="text" class="form-control" name="mark114" value="{{ $result->mark114}}" autofocus>

                                                        @if ($errors->has('mark114'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark114') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade114') ? ' has-error' : '' }}">
                                                    <label for="grade114" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade114" type="text" class="form-control" name="grade114" value="{{ $result->grade114 }}" autofocus>

                                                        @if ($errors->has('grade114'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade114') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr114') ? ' has-error' : '' }}">
                                                    <label for="cr114" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr114" type="text" class="form-control" name="cr114" value="{{ $result->cr114 }}" autofocus>

                                                        @if ($errors->has('cr114'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr114') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode115') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode115" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode115" type="text" class="form-control" name="rCourseCode115" value="{{ $result->rCourseCode115 }}" autofocus>

                                                        @if ($errors->has('rCourseCode115'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode115') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName115') ? ' has-error' : '' }}">
                                                    <label for="rCourseName115" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName115" type="text" class="form-control" name="rCourseName115" value="{{ $result->rCourseName115 }}" autofocus>

                                                        @if ($errors->has('rCourseName115'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName115') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark115') ? ' has-error' : '' }}">
                                                    <label for="mark115" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark115" type="text" class="form-control" name="mark115" value="{{ $result->mark115 }}" autofocus>

                                                        @if ($errors->has('mark115'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark115') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade115') ? ' has-error' : '' }}">
                                                    <label for="grade115" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade115" type="text" class="form-control" name="grade115" value="{{ $result->grade115 }}" autofocus>

                                                        @if ($errors->has('grade115'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade115') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr115') ? ' has-error' : '' }}">
                                                    <label for="cr115" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr115" type="text" class="form-control" name="cr115" value="{{ $result->cr115 }}" autofocus>

                                                        @if ($errors->has('cr115'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr115') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode116') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode116" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode116" type="text" class="form-control" name="rCourseCode116" value="{{ $result->rCourseCode116 }}" autofocus>

                                                        @if ($errors->has('rCourseCode116'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode116') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName116') ? ' has-error' : '' }}">
                                                    <label for="rCourseName116" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName116" type="text" class="form-control" name="rCourseName116" value="{{ $result->rCourseName116 }}" autofocus>

                                                        @if ($errors->has('rCourseName116'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName116') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark116') ? ' has-error' : '' }}">
                                                    <label for="mark116" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark116" type="text" class="form-control" name="mark116" value="{{ $result->mark116 }}" autofocus>

                                                        @if ($errors->has('mark116'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark116') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade116') ? ' has-error' : '' }}">
                                                    <label for="grade116" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade116" type="text" class="form-control" name="grade116" value="{{ $result->grade116 }}" autofocus>

                                                        @if ($errors->has('grade116'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade116') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr116') ? ' has-error' : '' }}">
                                                    <label for="cr116" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr116" type="text" class="form-control" name="cr116" value="{{ $result->cr116 }}" autofocus>

                                                        @if ($errors->has('cr116'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr116') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for first semester 100 level--}}
                                                <div class="form-group{{ $errors->has('rSwa11') ? ' has-error' : '' }}">
                                                    <label for="rSwa11" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa11" type="text" class="form-control" name="rSwa11" value="{{ $result->rSwa11 }}" autofocus>

                                                        @if ($errors->has('rSwa11'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa11') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </td>

                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode121') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode121" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode121" type="text" class="form-control" name="rCourseCode121" value="{{ $result->rCourseCode121 }}" autofocus>

                                                        @if ($errors->has('rCourseCode121'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode121') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName121') ? ' has-error' : '' }}">
                                                    <label for="rCourseName121" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName121" type="text" class="form-control" name="rCourseName121" value="{{ $result->rCourseName121 }}" autofocus>

                                                        @if ($errors->has('rCourseName121'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName121') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark121') ? ' has-error' : '' }}">
                                                    <label for="mark121" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark121" type="text" class="form-control" name="mark121" value="{{ $result->mark121 }}" autofocus>

                                                        @if ($errors->has('mark121'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark121') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade121') ? ' has-error' : '' }}">
                                                    <label for="grade121" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade121" type="text" class="form-control" name="grade121" value="{{ $result->grade121 }}" autofocus>

                                                        @if ($errors->has('grade121'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade121') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr121') ? ' has-error' : '' }}">
                                                    <label for="cr121" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr121" type="text" class="form-control" name="cr121" value="{{ $result->cr121 }}" autofocus>

                                                        @if ($errors->has('cr121'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr121') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode122') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode122" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode122" type="text" class="form-control" name="rCourseCode122" value="{{ $result->rCourseCode122 }}" autofocus>

                                                        @if ($errors->has('rCourseCode122'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode122') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName122') ? ' has-error' : '' }}">
                                                    <label for="rCourseName122" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName122" type="text" class="form-control" name="rCourseName122" value="{{ $result->rCourseName122 }}" autofocus>

                                                        @if ($errors->has('rCourseName122'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName122') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark122') ? ' has-error' : '' }}">
                                                    <label for="mark122" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark122" type="text" class="form-control" name="mark122" value="{{ $result->mark122 }}" autofocus>

                                                        @if ($errors->has('mark122'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark122') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade122') ? ' has-error' : '' }}">
                                                    <label for="grade122" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade122" type="text" class="form-control" name="grade122" value="{{ $result->grade122 }}" autofocus>

                                                        @if ($errors->has('grade122'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade122') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr122') ? ' has-error' : '' }}">
                                                    <label for="cr122" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr122" type="text" class="form-control" name="cr122" value="{{ $result->cr122 }}" autofocus>

                                                        @if ($errors->has('cr122'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr122') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode123') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode123" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode123" type="text" class="form-control" name="rCourseCode123" value="{{ $result->rCourseCode123 }}" autofocus>

                                                        @if ($errors->has('rCourseCode123'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode123') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName123') ? ' has-error' : '' }}">
                                                    <label for="rCourseName123" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName123" type="text" class="form-control" name="rCourseName123" value="{{ $result->rCourseName123 }}" autofocus>

                                                        @if ($errors->has('rCourseName123'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName123') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark123') ? ' has-error' : '' }}">
                                                    <label for="mark123" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark123" type="text" class="form-control" name="mark123" value="{{ $result->mark123 }}" autofocus>

                                                        @if ($errors->has('mark123'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark123') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade123') ? ' has-error' : '' }}">
                                                    <label for="grade123" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade123" type="text" class="form-control" name="grade123" value="{{ $result->grade123 }}" autofocus>

                                                        @if ($errors->has('grade123'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade123') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr123') ? ' has-error' : '' }}">
                                                    <label for="cr123" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr123" type="text" class="form-control" name="cr123" value="{{ $result->cr123 }}" autofocus>

                                                        @if ($errors->has('cr123'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr123') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode124') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode124" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode124" type="text" class="form-control" name="rCourseCode124" value="{{ $result->rCourseCode124 }}" autofocus>

                                                        @if ($errors->has('rCourseCode124'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode124') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName124') ? ' has-error' : '' }}">
                                                    <label for="rCourseName124" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName124" type="text" class="form-control" name="rCourseName124" value="{{ $result->rCourseName124 }}" autofocus>

                                                        @if ($errors->has('rCourseName124'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName124') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark124') ? ' has-error' : '' }}">
                                                    <label for="mark124" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark124" type="text" class="form-control" name="mark124" value="{{ $result->mark124 }}" autofocus>

                                                        @if ($errors->has('mark124'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark124') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade124') ? ' has-error' : '' }}">
                                                    <label for="grade124" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade124" type="text" class="form-control" name="grade124" value="{{ $result->grade124 }}" autofocus>

                                                        @if ($errors->has('grade124'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade124') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr124') ? ' has-error' : '' }}">
                                                    <label for="cr124" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr124" type="text" class="form-control" name="cr124" value="{{ $result->cr124 }}" autofocus>

                                                        @if ($errors->has('cr124'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr124') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode125') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode125" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode125" type="text" class="form-control" name="rCourseCode125" value="{{ $result->rCourseCode125 }}" autofocus>

                                                        @if ($errors->has('rCourseCode125'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode125') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName125') ? ' has-error' : '' }}">
                                                    <label for="rCourseName125" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName125" type="text" class="form-control" name="rCourseName125" value="{{ $result->rCourseName125 }}" autofocus>

                                                        @if ($errors->has('rCourseName125'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName125') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark125') ? ' has-error' : '' }}">
                                                    <label for="mark125" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark125" type="text" class="form-control" name="mark125" value="{{ $result->mark125 }}" autofocus>

                                                        @if ($errors->has('mark125'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark125') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade125') ? ' has-error' : '' }}">
                                                    <label for="grade125" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade125" type="text" class="form-control" name="grade125" value="{{ $result->grade125 }}" autofocus>

                                                        @if ($errors->has('grade125'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade125') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr125') ? ' has-error' : '' }}">
                                                    <label for="cr125" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr125" type="text" class="form-control" name="cr125" value="{{ $result->cr125 }}" autofocus>

                                                        @if ($errors->has('cr125'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr125') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode126') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode126" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode126" type="text" class="form-control" name="rCourseCode126" value="{{ $result->rCourseCode126 }}" autofocus>

                                                        @if ($errors->has('rCourseCode126'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode126') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName126') ? ' has-error' : '' }}">
                                                    <label for="rCourseName126" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName126" type="text" class="form-control" name="rCourseName126" value="{{ $result->rCourseName126 }}" autofocus>

                                                        @if ($errors->has('rCourseName126'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName126') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark126') ? ' has-error' : '' }}">
                                                    <label for="mark126" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark126" type="text" class="form-control" name="mark126" value="{{ $result->mark126 }}" autofocus>

                                                        @if ($errors->has('mark126'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark126') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade126') ? ' has-error' : '' }}">
                                                    <label for="grade126" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade126" type="text" class="form-control" name="grade126" value="{{ $result->grade126 }}" autofocus>

                                                        @if ($errors->has('grade126'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade126') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr126') ? ' has-error' : '' }}">
                                                    <label for="cr126" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr126" type="text" class="form-control" name="cr126" value="{{ $result->cr126 }}" autofocus>

                                                        @if ($errors->has('cr126'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr126') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for second semester 100 level--}}
                                                <div class="form-group{{ $errors->has('rSwa12') ? ' has-error' : '' }}">
                                                    <label for="rSwa12" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa12" type="text" class="form-control" name="rSwa12" value="{{ $result->rSwa12 }}" autofocus>

                                                        @if ($errors->has('rSwa12'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa12') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            </tr>

                        </table>

                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    {{--Level 200--}}
                                    <div class="form-group{{ $errors->has('rLevel2') ? ' has-error' : '' }}">
                                        <label for="rLevel2" class="col-md-3 control-label">Level</label>

                                        <div class="col-md-9">
                                            <select id="rLevel2" name="rLevel2" class="form-control"  value="{{ $result->rLevel2 }}"  autofocus>

                                                @if ($result->rLevel2 == "200")
                                                    <option disabled="disabled">Select a Level</option>
                                                    <option value="200">200</option>
                                                @else
                                                    <option disabled="disabled" selected = "selected">Select a Level</option>
                                                    <option value="200">200</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rLevel2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rLevel2') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    <div class="form-group{{ $errors->has('rSession2') ? ' has-error' : '' }}">
                                        <label for="rSession2" class="col-md-3 control-label">Session</label>

                                        <div class="col-md-9">
                                            <input id="rSession2" type="text" class="form-control" name="rSession2" value="{{ $result->rSession2 }}" autofocus>

                                            @if ($errors->has('rSession2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSession2') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    {{--First semester--}}
                                    <div class="form-group{{ $errors->has('rSemester21') ? ' has-error' : '' }}">
                                        <label for="rSemester21" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester21" name="rSemester21" class="form-control"  value="{{ $result->rSemester21 }}" autofocus>

                                                @if ($result->rSemester21 == "First Semester")
                                                <option disabled="disabled">Select a Semester</option>
                                                <option value="First Semester">First Semester</option>
                                                    @else
                                                    <option disabled="disabled" selected = "selected">Select a Semester</option>
                                                        <option value="First Semester">First Semester</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rSemester21'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester21') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{--Second semester--}}
                                    <div class="form-group{{ $errors->has('rSemester22') ? ' has-error' : '' }}">
                                        <label for="rSemester22" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester22" name="rSemester22" class="form-control"  value="{{ $result->rSemester22 }}" autofocus>

                                                @if ($result->rSemester22 == "Second Semester")
                                                <option disabled="disabled">Select a Semester</option>
                                                <option value="Second Semester">Second Semester</option>
                                                    @else

                                                    <option disabled="disabled" selected ="selected">Select a Semester</option>
                                                    <option value="Second Semester">Second Semester</option>
                                                @endif

                                            </select>
                                            @if ($errors->has('rSemester22'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester22') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode211') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode211" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode211" type="text" class="form-control" name="rCourseCode211" value="{{ $result->rCourseCode211 }}" autofocus>

                                                        @if ($errors->has('rCourseCode211'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode211') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName211') ? ' has-error' : '' }}">
                                                    <label for="rCourseName211" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName211" type="text" class="form-control" name="rCourseName211" value="{{ $result->rCourseName211 }}" autofocus>

                                                        @if ($errors->has('rCourseName211'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName211') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark211') ? ' has-error' : '' }}">
                                                    <label for="mark211" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark211" type="text" class="form-control" name="mark211" value="{{ $result->mark211 }}" autofocus>

                                                        @if ($errors->has('mark211'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark211') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade211') ? ' has-error' : '' }}">
                                                    <label for="grade211" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade211" type="text" class="form-control" name="grade211" value="{{ $result->grade211 }}" autofocus>

                                                        @if ($errors->has('grade211'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade211') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr211') ? ' has-error' : '' }}">
                                                    <label for="cr211" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr211" type="text" class="form-control" name="cr211" value="{{ $result->cr211 }}" autofocus>

                                                        @if ($errors->has('cr211'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr211') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode212') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode212" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode212" type="text" class="form-control" name="rCourseCode212" value="{{ $result->rCourseCode212 }}" autofocus>

                                                        @if ($errors->has('rCourseCode212'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode212') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName212') ? ' has-error' : '' }}">
                                                    <label for="rCourseName212" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName212" type="text" class="form-control" name="rCourseName212" value="{{ $result->rCourseName212 }}" autofocus>

                                                        @if ($errors->has('rCourseName212'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName212') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark212') ? ' has-error' : '' }}">
                                                    <label for="mark212" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark212" type="text" class="form-control" name="mark212" value="{{ $result->mark212 }}" autofocus>

                                                        @if ($errors->has('mark212'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark212') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade212') ? ' has-error' : '' }}">
                                                    <label for="grade212" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade212" type="text" class="form-control" name="grade212" value="{{ $result->grade212 }}" autofocus>

                                                        @if ($errors->has('grade212'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade212') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr212') ? ' has-error' : '' }}">
                                                    <label for="cr212" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr212" type="text" class="form-control" name="cr212" value="{{ $result->cr212 }}" autofocus>

                                                        @if ($errors->has('cr212'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr212') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode213') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode213" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode213" type="text" class="form-control" name="rCourseCode213" value="{{ $result->rCourseCode213 }}" autofocus>

                                                        @if ($errors->has('rCourseCode213'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode213') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName213') ? ' has-error' : '' }}">
                                                    <label for="rCourseName213" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName213" type="text" class="form-control" name="rCourseName213" value="{{ $result->rCourseName213 }}" autofocus>

                                                        @if ($errors->has('rCourseName213'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName213') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark213') ? ' has-error' : '' }}">
                                                    <label for="mark213" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark213" type="text" class="form-control" name="mark213" value="{{ $result->mark213 }}" autofocus>

                                                        @if ($errors->has('mark213'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark213') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade213') ? ' has-error' : '' }}">
                                                    <label for="grade213" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade213" type="text" class="form-control" name="grade213" value="{{ $result->grade213 }}" autofocus>

                                                        @if ($errors->has('grade213'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade213') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr213') ? ' has-error' : '' }}">
                                                    <label for="cr213" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr213" type="text" class="form-control" name="cr213" value="{{ $result->cr213 }}" autofocus>

                                                        @if ($errors->has('cr213'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr213') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode214') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode214" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode214" type="text" class="form-control" name="rCourseCode214" value="{{ $result->rCourseCode214 }}" autofocus>

                                                        @if ($errors->has('rCourseCode214'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode214') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName214') ? ' has-error' : '' }}">
                                                    <label for="rCourseName214" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName214" type="text" class="form-control" name="rCourseName214" value="{{ $result-> rCourseName214 }}" autofocus>

                                                        @if ($errors->has('rCourseName214'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName214') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark214') ? ' has-error' : '' }}">
                                                    <label for="mark214" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark214" type="text" class="form-control" name="mark214" value="{{ $result->mark214 }}" autofocus>

                                                        @if ($errors->has('mark214'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark214') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade214') ? ' has-error' : '' }}">
                                                    <label for="grade214" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade214" type="text" class="form-control" name="grade214" value="{{ $result->grade214 }}" autofocus>

                                                        @if ($errors->has('grade214'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade214') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr214') ? ' has-error' : '' }}">
                                                    <label for="cr214" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr214" type="text" class="form-control" name="cr214" value="{{ $result->cr214 }}" autofocus>

                                                        @if ($errors->has('cr214'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr214') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode215') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode215" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode215" type="text" class="form-control" name="rCourseCode215" value="{{ $result->rCourseCode215 }}" autofocus>

                                                        @if ($errors->has('rCourseCode215'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode215') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName215') ? ' has-error' : '' }}">
                                                    <label for="rCourseName215" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName215" type="text" class="form-control" name="rCourseName215" value="{{ $result->rCourseName215 }}" autofocus>

                                                        @if ($errors->has('rCourseName215'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName215') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark215') ? ' has-error' : '' }}">
                                                    <label for="mark215" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark215" type="text" class="form-control" name="mark215" value="{{ $result->mark215 }}" autofocus>

                                                        @if ($errors->has('mark215'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark215') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade215') ? ' has-error' : '' }}">
                                                    <label for="grade215" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade215" type="text" class="form-control" name="grade215" value="{{ $result->grade215 }}" autofocus>

                                                        @if ($errors->has('grade215'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade215') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr215') ? ' has-error' : '' }}">
                                                    <label for="cr215" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr215" type="text" class="form-control" name="cr215" value="{{ $result->cr215 }}" autofocus>

                                                        @if ($errors->has('cr215'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr215') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode216') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode216" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode216" type="text" class="form-control" name="rCourseCode216" value="{{ $result->rCourseCode216 }}" autofocus>

                                                        @if ($errors->has('rCourseCode216'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode216') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName216') ? ' has-error' : '' }}">
                                                    <label for="rCourseName216" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName216" type="text" class="form-control" name="rCourseName216" value="{{ $result->rCourseName216 }}" autofocus>

                                                        @if ($errors->has('rCourseName216'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName216') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark216') ? ' has-error' : '' }}">
                                                    <label for="mark216" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark216" type="text" class="form-control" name="mark216" value="{{ $result->mark216 }}" autofocus>

                                                        @if ($errors->has('mark216'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark216') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade216') ? ' has-error' : '' }}">
                                                    <label for="grade216" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade216" type="text" class="form-control" name="grade216" value="{{ $result->grade216 }}" autofocus>

                                                        @if ($errors->has('grade216'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade216') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr216') ? ' has-error' : '' }}">
                                                    <label for="cr216" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr216" type="text" class="form-control" name="cr216" value="{{ $result->cr216 }}" autofocus>

                                                        @if ($errors->has('cr216'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr216') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for first semester 200 level--}}
                                                <div class="form-group{{ $errors->has('rSwa21') ? ' has-error' : '' }}">
                                                    <label for="rSwa21" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa21" type="text" class="form-control" name="rSwa21" value="{{ $result->rSwa21 }}" autofocus>

                                                        @if ($errors->has('rSwa21'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa21') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode221') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode221" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode221" type="text" class="form-control" name="rCourseCode221" value="{{ $result->rCourseCode221 }}" autofocus>

                                                        @if ($errors->has('rCourseCode221'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode221') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName221') ? ' has-error' : '' }}">
                                                    <label for="rCourseName221" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName221" type="text" class="form-control" name="rCourseName221" value="{{ $result->rCourseName221 }}" autofocus>

                                                        @if ($errors->has('rCourseName221'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName221') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark221') ? ' has-error' : '' }}">
                                                    <label for="mark221" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark221" type="text" class="form-control" name="mark221" value="{{ $result-> mark221 }}" autofocus>

                                                        @if ($errors->has('mark221'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark221') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade221') ? ' has-error' : '' }}">
                                                    <label for="grade221" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade221" type="text" class="form-control" name="grade221" value="{{ $result->grade221 }}" autofocus>

                                                        @if ($errors->has('grade221'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade221') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr221') ? ' has-error' : '' }}">
                                                    <label for="cr221" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr221" type="text" class="form-control" name="cr221" value="{{ $result->cr221 }}" autofocus>

                                                        @if ($errors->has('cr221'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr221') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode222') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode222" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode222" type="text" class="form-control" name="rCourseCode222" value="{{ $result->rCourseCode222 }}" autofocus>

                                                        @if ($errors->has('rCourseCode222'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode222') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName222') ? ' has-error' : '' }}">
                                                    <label for="rCourseName222" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName222" type="text" class="form-control" name="rCourseName222" value="{{ $result->rCourseName222 }}" autofocus>

                                                        @if ($errors->has('rCourseName222'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName222') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark222') ? ' has-error' : '' }}">
                                                    <label for="mark222" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark222" type="text" class="form-control" name="mark222" value="{{ $result->mark222 }}" autofocus>

                                                        @if ($errors->has('mark222'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark222') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade222') ? ' has-error' : '' }}">
                                                    <label for="grade222" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade222" type="text" class="form-control" name="grade222" value="{{ $result->grade222 }}" autofocus>

                                                        @if ($errors->has('grade222'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade222') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr222') ? ' has-error' : '' }}">
                                                    <label for="cr222" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr222" type="text" class="form-control" name="cr222" value="{{ $result->cr222 }}" autofocus>

                                                        @if ($errors->has('cr222'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr222') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode223') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode223" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode223" type="text" class="form-control" name="rCourseCode223" value="{{ $result->rCourseCode223 }}" autofocus>

                                                        @if ($errors->has('rCourseCode223'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode223') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName223') ? ' has-error' : '' }}">
                                                    <label for="rCourseName223" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName223" type="text" class="form-control" name="rCourseName223" value="{{ $result->rCourseName223 }}" autofocus>

                                                        @if ($errors->has('rCourseName223'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName223') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark223') ? ' has-error' : '' }}">
                                                    <label for="mark223" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark223" type="text" class="form-control" name="mark223" value="{{ $result->mark223 }}" autofocus>

                                                        @if ($errors->has('mark223'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark223') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade223') ? ' has-error' : '' }}">
                                                    <label for="grade223" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade223" type="text" class="form-control" name="grade223" value="{{ $result->grade223 }}" autofocus>

                                                        @if ($errors->has('grade223'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade223') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr223') ? ' has-error' : '' }}">
                                                    <label for="cr223" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr223" type="text" class="form-control" name="cr223" value="{{ $result->cr223 }}" autofocus>

                                                        @if ($errors->has('cr223'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr223') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode224') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode224" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode224" type="text" class="form-control" name="rCourseCode224" value="{{ $result->rCourseCode224 }}" autofocus>

                                                        @if ($errors->has('rCourseCode224'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode224') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName224') ? ' has-error' : '' }}">
                                                    <label for="rCourseName224" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName224" type="text" class="form-control" name="rCourseName224" value="{{ $result->rCourseName224 }}" autofocus>

                                                        @if ($errors->has('rCourseName224'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName224') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark224') ? ' has-error' : '' }}">
                                                    <label for="mark224" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark224" type="text" class="form-control" name="mark224" value="{{ $result->mark224 }}" autofocus>

                                                        @if ($errors->has('mark224'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark224') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade224') ? ' has-error' : '' }}">
                                                    <label for="grade224" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade224" type="text" class="form-control" name="grade224" value="{{ $result->grade224 }}" autofocus>

                                                        @if ($errors->has('grade224'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade224') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr224') ? ' has-error' : '' }}">
                                                    <label for="cr224" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr224" type="text" class="form-control" name="cr224" value="{{ $result->cr224 }}" autofocus>

                                                        @if ($errors->has('cr224'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr224') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode225') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode225" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode225" type="text" class="form-control" name="rCourseCode225" value="{{ $result->rCourseCode225 }}" autofocus>

                                                        @if ($errors->has('rCourseCode225'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode225') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName225') ? ' has-error' : '' }}">
                                                    <label for="rCourseName225" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName225" type="text" class="form-control" name="rCourseName225" value="{{ $result->rCourseName225 }}" autofocus>

                                                        @if ($errors->has('rCourseName225'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName225') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark225') ? ' has-error' : '' }}">
                                                    <label for="mark225" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark225" type="text" class="form-control" name="mark225" value="{{ $result->mark225 }}" autofocus>

                                                        @if ($errors->has('mark225'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark225') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade225') ? ' has-error' : '' }}">
                                                    <label for="grade225" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade225" type="text" class="form-control" name="grade225" value="{{ $result->grade225 }}" autofocus>

                                                        @if ($errors->has('grade225'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade225') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr225') ? ' has-error' : '' }}">
                                                    <label for="cr225" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr225" type="text" class="form-control" name="cr225" value="{{ $result->cr225 }}" autofocus>

                                                        @if ($errors->has('cr225'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr225') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode226') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode226" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode226" type="text" class="form-control" name="rCourseCode226" value="{{ $result->rCourseCode226 }}" autofocus>

                                                        @if ($errors->has('rCourseCode226'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode226') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName226') ? ' has-error' : '' }}">
                                                    <label for="rCourseName226" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName226" type="text" class="form-control" name="rCourseName226" value="{{ $result->rCourseName226 }}" autofocus>

                                                        @if ($errors->has('rCourseName226'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName226') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark226') ? ' has-error' : '' }}">
                                                    <label for="mark226" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark226" type="text" class="form-control" name="mark226" value="{{ $result->mark226 }}" autofocus>

                                                        @if ($errors->has('mark226'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark226') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade226') ? ' has-error' : '' }}">
                                                    <label for="grade226" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade226" type="text" class="form-control" name="grade226" value="{{ $result->grade226 }}" autofocus>

                                                        @if ($errors->has('grade226'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade226') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr226') ? ' has-error' : '' }}">
                                                    <label for="cr226" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr226" type="text" class="form-control" name="cr226" value="{{ $result->cr226 }}" autofocus>

                                                        @if ($errors->has('cr226'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr226') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for second semester 200 level--}}
                                                <div class="form-group{{ $errors->has('rSwa22') ? ' has-error' : '' }}">
                                                    <label for="rSwa22" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa22" type="text" class="form-control" name="rSwa22" value="{{ $result->rSwa22 }}" autofocus>

                                                        @if ($errors->has('rSwa22'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa22') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>

                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    {{--Level 300--}}
                                    <div class="form-group{{ $errors->has('rLevel3') ? ' has-error' : '' }}">
                                        <label for="rLevel3" class="col-md-3 control-label">Level</label>

                                        <div class="col-md-9">
                                            <select id="rLevel3" name="rLevel3" class="form-control"  value="{{ $result->rLevel3 }}"  autofocus>

                                                @if ($result->rLevel3 == "300")
                                                <option disabled="disabled">Select a Level</option>
                                                <option value="300">300</option>
                                                    @else
                                                    <option disabled="disabled" selected="selected">Select a Level</option>
                                                    <option value="300">300</option>
                                                    @endif
                                            </select>
                                            @if ($errors->has('rLevel3'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rLevel3') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    <div class="form-group{{ $errors->has('rSession3') ? ' has-error' : '' }}">
                                        <label for="rSession3" class="col-md-3 control-label">Session</label>

                                        <div class="col-md-9">
                                            <input id="rSession3" type="text" class="form-control" name="rSession3" value="{{ $result->rSession3 }}" autofocus>

                                            @if ($errors->has('rSession3'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSession3') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    {{--First semester--}}
                                    <div class="form-group{{ $errors->has('rSemester31') ? ' has-error' : '' }}">
                                        <label for="rSemester31" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester31" name="rSemester31" class="form-control"  value="{{ $result->rSemester31 }}" autofocus>


                                                @if ($result->rSemester31 == "First Semester")
                                                <option disabled="disabled">Select a Semester</option>
                                                <option value="First Semester">First Semester</option>
                                                    @else
                                                    <option disabled="disabled" selected="selected">Select a Semester</option>
                                                    <option value="First Semester">First Semester</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rSemester31'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester31') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{--Second semester--}}
                                    <div class="form-group{{ $errors->has('rSemester32') ? ' has-error' : '' }}">
                                        <label for="rSemester32" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester32" name="rSemester32" class="form-control"  value="{{ $result->rSemester32 }}" autofocus>


                                                @if ($result->rSemester32 == "Second Semester")
                                                <option disabled="disabled">Select a Semester</option>
                                                <option value="Second Semester">Second Semester</option>
                                                    @else
                                                    <option disabled="disabled" selected="selected">Select a Semester</option>
                                                    <option value="Second Semester">Second Semester</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rSemester32'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester32') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode311') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode311" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode311" type="text" class="form-control" name="rCourseCode311" value="{{ $result->rCourseCode311 }}" autofocus>

                                                        @if ($errors->has('rCourseCode311'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode311') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName311') ? ' has-error' : '' }}">
                                                    <label for="rCourseName311" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName311" type="text" class="form-control" name="rCourseName311" value="{{ $result->rCourseName311 }}" autofocus>

                                                        @if ($errors->has('rCourseName311'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName311') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark311') ? ' has-error' : '' }}">
                                                    <label for="mark311" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark311" type="text" class="form-control" name="mark311" value="{{ $result->mark311 }}" autofocus>

                                                        @if ($errors->has('mark311'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark311') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade311') ? ' has-error' : '' }}">
                                                    <label for="grade311" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade311" type="text" class="form-control" name="grade311" value="{{ $result->grade311 }}" autofocus>

                                                        @if ($errors->has('grade311'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade311') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr311') ? ' has-error' : '' }}">
                                                    <label for="cr311" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr311" type="text" class="form-control" name="cr311" value="{{ $result->cr311 }}" autofocus>

                                                        @if ($errors->has('cr311'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr311') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode312') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode312" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode312" type="text" class="form-control" name="rCourseCode312" value="{{ $result->rCourseCode312 }}" autofocus>

                                                        @if ($errors->has('rCourseCode312'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode312') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName312') ? ' has-error' : '' }}">
                                                    <label for="rCourseName312" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName312" type="text" class="form-control" name="rCourseName312" value="{{ $result->rCourseName312 }}" autofocus>

                                                        @if ($errors->has('rCourseName312'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName312') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark312') ? ' has-error' : '' }}">
                                                    <label for="mark312" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark312" type="text" class="form-control" name="mark312" value="{{ $result->mark312 }}" autofocus>

                                                        @if ($errors->has('mark312'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark312') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade312') ? ' has-error' : '' }}">
                                                    <label for="grade312" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade312" type="text" class="form-control" name="grade312" value="{{ $result->grade312 }}" autofocus>

                                                        @if ($errors->has('grade312'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade312') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr312') ? ' has-error' : '' }}">
                                                    <label for="cr312" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr312" type="text" class="form-control" name="cr312" value="{{ $result->cr312 }}" autofocus>

                                                        @if ($errors->has('cr312'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr312') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode313') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode313" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode313" type="text" class="form-control" name="rCourseCode313" value="{{ $result->rCourseCode313 }}" autofocus>

                                                        @if ($errors->has('rCourseCode313'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode313') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName313') ? ' has-error' : '' }}">
                                                    <label for="rCourseName313" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName313" type="text" class="form-control" name="rCourseName313" value="{{ $result->rCourseName313 }}" autofocus>

                                                        @if ($errors->has('rCourseName313'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName313') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark313') ? ' has-error' : '' }}">
                                                    <label for="mark313" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark313" type="text" class="form-control" name="mark313" value="{{ $result->mark313 }}" autofocus>

                                                        @if ($errors->has('mark313'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark313') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade313') ? ' has-error' : '' }}">
                                                    <label for="grade313" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade313" type="text" class="form-control" name="grade313" value="{{ $result->grade313 }}" autofocus>

                                                        @if ($errors->has('grade313'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade313') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr313') ? ' has-error' : '' }}">
                                                    <label for="cr313" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr313" type="text" class="form-control" name="cr313" value="{{ $result->cr313 }}" autofocus>

                                                        @if ($errors->has('cr313'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr313') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode314') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode314" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode314" type="text" class="form-control" name="rCourseCode314" value="{{ $result->rCourseCode314 }}" autofocus>

                                                        @if ($errors->has('rCourseCode314'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode314') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName314') ? ' has-error' : '' }}">
                                                    <label for="rCourseName314" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName314" type="text" class="form-control" name="rCourseName314" value="{{ $result->rCourseName314 }}" autofocus>

                                                        @if ($errors->has('rCourseName314'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName314') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark314') ? ' has-error' : '' }}">
                                                    <label for="mark314" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark314" type="text" class="form-control" name="mark314" value="{{ $result->mark314 }}" autofocus>

                                                        @if ($errors->has('mark314'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark314') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade314') ? ' has-error' : '' }}">
                                                    <label for="grade314" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade314" type="text" class="form-control" name="grade314" value="{{ $result->grade314 }}" autofocus>

                                                        @if ($errors->has('grade314'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade314') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr314') ? ' has-error' : '' }}">
                                                    <label for="cr314" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr314" type="text" class="form-control" name="cr314" value="{{ $result->cr314 }}" autofocus>

                                                        @if ($errors->has('cr314'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr314') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode315') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode315" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode315" type="text" class="form-control" name="rCourseCode315" value="{{ $result->rCourseCode315 }}" autofocus>

                                                        @if ($errors->has('rCourseCode315'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode315') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName315') ? ' has-error' : '' }}">
                                                    <label for="rCourseName315" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName315" type="text" class="form-control" name="rCourseName315" value="{{ $result->rCourseName315 }}" autofocus>

                                                        @if ($errors->has('rCourseName315'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName315') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark315') ? ' has-error' : '' }}">
                                                    <label for="mark315" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark315" type="text" class="form-control" name="mark315" value="{{ $result->mark315 }}" autofocus>

                                                        @if ($errors->has('mark315'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark315') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade315') ? ' has-error' : '' }}">
                                                    <label for="grade315" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade315" type="text" class="form-control" name="grade315" value="{{ $result->grade315 }}" autofocus>

                                                        @if ($errors->has('grade315'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade315') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr315') ? ' has-error' : '' }}">
                                                    <label for="cr315" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr315" type="text" class="form-control" name="cr315" value="{{ $result->cr315 }}" autofocus>

                                                        @if ($errors->has('cr315'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr315') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode316') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode316" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode316" type="text" class="form-control" name="rCourseCode316" value="{{ $result->rCourseCode316 }}" autofocus>

                                                        @if ($errors->has('rCourseCode316'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode316') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName316') ? ' has-error' : '' }}">
                                                    <label for="rCourseName316" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName316" type="text" class="form-control" name="rCourseName316" value="{{ $result->rCourseName316 }}" autofocus>

                                                        @if ($errors->has('rCourseName316'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName316') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark316') ? ' has-error' : '' }}">
                                                    <label for="mark316" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark316" type="text" class="form-control" name="mark316" value="{{ $result->mark316 }}" autofocus>

                                                        @if ($errors->has('mark316'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark316') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade316') ? ' has-error' : '' }}">
                                                    <label for="grade316" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade316" type="text" class="form-control" name="grade316" value="{{ $result->grade316 }}" autofocus>

                                                        @if ($errors->has('grade316'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade316') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr316') ? ' has-error' : '' }}">
                                                    <label for="cr316" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr316" type="text" class="form-control" name="cr316" value="{{ $result->cr316 }}" autofocus>

                                                        @if ($errors->has('cr316'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr316') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for first semester 300 level--}}
                                                <div class="form-group{{ $errors->has('rSwa31') ? ' has-error' : '' }}">
                                                    <label for="rSwa31" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa31" type="text" class="form-control" name="rSwa31" value="{{ $result->rSwa31 }}" autofocus>

                                                        @if ($errors->has('rSwa31'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa31') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode321') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode321" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode321" type="text" class="form-control" name="rCourseCode321" value="{{ $result->rCourseCode321 }}" autofocus>

                                                        @if ($errors->has('rCourseCode321'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode321') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName321') ? ' has-error' : '' }}">
                                                    <label for="rCourseName321" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName321" type="text" class="form-control" name="rCourseName321" value="{{ $result->rCourseName321 }}" autofocus>

                                                        @if ($errors->has('rCourseName321'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName321') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark321') ? ' has-error' : '' }}">
                                                    <label for="mark321" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark321" type="text" class="form-control" name="mark321" value="{{ $result->mark321 }}" autofocus>

                                                        @if ($errors->has('mark321'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark321') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade321') ? ' has-error' : '' }}">
                                                    <label for="grade321" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade321" type="text" class="form-control" name="grade321" value="{{ $result->grade321 }}" autofocus>

                                                        @if ($errors->has('grade321'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade321') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr321') ? ' has-error' : '' }}">
                                                    <label for="cr321" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr321" type="text" class="form-control" name="cr321" value="{{ $result->cr321 }}" autofocus>

                                                        @if ($errors->has('cr321'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr321') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode322') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode322" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode322" type="text" class="form-control" name="rCourseCode322" value="{{ $result->rCourseCode322 }}" autofocus>

                                                        @if ($errors->has('rCourseCode322'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode322') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName322') ? ' has-error' : '' }}">
                                                    <label for="rCourseName322" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName322" type="text" class="form-control" name="rCourseName322" value="{{ $result->rCourseName322 }}" autofocus>

                                                        @if ($errors->has('rCourseName322'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName322') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark322') ? ' has-error' : '' }}">
                                                    <label for="mark322" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark322" type="text" class="form-control" name="mark322" value="{{ $result->mark322 }}" autofocus>

                                                        @if ($errors->has('mark322'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark322') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade322') ? ' has-error' : '' }}">
                                                    <label for="grade322" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade322" type="text" class="form-control" name="grade322" value="{{ $result->grade322 }}" autofocus>

                                                        @if ($errors->has('grade322'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade322') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr322') ? ' has-error' : '' }}">
                                                    <label for="cr322" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr322" type="text" class="form-control" name="cr322" value="{{ $result->cr322 }}" autofocus>

                                                        @if ($errors->has('cr322'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr322') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode323') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode323" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode323" type="text" class="form-control" name="rCourseCode323" value="{{ $result->rCourseCode323 }}" autofocus>

                                                        @if ($errors->has('rCourseCode323'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode323') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName323') ? ' has-error' : '' }}">
                                                    <label for="rCourseName323" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName323" type="text" class="form-control" name="rCourseName323" value="{{ $result->rCourseName323 }}" autofocus>

                                                        @if ($errors->has('rCourseName323'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName323') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark323') ? ' has-error' : '' }}">
                                                    <label for="mark323" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark323" type="text" class="form-control" name="mark323" value="{{ $result->mark323 }}" autofocus>

                                                        @if ($errors->has('mark323'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark323') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade323') ? ' has-error' : '' }}">
                                                    <label for="grade323" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade323" type="text" class="form-control" name="grade323" value="{{ $result->grade323 }}" autofocus>

                                                        @if ($errors->has('grade323'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade323') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr323') ? ' has-error' : '' }}">
                                                    <label for="cr323" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr323" type="text" class="form-control" name="cr323" value="{{ $result->cr323 }}" autofocus>

                                                        @if ($errors->has('cr323'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr323') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode324') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode324" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode324" type="text" class="form-control" name="rCourseCode324" value="{{ $result->rCourseCode324 }}" autofocus>

                                                        @if ($errors->has('rCourseCode324'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode324') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName324') ? ' has-error' : '' }}">
                                                    <label for="rCourseName324" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName324" type="text" class="form-control" name="rCourseName324" value="{{ $result->rCourseName324 }}" autofocus>

                                                        @if ($errors->has('rCourseName324'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName324') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark324') ? ' has-error' : '' }}">
                                                    <label for="mark324" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark324" type="text" class="form-control" name="mark324" value="{{ $result->mark324 }}" autofocus>

                                                        @if ($errors->has('mark324'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark324') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade324') ? ' has-error' : '' }}">
                                                    <label for="grade324" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade324" type="text" class="form-control" name="grade324" value="{{ $result->grade324 }}" autofocus>

                                                        @if ($errors->has('grade324'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade324') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr324') ? ' has-error' : '' }}">
                                                    <label for="cr324" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr324" type="text" class="form-control" name="cr324" value="{{ $result->cr324 }}" autofocus>

                                                        @if ($errors->has('cr324'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr324') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode325') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode325" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode325" type="text" class="form-control" name="rCourseCode325" value="{{ $result->rCourseCode325 }}" autofocus>

                                                        @if ($errors->has('rCourseCode325'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode325') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName325') ? ' has-error' : '' }}">
                                                    <label for="rCourseName325" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName325" type="text" class="form-control" name="rCourseName325" value="{{ $result->rCourseName325 }}" autofocus>

                                                        @if ($errors->has('rCourseName325'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName325') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark325') ? ' has-error' : '' }}">
                                                    <label for="mark325" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark325" type="text" class="form-control" name="mark325" value="{{ $result->mark325 }}" autofocus>

                                                        @if ($errors->has('mark325'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark325') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade325') ? ' has-error' : '' }}">
                                                    <label for="grade325" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade325" type="text" class="form-control" name="grade325" value="{{ $result->grade325 }}" autofocus>

                                                        @if ($errors->has('grade325'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade325') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr325') ? ' has-error' : '' }}">
                                                    <label for="cr325" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr325" type="text" class="form-control" name="cr325" value="{{ $result->cr325 }}" autofocus>

                                                        @if ($errors->has('cr325'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr325') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode326') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode326" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode326" type="text" class="form-control" name="rCourseCode326" value="{{ $result->rCourseCode326 }}" autofocus>

                                                        @if ($errors->has('rCourseCode326'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode326') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName326') ? ' has-error' : '' }}">
                                                    <label for="rCourseName326" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName326" type="text" class="form-control" name="rCourseName326" value="{{ $result->rCourseName326 }}" autofocus>

                                                        @if ($errors->has('rCourseName326'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName326') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark326') ? ' has-error' : '' }}">
                                                    <label for="mark326" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark326" type="text" class="form-control" name="mark326" value="{{$result->mark326}}" autofocus>

                                                        @if ($errors->has('mark326'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark326') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade326') ? ' has-error' : '' }}">
                                                    <label for="grade326" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade326" type="text" class="form-control" name="grade326" value="{{ $result->grade326 }}" autofocus>

                                                        @if ($errors->has('grade326'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade326') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr326') ? ' has-error' : '' }}">
                                                    <label for="cr326" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr326" type="text" class="form-control" name="cr326" value="{{ $result->cr326 }}" autofocus>

                                                        @if ($errors->has('cr326'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr326') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for second semester 300 level--}}
                                                <div class="form-group{{ $errors->has('rSwa32') ? ' has-error' : '' }}">
                                                    <label for="rSwa32" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa32" type="text" class="form-control" name="rSwa32" value="{{ $result->rSwa32 }}" autofocus>

                                                        @if ($errors->has('rSwa32'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa32') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    {{--Level 400--}}
                                    <div class="form-group{{ $errors->has('rLevel4') ? ' has-error' : '' }}">
                                        <label for="rLevel4" class="col-md-3 control-label">Level</label>

                                        <div class="col-md-9">
                                            <select id="rLevel4" name="rLevel4" class="form-control"  value="{{ $result->rLevel4 }}"  autofocus>


                                                @if ($result->rLevel4 == "400")
                                                <option disabled="disabled" >Select a Level</option>
                                                <option value="400">400</option>
                                                    @else
                                                    <option disabled="disabled" selected="selected">Select a Level</option>
                                                    <option value="400">400</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rLevel4'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rLevel4') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    <div class="form-group{{ $errors->has('rSession4') ? ' has-error' : '' }}">
                                        <label for="rSession4" class="col-md-3 control-label">Session</label>

                                        <div class="col-md-9">
                                            <input id="rSession4" type="text" class="form-control" name="rSession4" value="{{ $result->rSession4 }}" autofocus>

                                            @if ($errors->has('rSession4'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSession4') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="form-group{{ $errors->has('rSemester41') ? ' has-error' : '' }}">
                                        <label for="rSemester41" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester41" name="rSemester41" class="form-control"  value="{{ $result->rSemester41 }}" autofocus>


                                                @if($result->rSemester41 == "First Semester")
                                                <option disabled="disabled" >Select a Semester</option>
                                                <option value="First Semester">First Semester</option>
                                                    @else
                                                    <option disabled="disabled" selected="selected">Select a Semester</option>
                                                    <option value="First Semester">First Semester</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rSemester41'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester41') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{--Second semester--}}
                                    <div class="form-group{{ $errors->has('rSemester42') ? ' has-error' : '' }}">
                                        <label for="rSemester42" class="col-md-4 control-label">Semester</label>

                                        <div class="col-md-6">
                                            <select id="rSemester42" name="rSemester42" class="form-control"  value="{{ $result->rSemester42 }}" autofocus>

                                                @if ($result->rSemester42 == "Second Semester")
                                                <option disabled="disabled" >Select a Semester</option>
                                                <option value="Second Semester">Second Semester</option>
                                                    @else
                                                    <option disabled="disabled" selected = "selected">Select a Semester</option>
                                                    <option value="Second Semester">Second Semester</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('rSemester42'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rSemester42') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode411') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode411" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode411" type="text" class="form-control" name="rCourseCode411" value="{{ $result->rCourseCode411 }}" autofocus>

                                                        @if ($errors->has('rCourseCode411'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode411') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName411') ? ' has-error' : '' }}">
                                                    <label for="rCourseName411" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName411" type="text" class="form-control" name="rCourseName411" value="{{ $result->rCourseName411 }}" autofocus>

                                                        @if ($errors->has('rCourseName411'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName411') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark411') ? ' has-error' : '' }}">
                                                    <label for="mark411" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark411" type="text" class="form-control" name="mark411" value="{{ $result->mark411 }}" autofocus>

                                                        @if ($errors->has('mark411'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark411') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade411') ? ' has-error' : '' }}">
                                                    <label for="grade411" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade411" type="text" class="form-control" name="grade411" value="{{ $result->grade411 }}" autofocus>

                                                        @if ($errors->has('grade411'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade411') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr411') ? ' has-error' : '' }}">
                                                    <label for="cr411" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr411" type="text" class="form-control" name="cr411" value="{{ $result->cr411 }}" autofocus>
                                                        @if ($errors->has('cr411'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr411') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode412') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode412" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode412" type="text" class="form-control" name="rCourseCode412" value="{{ $result->rCourseCode412 }}" autofocus>

                                                        @if ($errors->has('rCourseCode412'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode412') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName412') ? ' has-error' : '' }}">
                                                    <label for="rCourseName412" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName412" type="text" class="form-control" name="rCourseName412" value="{{ $result->rCourseName412 }}" autofocus>

                                                        @if ($errors->has('rCourseName412'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName412') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark412') ? ' has-error' : '' }}">
                                                    <label for="mark412" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark412" type="text" class="form-control" name="mark412" value="{{ $result->mark412 }}" autofocus>

                                                        @if ($errors->has('mark412'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark412') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade412') ? ' has-error' : '' }}">
                                                    <label for="grade412" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade412" type="text" class="form-control" name="grade412" value="{{ $result->grade412 }}" autofocus>

                                                        @if ($errors->has('grade412'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade412') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr412') ? ' has-error' : '' }}">
                                                    <label for="cr412" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr412" type="text" class="form-control" name="cr412" value="{{ $result->cr412 }}" autofocus>

                                                        @if ($errors->has('cr412'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr412') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode413') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode413" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode413" type="text" class="form-control" name="rCourseCode413" value="{{ $result->rCourseCode413 }}" autofocus>

                                                        @if ($errors->has('rCourseCode413'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode413') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName413') ? ' has-error' : '' }}">
                                                    <label for="rCourseName413" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName413" type="text" class="form-control" name="rCourseName413" value="{{ $result->rCourseName413 }}" autofocus>

                                                        @if ($errors->has('rCourseName413'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName413') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark413') ? ' has-error' : '' }}">
                                                    <label for="mark413" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark413" type="text" class="form-control" name="mark413" value="{{ $result->mark413 }}" autofocus>

                                                        @if ($errors->has('mark413'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark413') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade413') ? ' has-error' : '' }}">
                                                    <label for="grade413" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade413" type="text" class="form-control" name="grade413" value="{{ $result->grade413 }}" autofocus>

                                                        @if ($errors->has('grade413'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade413') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr413') ? ' has-error' : '' }}">
                                                    <label for="cr413" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr413" type="text" class="form-control" name="cr413" value="{{ $result->cr413 }}" autofocus>

                                                        @if ($errors->has('cr413'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr413') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode414') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode414" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode414" type="text" class="form-control" name="rCourseCode414" value="{{ $result->rCourseCode414 }}" autofocus>

                                                        @if ($errors->has('rCourseCode414'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode414') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName414') ? ' has-error' : '' }}">
                                                    <label for="rCourseName414" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName414" type="text" class="form-control" name="rCourseName414" value="{{ $result->rCourseName414 }}" autofocus>

                                                        @if ($errors->has('rCourseName414'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName414') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark414') ? ' has-error' : '' }}">
                                                    <label for="mark414" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark414" type="text" class="form-control" name="mark414" value="{{ $result->mark414 }}" autofocus>

                                                        @if ($errors->has('mark414'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark414') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade414') ? ' has-error' : '' }}">
                                                    <label for="grade414" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade414" type="text" class="form-control" name="grade414" value="{{ $result->grade414 }}" autofocus>

                                                        @if ($errors->has('grade414'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade414') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr414') ? ' has-error' : '' }}">
                                                    <label for="cr414" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr414" type="text" class="form-control" name="cr414" value="{{ $result->cr414 }}" autofocus>

                                                        @if ($errors->has('cr414'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr414') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode415') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode415" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode415" type="text" class="form-control" name="rCourseCode415" value="{{ $result->rCourseCode415 }}" autofocus>

                                                        @if ($errors->has('rCourseCode415'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode415') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName415') ? ' has-error' : '' }}">
                                                    <label for="rCourseName415" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName415" type="text" class="form-control" name="rCourseName415" value="{{$result->rCourseName415 }}" autofocus>

                                                        @if ($errors->has('rCourseName415'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName415') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark415') ? ' has-error' : '' }}">
                                                    <label for="mark415" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark415" type="text" class="form-control" name="mark415" value="{{ $result->mark415 }}" autofocus>

                                                        @if ($errors->has('mark415'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark415') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade415') ? ' has-error' : '' }}">
                                                    <label for="grade415" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade415" type="text" class="form-control" name="grade415" value="{{ $result->grade415 }}" autofocus>

                                                        @if ($errors->has('grade415'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade415') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr415') ? ' has-error' : '' }}">
                                                    <label for="cr415" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr415" type="text" class="form-control" name="cr415" value="{{ $result->cr415 }}" autofocus>

                                                        @if ($errors->has('cr415'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr415') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode416') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode416" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode416" type="text" class="form-control" name="rCourseCode416" value="{{ $result->rCourseCode416 }}" autofocus>

                                                        @if ($errors->has('rCourseCode416'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode416') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName416') ? ' has-error' : '' }}">
                                                    <label for="rCourseName416" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName416" type="text" class="form-control" name="rCourseName416" value="{{ $result->rCourseName416 }}" autofocus>

                                                        @if ($errors->has('rCourseName416'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName416') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark416') ? ' has-error' : '' }}">
                                                    <label for="mark416" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark416" type="text" class="form-control" name="mark416" value="{{ $result->mark416 }}" autofocus>

                                                        @if ($errors->has('mark416'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark416') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade416') ? ' has-error' : '' }}">
                                                    <label for="grade416" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade416" type="text" class="form-control" name="grade416" value="{{ $result->grade416 }}" autofocus>

                                                        @if ($errors->has('grade416'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade416') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr416') ? ' has-error' : '' }}">
                                                    <label for="cr416" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr416" type="text" class="form-control" name="cr416" value="{{ $result->cr416 }}" autofocus>

                                                        @if ($errors->has('cr416'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr416') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for first semester 400 level--}}
                                                <div class="form-group{{ $errors->has('rSwa41') ? ' has-error' : '' }}">
                                                    <label for="rSwa41" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa41" type="text" class="form-control" name="rSwa41" value="{{ $result->rSwa41 }}" autofocus>

                                                        @if ($errors->has('rSwa41'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa41') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--first course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode421') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode421" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode421" type="text" class="form-control" name="rCourseCode421" value="{{ $result->rCourseCode421 }}" autofocus>

                                                        @if ($errors->has('rCourseCode421'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode421') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName421') ? ' has-error' : '' }}">
                                                    <label for="rCourseName421" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName421" type="text" class="form-control" name="rCourseName421" value="{{ $result->rCourseName421 }}" autofocus>

                                                        @if ($errors->has('rCourseName421'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName421') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark421') ? ' has-error' : '' }}">
                                                    <label for="mark421" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark421" type="text" class="form-control" name="mark421" value="{{ $result->mark421 }}" autofocus>

                                                        @if ($errors->has('mark421'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark421') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade421') ? ' has-error' : '' }}">
                                                    <label for="grade421" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade421" type="text" class="form-control" name="grade421" value="{{ $result->grade421}}" autofocus>

                                                        @if ($errors->has('grade421'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade421') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr421') ? ' has-error' : '' }}">
                                                    <label for="cr421" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr421" type="text" class="form-control" name="cr421" value="{{$result->cr421 }}" autofocus>

                                                        @if ($errors->has('cr421'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr421') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--second course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode422') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode422" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode422" type="text" class="form-control" name="rCourseCode422" value="{{ $result->rCourseCode422 }}" autofocus>

                                                        @if ($errors->has('rCourseCode422'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode422') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName422') ? ' has-error' : '' }}">
                                                    <label for="rCourseName422" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName422" type="text" class="form-control" name="rCourseName422" value="{{ $result->rCourseName422 }}" autofocus>

                                                        @if ($errors->has('rCourseName422'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName422') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark422') ? ' has-error' : '' }}">
                                                    <label for="mark422" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark422" type="text" class="form-control" name="mark422" value="{{ $result->mark422 }}" autofocus>

                                                        @if ($errors->has('mark422'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark422') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade422') ? ' has-error' : '' }}">
                                                    <label for="grade422" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade422" type="text" class="form-control" name="grade422" value="{{ $result->grade422 }}" autofocus>

                                                        @if ($errors->has('grade422'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade422') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr422') ? ' has-error' : '' }}">
                                                    <label for="cr422" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr422" type="text" class="form-control" name="cr422" value="{{ $result->cr422 }}" autofocus>

                                                        @if ($errors->has('cr422'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr422') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--third course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode423') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode423" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode423" type="text" class="form-control" name="rCourseCode423" value="{{ $result->rCourseCode423 }}" autofocus>

                                                        @if ($errors->has('rCourseCode423'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode423') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName423') ? ' has-error' : '' }}">
                                                    <label for="rCourseName423" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName423" type="text" class="form-control" name="rCourseName423" value="{{ $result->rCourseName423 }}" autofocus>

                                                        @if ($errors->has('rCourseName423'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName423') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark423') ? ' has-error' : '' }}">
                                                    <label for="mark423" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark423" type="text" class="form-control" name="mark423" value="{{ $result->mark423 }}" autofocus>

                                                        @if ($errors->has('mark423'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark423') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade423') ? ' has-error' : '' }}">
                                                    <label for="grade423" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade423" type="text" class="form-control" name="grade423" value="{{ $result->grade423 }}" autofocus>

                                                        @if ($errors->has('grade423'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade423') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr423') ? ' has-error' : '' }}">
                                                    <label for="cr423" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr423" type="text" class="form-control" name="cr423" value="{{ $result->cr423 }}" autofocus>

                                                        @if ($errors->has('cr423'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr423') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fourth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode424') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode424" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode424" type="text" class="form-control" name="rCourseCode424" value="{{ $result->rCourseCode424 }}" autofocus>

                                                        @if ($errors->has('rCourseCode424'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode424') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName424') ? ' has-error' : '' }}">
                                                    <label for="rCourseName424" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName424" type="text" class="form-control" name="rCourseName424" value="{{ $result->rCourseName424 }}" autofocus>

                                                        @if ($errors->has('rCourseName424'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName424') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark424') ? ' has-error' : '' }}">
                                                    <label for="mark424" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark424" type="text" class="form-control" name="mark424" value="{{ $result->mark424 }}" autofocus>

                                                        @if ($errors->has('mark424'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark424') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade424') ? ' has-error' : '' }}">
                                                    <label for="grade424" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade424" type="text" class="form-control" name="grade424" value="{{ $result->grade424 }}" autofocus>

                                                        @if ($errors->has('grade424'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade424') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr424') ? ' has-error' : '' }}">
                                                    <label for="cr424" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr424" type="text" class="form-control" name="cr424" value="{{ $result-> cr424 }}" autofocus>

                                                        @if ($errors->has('cr424'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr424') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--fifth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode425') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode425" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode425" type="text" class="form-control" name="rCourseCode425" value="{{ $result->rCourseCode425 }}" autofocus>

                                                        @if ($errors->has('rCourseCode425'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode425') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName425') ? ' has-error' : '' }}">
                                                    <label for="rCourseName425" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName425" type="text" class="form-control" name="rCourseName425" value="{{ $result->rCourseName425 }}" autofocus>

                                                        @if ($errors->has('rCourseName425'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName425') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark425') ? ' has-error' : '' }}">
                                                    <label for="mark425" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark425" type="text" class="form-control" name="mark425" value="{{ $result->mark425 }}" autofocus>

                                                        @if ($errors->has('mark425'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark425') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade425') ? ' has-error' : '' }}">
                                                    <label for="grade425" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade425" type="text" class="form-control" name="grade425" value="{{ $result->grade425 }}" autofocus>

                                                        @if ($errors->has('grade425'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade425') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr425') ? ' has-error' : '' }}">
                                                    <label for="cr425" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr425" type="text" class="form-control" name="cr425" value="{{ $result->cr425 }}" autofocus>

                                                        @if ($errors->has('cr425'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr425') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--sixth course--}}
                                                <div class="form-group{{ $errors->has('rCourseCode426') ? ' has-error' : '' }}">
                                                    <label for="rCourseCode426" class="col-md-4 control-label">Course Code</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseCode426" type="text" class="form-control" name="rCourseCode426" value="{{ $result->rCourseCode426 }}" autofocus>

                                                        @if ($errors->has('rCourseCode426'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseCode426') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('rCourseName426') ? ' has-error' : '' }}">
                                                    <label for="rCourseName426" class="col-md-4 control-label">Course Name</label>

                                                    <div class="col-md-6">
                                                        <input id="rCourseName426" type="text" class="form-control" name="rCourseName426" value="{{ $result->rCourseName426 }}" autofocus>

                                                        @if ($errors->has('rCourseName426'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rCourseName426') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('mark426') ? ' has-error' : '' }}">
                                                    <label for="mark426" class="col-md-4 control-label">Mark</label>

                                                    <div class="col-md-6">
                                                        <input id="mark426" type="text" class="form-control" name="mark426" value="{{ $result->mark426 }}" autofocus>

                                                        @if ($errors->has('mark426'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('mark426') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('grade426') ? ' has-error' : '' }}">
                                                    <label for="grade426" class="col-md-4 control-label">Grade</label>

                                                    <div class="col-md-6">
                                                        <input id="grade426" type="text" class="form-control" name="grade426" value="{{ $result->grade426 }}" autofocus>

                                                        @if ($errors->has('grade426'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('grade426') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('cr426') ? ' has-error' : '' }}">
                                                    <label for="cr426" class="col-md-4 control-label">Credit Hours</label>

                                                    <div class="col-md-6">
                                                        <input id="cr426" type="text" class="form-control" name="cr426" value="{{ $result->cr426 }}" autofocus>

                                                        @if ($errors->has('cr426'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('cr426') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                {{--swa for second semester 400 level--}}
                                                <div class="form-group{{ $errors->has('rSwa42') ? ' has-error' : '' }}">
                                                    <label for="rSwa42" class="col-md-4 control-label">Semester Weighted Average</label>

                                                    <div class="col-md-6">
                                                        <input id="rSwa42" type="text" class="form-control" name="rSwa42" value="{{ $result->rSwa42 }}" autofocus>

                                                        @if ($errors->has('rSwa42'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('rSwa42') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100%;" colspan="2">
                                    {{--cwa--}}
                                    <div class="form-group{{ $errors->has('rCwa') ? ' has-error' : '' }}">
                                        <label for="rCwa" class="col-md-3 control-label">Cummulative Weighted Average</label>

                                        <div class="col-md-9">
                                            <input id="rCwa" type="text" class="form-control" name="rCwa" value="{{ $result->rCwa }}" required autofocus>

                                            @if ($errors->has('rCwa'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rCwa') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Result
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
