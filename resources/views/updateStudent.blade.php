<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Update Student</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-2 div-for-nav">
            <p class="dashboardText"><strong>DASHBOARD</strong></p>
            <div>
                <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
            </div>

            <div class="side-nav col-md-12">
                <br/><br><ul class="nav"><strong>ADD</strong></ul>
                <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                <a href="select-student" onclick="history.go(-1); return false;" role="button" class="linkColor"> <li>Update Student</li></a>
                <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                <a href="select-result" role="button" class="linkColor"> <li>Update Result</li></a>

                <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                <a href="view-invoices" role="button" class="linkColor"> <li>View Invoice</li></a>
                <a href="view-timetable" role="button" class="linkColor"> <li>View TimeTable</li></a>
                <a href="view-results" role="button" class="linkColor"> <li>View Result</li></a>
                <br/><br><br/><br/><br><br/><br/><br><br><br/><br/>
            </div>
        </div>

        <div class="col-md-10">
            <!-- Header -->
            <header>
                <div class="row">
                    <div class=" navHeader col-md-12">
                        <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                        </p>

                    </div>
                </div>
            </header>

            <div class="panel panel-default">
                <div class="panel-heading"> Update Student </div>

                @include('notification')

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/update-student') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="sida" value="{{ $student->sida }}">

                        <div class="form-group{{ $errors->has('sFname') ? ' has-error' : '' }}">
                            <label for="sFname" class="col-md-4 control-label">Student First Name</label>

                            <div class="col-md-6">
                                <input id="sFname" type="text" class="form-control" name="sFname" value="{{ $student->sFname }}" required autofocus>

                                @if ($errors->has('sFname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sFname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sSname') ? ' has-error' : '' }}">
                            <label for="sSname" class="col-md-4 control-label">Student Surname</label>

                            <div class="col-md-6">
                                <input id="sSname" type="text" class="form-control" name="sSname" value="{{ $student->sSname }}" required autofocus>

                                @if ($errors->has('sSname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sSname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sOname') ? ' has-error' : '' }}">
                            <label for="sOname" class="col-md-4 control-label">Student Other Name</label>

                            <div class="col-md-6">
                                <input id="sOname" type="text" class="form-control" name="sOname" value="{{ $student->sOname }}" required autofocus>

                                @if ($errors->has('sOname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sOname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sid') ? ' has-error' : '' }}">
                            <label for="sid" class="col-md-4 control-label">Student ID</label>

                            <div class="col-md-6">
                                <input id="sid" type="text" class="form-control" name="sid" value="{{ $student->sid }}" required autofocus>

                                @if ($errors->has('sid'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sid') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sEmail') ? ' has-error' : '' }}">
                            <label for="sEmail" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="sEmail" type="email" class="form-control" name="sEmail" value="{{ $student->sEmail }}" required>

                                @if ($errors->has('sEmail'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sEmail') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('spassword') ? ' has-error' : '' }}">
                            <label for="spassword" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="spassword" type="password" class="form-control" name="spassword" value="{{ $student->spassword }}" required>

                                @if ($errors->has('spassword'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('spassword') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $student->phone }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sLevel') ? ' has-error' : '' }}">
                            <label for="sLevel" class="col-md-4 control-label">Level</label>

                            <div class="col-md-6">
                                <select id="sLevel" name="sLevel" class="form-control"  value="{{ $student->sLevel }}" required autofocus>

                                    <option disabled="disabled">select a level</option>
                                    @if($student->sLevel == "100")
                                    <option value="100">100</option>
                                    @endif
                                    @if($student->sLevel == "200")
                                    <option value="200">200</option>
                                    @endif
                                    @if($student->sLevel == "300")
                                    <option value="300">300</option>
                                    @endif
                                    @if($student->sLevel == "400")
                                    <option value="400">400</option>
                                        @endif
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="300">300</option>
                                    <option value="400">400</option>
                                </select>

                                @if ($errors->has('sLevel'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sLevel') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('programme') ? ' has-error' : '' }}">
                            <label for="programme" class="col-md-4 control-label">Programme</label>

                            <div class="col-md-6">
                                <input id="programme" type="text" class="form-control" name="programme" value="{{ $student->programme }}" required autofocus>

                                @if ($errors->has('programme'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('programme') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sSchool') ? ' has-error' : '' }}">
                            <label for="sSchool" class="col-md-4 control-label">School</label>

                            <div class="col-md-6">
                                <select id="sSchool" name="sSchool" class="form-control"  value="{{ $student->sSchool }}" required autofocus>

                                    <option disabled="disabled">Select School</option>
                                    @if($student->sSchool == "School of Informatics,Engineering and Technology")
                                    <option value="School of Informatics,Engineering and Technology">SIET</option>
                                    @endif
                                    @if($student->sSchool == "School of business and Leadership")
                                    <option value="School of business and Leadership">SBL</option>
                                    @endif
                                    @if($student->sSchool == "Faculty of Art and Science")
                                    <option value="Faculty of Art and Science">FAS</option>
                                        @endif
                                    <option value="School of Informatics,Engineering and Technology">SIET</option>
                                    <option value="School of business and Leadership">SBL</option>
                                    <option value="Faculty of Art and Science">FAS</option>
                                </select>

                                @if ($errors->has('sSchool'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sSchool') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('stream') ? ' has-error' : '' }}">
                            <label for="stream" class="col-md-4 control-label">Stream</label>

                            <div class="col-md-6">
                                <select id="stream" name="stream" class="form-control"  value="{{ $student->stream }}" required autofocus>

                                    <option disabled="disabled" >Select Stream</option>
                                    @if($student->stream == "Morning")
                                    <option value="Morning">Morning</option>
                                    @endif
                                    @if($student->stream == "Evening")
                                    <option value="Evening">Evening</option>
                                    @endif
                                    <option value="Morning">Morning</option>
                                    <option value="Evening">Evening</option>
                                </select>

                                @if ($errors->has('stream'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stream') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                            <label for="nationality" class="col-md-4 control-label">Nationality</label>

                            <div class="col-md-6">
                                <input id="nationality" type="text" class="form-control" name="nationality" value="{{ $student->nationality }}" required autofocus>

                                @if ($errors->has('nationality'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nationality') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tid') ? ' has-error' : '' }}">
                            <label for="tid" class="col-md-4 control-label">Timetable ID</label>

                            <div class="col-md-6">
                                <input id="tid" type="text" class="form-control" name="tid" value="{{ $student->tid  }}" autofocus>

                                @if ($errors->has('tid'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tid') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Student
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
