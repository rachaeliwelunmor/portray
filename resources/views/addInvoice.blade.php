<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Student</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
        <div class="row">
            <div class="col-md-2 div-for-nav">
                <p class="dashboardText"><strong>DASHBOARD</strong></p>
                <div>
                    <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
                </div>

                <div class="side-nav col-md-12">
                    <br/><br><ul class="nav"><strong>ADD</strong></ul>
                    <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                    <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                    <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                    <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                    <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                    <a href="select-student" role="button" class="linkColor"> <li>Update Student</li></a>
                    <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                    <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                    <a href="select-result" role="button" class="linkColor"> <li>Update Result</li></a>

                    <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                    <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                    <a href="view-invoices" role="button" class="linkColor"> <li>View Invoice</li></a>
                    <a href="view-timetable" role="button" class="linkColor"> <li>View TimeTable</li></a>
                    <a href="view-results" role="button" class="linkColor"> <li>View Result</li></a>
                    <br/><br><br/>
                </div>
            </div>

            <div class="col-md-10">
                <!-- Header -->
                <header>
                    <div class="row">
                        <div class=" navHeader col-md-12">
                            <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                            </p>

                        </div>
                    </div>
                </header>
                <div class="panel panel-default">
                    <div class="panel-heading"> Add Invoice </div>

                    @include('notification')

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('add-invoice') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('sida') ? ' has-error' : '' }}">
                                <label for="sida" class="col-md-4 control-label">Student's ID</label>

                                <div class="col-md-6">
                                    <input id="sida" type="text" class="form-control" name="sida" value="{{ old('sida') }}" required autofocus>

                                    @if ($errors->has('sida'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sida') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('iLevel') ? ' has-error' : '' }}">
                                <label for="iLevel" class="col-md-4 control-label">Level for Arrears</label>

                                <div class="col-md-6">
                                    <select id="sLevel" name="iLevel" class="form-control"  value="{{ old('iLevel') }}" autofocus>

                                        <option disabled="disabled" selected = "selected">select a Level</option>
                                        <option value="100">100</option>
                                        <option value="200">200</option>
                                        <option value="300">300</option>
                                        <option value="400">400</option>
                                    </select>

                                    @if ($errors->has('iLevel'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('iLevel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('iSemester') ? ' has-error' : '' }}">
                                <label for="iSemester" class="col-md-4 control-label">Semester For Arrears</label>

                                <div class="col-md-6">
                                    <select id="sLevel" name="iSemester" class="form-control"  value="{{ old('iSemester') }}" autofocus>

                                        <option disabled="disabled" selected = "selected">select a Semester</option>
                                        <option value="First Semester">First Semester</option>
                                        <option value="Second Semester">Second Semester</option>
                                    </select>

                                    @if ($errors->has('iSemester'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('iSemester') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('iFeesPayable') ? ' has-error' : '' }}">
                                <label for="iFeesPayable" class="col-md-4 control-label">Fees Payable For Arrears</label>

                                <div class="col-md-6">
                                    <input id="iFeesPayable" type="number" step=".01" class="form-control" name="iFeesPayable" value="{{ old('iFeesPayable') }}"  autofocus>

                                    @if ($errors->has('iFeesPayable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('iFeesPayable') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('iCLevel') ? ' has-error' : '' }}">
                                <label for="iCLevel" class="col-md-4 control-label">Current Level</label>

                                <div class="col-md-6">
                                    <select id="sLevel" name="iCLevel" class="form-control"  value="{{ old('iCLevel') }}" required autofocus>

                                        <option disabled="disabled" selected = "selected">select a Level</option>
                                        <option value="100">100</option>
                                        <option value="200">200</option>
                                        <option value="300">300</option>
                                        <option value="400">400</option>
                                    </select>

                                    @if ($errors->has('iCLevel'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('iCLevel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('iCSemester') ? ' has-error' : '' }}">
                                <label for="iCSemester" class="col-md-4 control-label">Current Semester</label>

                                <div class="col-md-6">
                                    <select id="iCSemester" name="iCSemester" class="form-control"  value="{{ old('iCSemester') }}" required autofocus>

                                        <option disabled="disabled" selected = "selected">select a Semester</option>
                                        <option value="First Semester">First Semester</option>
                                        <option value="Second Semester">Second Semester</option>
                                    </select>

                                    @if ($errors->has('iCSemester'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('iCSemester') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('iCFeesPayable') ? ' has-error' : '' }}">
                                <label for="iCFeesPayable" class="col-md-4 control-label">Fees Payable For Current Semester</label>

                                <div class="col-md-6">
                                    <input id="iCFeesPayable" type="number" step=".01" class="form-control" name="iCFeesPayable" value="{{ old('iCFeesPayable') }}" required  autofocus>

                                    @if ($errors->has('iCFeesPayable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('iCFeesPayable') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add Invoice
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
