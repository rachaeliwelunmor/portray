<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Student</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-2 div-for-nav">
            <p class="dashboardText"><strong>DASHBOARD</strong></p>
            <div>
                <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
            </div>

            <div class="side-nav col-md-12">
                <br/><br><ul class="nav"><strong>ADD</strong></ul>
                <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                <a href="select-student" role="button" class="linkColor"> <li>Update Student</li></a>
                <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                <a href="select-result" role="button" class="linkColor"> <li>Update Result</li></a>

                <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                <a href="view-invoices" role="button" class="linkColor"> <li>View Invoice</li></a>
                <a href="view-timetable" role="button" class="linkColor"> <li>View TimeTable</li></a>
                <a href="view-results" role="button" class="linkColor"> <li>View Result</li></a>
                <br/><br><br/>
            </div>
        </div>
        <div class="col-md-10">
            <!-- Header -->
            <header>
                <div class="row">
                    <div class=" navHeader col-md-12">
                        <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                        </p>

                    </div>
                </div>
            </header>
            <div class="panel panel-default">
                <div class="panel-heading">Invoices</div>
                <div class="panel-body">
                    <table id="table" class="table table-hover">
                        <tr>
                            <th>Students Name</th>
                            <th>Students ID</th>
                            <th>Level</th>
                            <th>Status</th>
                        </tr>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>
                                    {{$invoice->student['sFname']}} {{$invoice->student['sSname']}} {{$invoice->student['sOname']}}
                                </td>
                                <td>
                                    {{$invoice->student['sid']}}
                                </td>
                                <td>
                                    {{$invoice->student['sLevel']}}
                                </td>
                                <td>
                                    {{$invoice->status}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
