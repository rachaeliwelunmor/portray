<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Timetable</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
        <div class="row">
            <div class="col-md-2 div-for-nav">
                <p class="dashboardText"><strong>DASHBOARD</strong></p>
                <div>
                    <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
                </div>

                <div class="side-nav col-md-12">
                    <br/><br><ul class="nav"><strong>ADD</strong></ul>
                    <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                    <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                    <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                    <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                    <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                    <a href="select-student" role="button" class="linkColor"> <li>Update Student</li></a>
                    <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                    <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                    <a href="select-result" role="button" class="linkColor"> <li>Update Result</li></a>

                    <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                    <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                    <a href="view-invoices" role="button" class="linkColor"> <li>View Invoice</li></a>
                    <a href="view-timetable" role="button" class="linkColor"> <li>View TimeTable</li></a>
                    <a href="view-results" role="button" class="linkColor"> <li>View Result</li></a>
                    <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br/><br>
                    <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br>
                    <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br>
                    <br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/><br><br/>

                </div>
            </div>
            <div class="col-md-10">
                <!-- Header -->
                <header>
                    <div class="row">
                        <div class=" navHeader col-md-12">
                            <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                            </p>

                        </div>
                    </div>
                </header>
                <div class="panel panel-default">
                    <div class="panel-heading"> Add Timetable </div>

                    @include('notification')

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('add-timetable') }}">
                            {{ csrf_field() }}

                            <table class="table table-bordered">

                                <tr style="width: 100%;" colspan="3">
                                <div class="form-group{{ $errors->has('tSchool') ? ' has-error' : '' }}">
                                    <label for="tSchool" class="col-md-3 control-label">School</label>

                                    <div class="col-md-9">
                                        <input id="tSchool" type="text" class="form-control" name="tSchool" value="{{ old('tSchool') }}" autofocus>

                                        @if ($errors->has('tSchool'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('tSchool') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </tr>

                                <tr style="width: 100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('tProgramme') ? ' has-error' : '' }}">
                                        <label for="tProgramme" class="col-md-3 control-label">Programme</label>

                                        <div class="col-md-9">
                                            <input id="tProgramme" type="text" class="form-control" name="tProgramme" value="{{ old('tProgramme') }}" autofocus>

                                            @if ($errors->has('tProgramme'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tProgramme') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </tr>

                                <tr style="width: 100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('tSemester') ? ' has-error' : '' }}">
                                        <label for="tSemester" class="col-md-3 control-label">Semester</label>

                                        <div class="col-md-9">
                                            <input id="tSemester" type="text" class="form-control" name="tSemester" value="{{ old('tSemester') }}" autofocus>

                                            @if ($errors->has('tSemester'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tSemester') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </tr>

                                <tr style="width: 100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('tStream') ? ' has-error' : '' }}">
                                        <label for="tStream" class="col-md-3 control-label">Stream</label>

                                        <div class="col-md-9">
                                            <input id="tStream" type="text" class="form-control" name="tStream" value="{{ old('tStream') }}" autofocus>

                                            @if ($errors->has('tStream'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tStream') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </tr>

                                <tr style="width: 100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('tLevel') ? ' has-error' : '' }}">
                                        <label for="tLevel" class="col-md-3 control-label">Level</label>

                                        <div class="col-md-9">
                                            <input id="tLevel" type="text" class="form-control" name="tLevel" value="{{ old('tLevel') }}" autofocus>

                                            @if ($errors->has('tLevel'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tLevel') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </tr>


                                <td style="width: 100%;" colspan="3">
                                       {{--Select day --}}
                                       <div class="form-group{{ $errors->has('monday') ? ' has-error' : '' }}">
                                           <label for="monday" class="col-md-2 control-label">Day</label>

                                           <div class="col-md-10">
                                               <select id="monday" name="monday" class="form-control"  value="{{ old('monday') }}" autofocus>

                                                   <option disabled="disabled" selected = "selected">select a day</option>
                                                   <option value="Monday">Monday</option>
                                               </select>
                                               @if ($errors->has('monday'))
                                                   <span class="help-block">
                                                     <strong>{{ $errors->first('monday') }}</strong>
                                                   </span>
                                               @endif
                                           </div>
                                       </div>
                                   </td>

                                <tr>

                                    <th>
                                        FIRST COURSE
                                    </th>

                                    <th>
                                        SECOND COURSE
                                    </th>

                                    <th>
                                        THIRD COURSE
                                    </th>
                                </tr>

                                <tr>
                                    <td>
                                        {{--Enter course code, course Name, Course lecturer and venue--}}
                                        <div class="form-group{{ $errors->has('course1mc') ? ' has-error' : '' }}">
                                            <label for="course1mc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course1mc" type="text" class="form-control" name="course1mc" value="{{ old('course1mc') }}" autofocus>

                                                @if ($errors->has('course1mc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1mc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1mn') ? ' has-error' : '' }}">
                                            <label for="course1mn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course1mn" type="text" class="form-control" name="course1mn" value="{{ old('course1mn') }}" autofocus>

                                                @if ($errors->has('course1mn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1mn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1ml') ? ' has-error' : '' }}">
                                            <label for="course1ml" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course1ml" type="text" class="form-control" name="course1ml" value="{{ old('course1ml') }}" autofocus>

                                                @if ($errors->has('course1ml'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1ml') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1mv') ? ' has-error' : '' }}">
                                            <label for="course1mv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course1mv" type="text" class="form-control" name="course1mv" value="{{ old('course1mv') }}" autofocus>

                                                @if ($errors->has('course1mv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1mv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>
                                    {{--END OF FIRST COURSE TABLE ROW--}}


                                        {{--BEGINNING OF SECOND COURSE TABLE ROW--}}
                                    <td>
                                        <div class="form-group{{ $errors->has('course2mc') ? ' has-error' : '' }}">
                                            <label for="course2mc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course2mc" type="text" class="form-control" name="course2mc" value="{{ old('course2mc') }}" autofocus>

                                                @if ($errors->has('course2mc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2mc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2mn') ? ' has-error' : '' }}">
                                            <label for="course2mn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course2mn" type="text" class="form-control" name="course2mn" value="{{ old('course2mn') }}" autofocus>

                                                @if ($errors->has('course2mn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2mn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2ml') ? ' has-error' : '' }}">
                                            <label for="course2ml" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course2ml" type="text" class="form-control" name="course2ml" value="{{ old('course2ml') }}" autofocus>

                                                @if ($errors->has('course2ml'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2ml') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2mv') ? ' has-error' : '' }}">
                                            <label for="course2mv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course2mv" type="text" class="form-control" name="course2mv" value="{{ old('course2mv') }}" autofocus>

                                                @if ($errors->has('course2mv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2mv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    {{--END OF SECOND COURSE TABLE ROW--}}

                                    {{--BEGINNING OF THIRD COURSE TABLE ROW--}}
                                    <td>
                                        <div class="form-group{{ $errors->has('course3mc') ? ' has-error' : '' }}">
                                            <label for="course3mc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course3mc" type="text" class="form-control" name="course3mc" value="{{ old('course3mc') }}" autofocus>

                                                @if ($errors->has('course3mc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3mc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3mn') ? ' has-error' : '' }}">
                                            <label for="course3mn" class="col-md-4 control-label">Course Name </label>

                                            <div class="col-md-6">
                                                <input id="course3mn" type="text" class="form-control" name="course3mn" value="{{ old('course3mn') }}" autofocus>

                                                @if ($errors->has('course3mn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3mn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3ml') ? ' has-error' : '' }}">
                                            <label for="course3ml" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course3ml" type="text" class="form-control" name="course3ml" value="{{ old('course3ml') }}" autofocus>

                                                @if ($errors->has('course3ml'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3ml') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3mv') ? ' has-error' : '' }}">
                                            <label for="course3mv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course3mv" type="text" class="form-control" name="course3mv" value="{{ old('course3mv') }}" autofocus>

                                                @if ($errors->has('course3mv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3mv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                    {{--END OF THIRD COURSE TABLE ROW--}}
                            </table>

                            {{--SECOND TABLE--}}

                            <table class="table table-bordered">
                                <td style="width: 100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('tuesday') ? ' has-error' : '' }}">
                                        <label for="tuesday" class="col-md-2 control-label">Day</label>

                                        <div class="col-md-10">
                                            <select id="tuesday" name="tuesday" class="form-control"  value="{{ old('tuesday') }}"  autofocus>

                                                <option disabled="disabled" selected = "selected">select a day</option>
                                                <option value="Tuesday">Tuesday</option>
                                            </select>
                                            @if ($errors->has('tuesday'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tuesday') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>

                                <tr>

                                    <th>
                                        FIRST COURSE
                                    </th>

                                    <th>
                                        SECOND COURSE
                                    </th>

                                    <th>
                                        THIRD COURSE
                                    </th>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="form-group{{ $errors->has('course1tc') ? ' has-error' : '' }}">
                                            <label for="course1tc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course1tc" type="text" class="form-control" name="course1tc" value="{{ old('course1tc') }}" autofocus>

                                                @if ($errors->has('course1tc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1tc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1tn') ? ' has-error' : '' }}">
                                            <label for="course1tn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course1tn" type="text" class="form-control" name="course1tn" value="{{ old('course1tn') }}" autofocus>

                                                @if ($errors->has('course1tn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1tn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1tl') ? ' has-error' : '' }}">
                                            <label for="course1tl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course1tl" type="text" class="form-control" name="course1tl" value="{{ old('course1tl') }}" autofocus>

                                                @if ($errors->has('course1tl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1tl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1tv') ? ' has-error' : '' }}">
                                            <label for="course1tv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course1tv" type="text" class="form-control" name="course1tv" value="{{ old('course1tv') }}" autofocus>

                                                @if ($errors->has('course1tv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1tv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="form-group{{ $errors->has('course2tc') ? ' has-error' : '' }}">
                                            <label for="course2tc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course2tc" type="text" class="form-control" name="course2tc" value="{{ old('course2tc') }}" autofocus>

                                                @if ($errors->has('course2tc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2tc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2tn') ? ' has-error' : '' }}">
                                            <label for="course2tn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course2tn" type="text" class="form-control" name="course2tn" value="{{ old('course2tn') }}" autofocus>

                                                @if ($errors->has('course2tn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2tn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2tl') ? ' has-error' : '' }}">
                                            <label for="course2tl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course2tl" type="text" class="form-control" name="course2tl" value="{{ old('course2tl') }}" autofocus>

                                                @if ($errors->has('course2tl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2tl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2tv') ? ' has-error' : '' }}">
                                            <label for="course2tv" class="col-md-4 control-label">venue</label>

                                            <div class="col-md-6">
                                                <input id="course2tv" type="text" class="form-control" name="course2tv" value="{{ old('course2tv') }}" autofocus>

                                                @if ($errors->has('course2tv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2tv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>

                                    <td>
                                        <div class="form-group{{ $errors->has('course3tc') ? ' has-error' : '' }}">
                                            <label for="course3tc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course3tc" type="text" class="form-control" name="course3tc" value="{{ old('course3tc') }}" autofocus>

                                                @if ($errors->has('course3tc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3tc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3tn') ? ' has-error' : '' }}">
                                            <label for="course3tn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course3tn" type="text" class="form-control" name="course3tn" value="{{ old('course3tn') }}" autofocus>

                                                @if ($errors->has('course3tn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3tn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3tl') ? ' has-error' : '' }}">
                                            <label for="course3tl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course3tl" type="text" class="form-control" name="course3tl" value="{{ old('course3tl') }}" autofocus>

                                                @if ($errors->has('course3tl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3tl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3tv') ? ' has-error' : '' }}">
                                            <label for="course3tv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course3tv" type="text" class="form-control" name="course3tv" value="{{ old('course3tv') }}" autofocus>

                                                @if ($errors->has('course3tv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3tv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>

                                </tr>
                            </table>

                            <table class="table table-bordered">
                                <td style="width:100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('wednesday') ? ' has-error' : '' }}">
                                        <label for="wednesday" class="col-md-2 control-label">Day</label>

                                        <div class="col-md-10">
                                            <select id="wednesday" name="wednesday" class="form-control"  value="{{ old('wednesday') }}" autofocus>

                                                <option disabled="disabled" selected = "selected">select a day</option>
                                                <option value="wednesday">wednesday</option>
                                            </select>
                                            @if ($errors->has('wednesday'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('wednesday') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>

                                <tr>

                                    <th>
                                        FIRST COURSE
                                    </th>

                                    <th>
                                        SECOND COURSE
                                    </th>

                                    <th>
                                        THIRD COURSE
                                    </th>
                                </tr>

                                <tr>
                                    <td>

                                        <div class="form-group{{ $errors->has('course1wc') ? ' has-error' : '' }}">
                                            <label for="course1wc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course1wc" type="text" class="form-control" name="course1wc" value="{{ old('course1wc') }}" autofocus>

                                                @if ($errors->has('course1wc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1wc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1wn') ? ' has-error' : '' }}">
                                            <label for="course1wn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course1wn" type="text" class="form-control" name="course1wn" value="{{ old('course1wn') }}" autofocus>

                                                @if ($errors->has('course1wn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1wn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1wl') ? ' has-error' : '' }}">
                                            <label for="course1wl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course1wl" type="text" class="form-control" name="course1wl" value="{{ old('course1wl') }}" autofocus>

                                                @if ($errors->has('course1wl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1wl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1wv') ? ' has-error' : '' }}">
                                            <label for="course1wv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course1wv" type="text" class="form-control" name="course1wv" value="{{ old('course1wv') }}" autofocus>

                                                @if ($errors->has('course1wv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1wv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group{{ $errors->has('course2wc') ? ' has-error' : '' }}">
                                            <label for="course2wc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course2wc" type="text" class="form-control" name="course2wc" value="{{ old('course2wc') }}" autofocus>

                                                @if ($errors->has('course2wc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2wc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2wn') ? ' has-error' : '' }}">
                                            <label for="course2wn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course2wn" type="text" class="form-control" name="course2wn" value="{{ old('course2wn') }}" autofocus>

                                                @if ($errors->has('course2wn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2wn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2wl') ? ' has-error' : '' }}">
                                            <label for="course2wl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course2wl" type="text" class="form-control" name="course2wl" value="{{ old('course2wl') }}" autofocus>

                                                @if ($errors->has('course2wl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2wl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2wv') ? ' has-error' : '' }}">
                                            <label for="course2wv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course2wv" type="text" class="form-control" name="course2wv" value="{{ old('course2wv') }}" autofocus>

                                                @if ($errors->has('course2wv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2wv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>
                                    <td>
                                        <div class="form-group{{ $errors->has('course3wc') ? ' has-error' : '' }}">
                                            <label for="course3wc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course3wc" type="text" class="form-control" name="course3wc" value="{{ old('course3wc') }}" autofocus>

                                                @if ($errors->has('course3wc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3wc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3wn') ? ' has-error' : '' }}">
                                            <label for="course3wn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course3wn" type="text" class="form-control" name="course3wn" value="{{ old('course3wn') }}" autofocus>

                                                @if ($errors->has('course3wn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3wn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3wl') ? ' has-error' : '' }}">
                                            <label for="course3wl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course3wl" type="text" class="form-control" name="course3wl" value="{{ old('course3wl') }}" autofocus>

                                                @if ($errors->has('course3wl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3wl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3wv') ? ' has-error' : '' }}">
                                            <label for="course3wv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course3wv" type="text" class="form-control" name="course3wv" value="{{ old('course3wv') }}" autofocus>

                                                @if ($errors->has('course3wv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3wv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>
                                </tr>

                            </table>

                            <table class="table table-bordered">
                                <td style="width:100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('thursday') ? ' has-error' : '' }}">
                                        <label for="thursday" class="col-md-2 control-label">Day</label>

                                        <div class="col-md-10">
                                            <select id="thursday" name="thursday" class="form-control"  value="{{ old('thursday') }}" autofocus>

                                                <option disabled="disabled" selected = "selected">select a day</option>
                                                <option value="thursday">thursday</option>
                                            </select>
                                            @if ($errors->has('thursday'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('thursday') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </td>

                                <tr>

                                    <th>
                                        FIRST COURSE
                                    </th>

                                    <th>
                                        SECOND COURSE
                                    </th>

                                    <th>
                                        THIRD COURSE
                                    </th>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="form-group{{ $errors->has('course1thc') ? ' has-error' : '' }}">
                                            <label for="course1thc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course1thc" type="text" class="form-control" name="course1thc" value="{{ old('course1thc') }}" autofocus>

                                                @if ($errors->has('course1thc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1thc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1thn') ? ' has-error' : '' }}">
                                            <label for="course1thn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course1thn" type="text" class="form-control" name="course1thn" value="{{ old('course1thn') }}" autofocus>

                                                @if ($errors->has('course1thn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1thn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1thl') ? ' has-error' : '' }}">
                                            <label for="course1thl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course1thl" type="text" class="form-control" name="course1thl" value="{{ old('course1thl') }}" autofocus>

                                                @if ($errors->has('course1thl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1thl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1thv') ? ' has-error' : '' }}">
                                            <label for="course1thv" class="col-md-4 control-label">Course Venue</label>

                                            <div class="col-md-6">
                                                <input id="course1thv" type="text" class="form-control" name="course1thv" value="{{ old('course1thv') }}" autofocus>

                                                @if ($errors->has('course1thv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1thv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>
                                    <td>
                                        <div class="form-group{{ $errors->has('course2thc') ? ' has-error' : '' }}">
                                            <label for="course2thc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course2thc" type="text" class="form-control" name="course2thc" value="{{ old('course2thc') }}" autofocus>

                                                @if ($errors->has('course2thc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2thc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2thn') ? ' has-error' : '' }}">
                                            <label for="course2thn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course2thn" type="text" class="form-control" name="course2thn" value="{{ old('course2thn') }}" autofocus>

                                                @if ($errors->has('course2thn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2thn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2thl') ? ' has-error' : '' }}">
                                            <label for="course2thl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course2thl" type="text" class="form-control" name="course2thl" value="{{ old('course2thl') }}" autofocus>

                                                @if ($errors->has('course2thl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2thl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2thv') ? ' has-error' : '' }}">
                                            <label for="course2thv" class="col-md-4 control-label">Course Venue</label>

                                            <div class="col-md-6">
                                                <input id="course2thv" type="text" class="form-control" name="course2thv" value="{{ old('course2thv') }}" autofocus>

                                                @if ($errors->has('course2thv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2thv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group{{ $errors->has('course3thc') ? ' has-error' : '' }}">
                                            <label for="course3thc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course3thc" type="text" class="form-control" name="course3thc" value="{{ old('course3thc') }}" autofocus>

                                                @if ($errors->has('course3thc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3thc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3thn') ? ' has-error' : '' }}">
                                            <label for="course3thn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course3thn" type="text" class="form-control" name="course3thn" value="{{ old('course3thn') }}" autofocus>

                                                @if ($errors->has('course3thn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3thn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3thl') ? ' has-error' : '' }}">
                                            <label for="course3thl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course3thl" type="text" class="form-control" name="course3thl" value="{{ old('course3thl') }}" autofocus>

                                                @if ($errors->has('course3thl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3thl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3thv') ? ' has-error' : '' }}">
                                            <label for="course3thv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course3thv" type="text" class="form-control" name="course3thv" value="{{ old('course3thv') }}" autofocus>

                                                @if ($errors->has('course3thv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3thv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>
                                </tr>

                            </table>

                            <table class="table table-bordered">
                                <td style="width:100%;" colspan="3">
                                    <div class="form-group{{ $errors->has('friday') ? ' has-error' : '' }}">
                                        <label for="friday" class="col-md-2 control-label">Day</label>

                                        <div class="col-md-10">
                                            <select id="friday" name="friday" class="form-control"  value="{{ old('friday') }}" autofocus>

                                                <option disabled="disabled" selected = "selected">select a day</option>
                                                <option value="friday">friday</option>
                                            </select>
                                            @if ($errors->has('friday'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('friday') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </td>

                                <tr>

                                    <th>
                                        FIRST COURSE
                                    </th>

                                    <th>
                                        SECOND COURSE
                                    </th>

                                    <th>
                                        THIRD COURSE
                                    </th>
                                </tr>


                                <tr>
                                    <td>
                                        <div class="form-group{{ $errors->has('course1fc') ? ' has-error' : '' }}">
                                            <label for="course1fc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course1fc" type="text" class="form-control" name="course1fc" value="{{ old('course1fc') }}" autofocus>

                                                @if ($errors->has('course1fc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1fc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1fn') ? ' has-error' : '' }}">
                                            <label for="course1fn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course1fn" type="text" class="form-control" name="course1fn" value="{{ old('course1fn') }}" autofocus>

                                                @if ($errors->has('course1fn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1fn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1fl') ? ' has-error' : '' }}">
                                            <label for="course1fl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course1fl" type="text" class="form-control" name="course1fl" value="{{ old('course1fl') }}" autofocus>

                                                @if ($errors->has('course1fl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1fl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course1fv') ? ' has-error' : '' }}">
                                            <label for="course1fv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course1fv" type="text" class="form-control" name="course1fv" value="{{ old('course1fv') }}" autofocus>

                                                @if ($errors->has('course1fv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course1fv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </td>
                                    <td>
                                        <div class="form-group{{ $errors->has('course2fc') ? ' has-error' : '' }}">
                                            <label for="course2fc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course2fc" type="text" class="form-control" name="course2fc" value="{{ old('course2fc') }}" autofocus>

                                                @if ($errors->has('course2fc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2fc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2fn') ? ' has-error' : '' }}">
                                            <label for="course2fn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course2fn" type="text" class="form-control" name="course2fn" value="{{ old('course2fn') }}" autofocus>

                                                @if ($errors->has('course2fn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2fn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2fl') ? ' has-error' : '' }}">
                                            <label for="course2fl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course2fl" type="text" class="form-control" name="course2fl" value="{{ old('course2fl') }}" autofocus>

                                                @if ($errors->has('course2fl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2fl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course2fv') ? ' has-error' : '' }}">
                                            <label for="course2fv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course2fv" type="text" class="form-control" name="course2fv" value="{{ old('course2fv') }}" autofocus>

                                                @if ($errors->has('course2fv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course2fv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td>

                                        <div class="form-group{{ $errors->has('course3fc') ? ' has-error' : '' }}">
                                            <label for="course3fc" class="col-md-4 control-label">Course Code</label>

                                            <div class="col-md-6">
                                                <input id="course3fc" type="text" class="form-control" name="course3fc" value="{{ old('course3fc') }}" autofocus>

                                                @if ($errors->has('course3fc'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3fc') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3fn') ? ' has-error' : '' }}">
                                            <label for="course3fn" class="col-md-4 control-label">Course Name</label>

                                            <div class="col-md-6">
                                                <input id="course3fn" type="text" class="form-control" name="course3fn" value="{{ old('course3fn') }}" autofocus>

                                                @if ($errors->has('course3fn'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3fn') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3fl') ? ' has-error' : '' }}">
                                            <label for="course3fl" class="col-md-4 control-label">Lecturer</label>

                                            <div class="col-md-6">
                                                <input id="course3fl" type="text" class="form-control" name="course3fl" value="{{ old('course3fl') }}" autofocus>

                                                @if ($errors->has('course3fl'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3fl') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('course3fv') ? ' has-error' : '' }}">
                                            <label for="course3fv" class="col-md-4 control-label">Venue</label>

                                            <div class="col-md-6">
                                                <input id="course3fv" type="text" class="form-control" name="course3fv" value="{{ old('course3fv') }}" autofocus>

                                                @if ($errors->has('course3fv'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('course3fv') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </table>



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add Timetable
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
