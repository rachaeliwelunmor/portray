<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>view Invoice</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-2 div-for-nav">
            <p class="dashboardText"><strong>DASHBOARD</strong></p>
            <div>
                <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
            </div>

            <div class="side-nav col-md-12">
                <br/><br><ul class="nav"><strong>ADD</strong></ul>
                <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                <a href="select-student" role="button" class="linkColor"> <li>Update Student</li></a>
                <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                <a href="select-result" role="button" class="linkColor"> <li>Update Result</li></a>

                <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                <a href="view-invoices" onclick="history.go(-1); return false;" role="button" class="linkColor"> <li>View Invoice</li></a>
                <a href="view-timetable" role="button" class="linkColor"> <li>View TimeTable</li></a>
                <a href="view-results" role="button" class="linkColor"> <li>View Result</li></a>
                <br/><br><br/>
            </div>
        </div>
        <div class="col-md-10">
            <!-- Header -->
            <header>
                <div class="row">
                    <div class=" navHeader col-md-12">
                        <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                        </p>

                    </div>
                </div>
            </header>
            <div class="panel panel-default">
                <div class="panel-heading">Invoice for {{$invoice->student['sFname']}} {{$invoice->student['sSname']}} {{$invoice->student['sOname']}}</div>
                <div class="panel-body">
                        <p><strong>Name: </strong>{{$invoice->student['sFname']}} {{$invoice->student['sSname']}} {{$invoice->student['sOname']}}</p>
                        <p><strong>ID Number: </strong> {{$invoice->student['sid']}} </p>
                        <p><strong>Programme: </strong> {{$invoice->student['programme']}} </p>
                        <p><strong>Stream: </strong> {{$invoice->student['stream']}} </p>
                        <p><strong>Current Level: </strong> {{$invoice->student['sLevel']}} </p>

                        <h4 align="center"><u>STUDENT INVOICE</u></h4>

                        <table id="table" class="table table-hover table-bordered" align="center">
                            <tr style="text-transform: uppercase;">
                                <th style="text-align: center;">Level</th>
                                <th style="text-align: center;">Semester</th>
                                <th style="text-align: center;">Fees payable (USD)</th>
                            </tr>

                            <tr>
                                <td style="text-align: center;">
                                    {{$invoice->iLevel}}
                                </td>
                                <td style="text-align: center;">
                                    {{$invoice->iSemester}} <br> (ARREARS)
                                </td>

                                <td style="text-align: center;">{{$invoice->iFeesPayable}}</td>

                            </tr>

                            <tr>
                                <td style="text-align: center;">
                                    {{$invoice->iCLevel}}
                                </td>
                                <td style="text-align: center;">
                                    {{$invoice->iCSemester}}
                                </td>

                                <td style="text-align: center;">{{$invoice->iCFeesPayable}}</td>

                            </tr>

                            <tr>
                                <td> </td>
                                <td style="text-align: center;"> GRAND TOTAL</td>

                                <td style="text-align: center;">{{$invoice->iTotal}}</td>

                            </tr>

                            <tr>
                                <td> </td>
                                <td style="text-align: center;"> STATUS </td>

                                <td style="text-align: center;"> {{$invoice->status}}</td>

                            </tr>

                        </table>
                    <a href="{{url('/generate-pdf-inv/'.$invoice->invid)}}" class="btn btn-primary" role="button" style="float: right;">
                        Generate PDF
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
