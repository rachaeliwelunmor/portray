<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Student</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{url('css\bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css\style.css')}}" rel="stylesheet" type="text/css">

    {{--Scripts--}}
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body>

<div class="container-fluid con-style">
    <div class="row">
        <div class="col-md-2 div-for-nav">
            <p class="dashboardText"><strong>DASHBOARD</strong></p>
            <div>
                <br> <a href="home"> <img class="img-circle img-school" src="{{url('images\SCHOOL LOGO.jpg')}}" alt="Schools Logo"/> </a>
            </div>

            <div class="side-nav col-md-12">
                <br/><br><ul class="nav"><strong>ADD</strong></ul>
                <a href="add-student" role="button" class="linkColor"> <li >Add Student</li></a>
                <a href="add-invoice" role="button" class="linkColor"><li>Add Invoice</li></a>
                <a href="add-timetable" role="button" class="linkColor"><li>Add TimeTable</li></a>
                <a href="add-result" role="button" class="linkColor"><li>Add Result</li></a>

                <br><br/><ul class="nav"><strong>UPDATE</strong></ul>
                <a href="select-student" role="button" class="linkColor"> <li>Update Student</li></a>
                <a href="select-invoice" role="button" class="linkColor"> <li>Update Invoice</li></a>
                <a href="select-timetable" role="button" class="linkColor"> <li>Update TimeTable</li></a>
                <a href="select-result" role="button" class="linkColor"> <li>Update Result</li></a>

                <br/><br>  <ul class="nav"><strong>VIEW</strong></ul>
                <a href="view-students" role="button" class="linkColor"> <li>View Student</li></a>
                <a href="view-invoices" role="button" class="linkColor"> <li>View Invoice</li></a>
                <a href="view-timetable"  onclick="history.go(-1); return false;" role="button" class="linkColor"> <li>View TimeTable</li></a>
                <a href="view-results" role="button" class="linkColor"> <li>View Result</li></a>
                <br/><br><br/><br/>
            </div>
        </div>
        <div class="col-md-10">
            <!-- Header -->
            <header>
                <div class="row">
                    <div class=" navHeader col-md-12">
                        <p class="appName"><strong>PORTRAY</strong>
                            <span style="color: white; float: right; font-size: 10px; margin-top: 10px;"><strong>
                                    @if (Auth::guest())
                                        <span><a href="{{ route('login') }}">Login</a></span>
                                        <span><a href="{{ route('register') }}">Register</a></span>
                                    @else
                                        <span class="dropdown">
                                            <a style="color: white;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <span class="dropdown-menu" role="menu">
                                                <span>
                                                    <a style="font-size: 10px;" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </span>
                                            </span>
                                        </span>
                                    @endif
                                </strong></span>
                        </p>

                    </div>
                </div>
            </header>
            <div class="panel panel-default">
                <div class="panel-heading" align="center"> View Timetable </div>

                    <div align="center" style="line-height: 10px;">
                        <p style="margin-top: 5px;">REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</p>
                        <p><strong>{{$timetable->tSchool}}</strong></p>
                        <p><strong>{{$timetable->tProgramme}}</strong></p>
                        <p><strong>McCARTHY HILL CAMPUS</strong></p>
                        <p><strong>{{$timetable->tSemester}} TEACHING TIME TABLE</strong></p>
                        <p><strong>LEVEL {{$timetable->tLevel}}-{{$timetable->tStream}} STREAM</strong></p>
                    </div>
                    <table class="table" style="border: 2px solid black; color: black; margin-top: 10px; width: 100%;">

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">Day/weak</th>
                            <th style="border: 2px solid black">8.30AM - 11.15AM</th>
                            <th style="border: 2px solid black">11.15AM - <br>11.30AM</th>
                            <th style="border: 2px solid black">11.30AM - 2.15AM</th>
                            <th style="border: 2px solid black">2.15AM - <br>2.30AM </th>
                            <th style="border: 2px solid black">2.30AM - 5.15AM</th>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->monday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1mc}} <br> {{$timetable->course1mn}} <br> {{$timetable->course1ml}} <br> {{$timetable->course1mnv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">B </td>
                            <td style="border: 2px solid black">{{$timetable->course2mc}} <br> {{$timetable->course2mn}} <br> {{$timetable->course2ml}} <br> {{$timetable->course2mnv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">B </td>
                            <td style="border: 2px solid black">{{$timetable->course3mc}} <br> {{$timetable->course3mn}} <br> {{$timetable->course3ml}} <br> {{$timetable->course3mnv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->tuesday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1tc}} <br> {{$timetable->course1tn}} <br> {{$timetable->course1tl}} <br> {{$timetable->course1tv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">R</td>
                            <td style="border: 2px solid black">{{$timetable->course2tc}} <br> {{$timetable->course2tn}} <br> {{$timetable->course2tl}} <br> {{$timetable->course2tv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">R</td>
                            <td style="border: 2px solid black">{{$timetable->course3tc}} <br> {{$timetable->course3tn}} <br> {{$timetable->course3tl}} <br> {{$timetable->course3tv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->wednesday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1wc}} <br> {{$timetable->course1wn}} <br> {{$timetable->course1wl}} <br> {{$timetable->course1wv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">E</td>
                            <td style="border: 2px solid black">{{$timetable->course2wc}} <br> {{$timetable->course2wn}} <br> {{$timetable->course2wl}} <br> {{$timetable->course2wv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">E</td>
                            <td style="border: 2px solid black">{{$timetable->course3wc}} <br> {{$timetable->course3wn}} <br> {{$timetable->course3wl}} <br> {{$timetable->course3wv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->thursday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1thc}} <br> {{$timetable->course1thn}} <br> {{$timetable->course1thl}} <br> {{$timetable->course1thv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">A</td>
                            <td style="border: 2px solid black">{{$timetable->course2thc}} <br> {{$timetable->course2thn}} <br> {{$timetable->course2thl}} <br> {{$timetable->course2thv}}</td>
                            <td style="border-bottom: 2px solid white; text-align: center;">A</td>
                            <td style="border: 2px solid black">{{$timetable->course3thc}} <br> {{$timetable->course3thn}} <br> {{$timetable->course3thl}} <br> {{$timetable->course3thv}}</td>
                        </tr>

                        <tr style="border: 2px solid black">
                            <th style="border: 2px solid black">{{$timetable->friday}}</th>
                            <td style="border: 2px solid black">{{$timetable->course1fc}} <br> {{$timetable->course1fn}} <br> {{$timetable->course1fl}} <br> {{$timetable->course1fv}}</td>
                            <td style="border: 2px solid black; text-align: center;">K</td>
                            <td style="border: 2px solid black">{{$timetable->course2fc}} <br> {{$timetable->course2fn}} <br> {{$timetable->course2fl}} <br> {{$timetable->course2fv}}</td>
                            <td style="border: 2px solid black; text-align: center;">K</td>
                            <td style="border: 2px solid black">{{$timetable->course3fc}} <br> {{$timetable->course3fn}} <br> {{$timetable->course3fl}} <br> {{$timetable->course3fv}}</td>
                        </tr>
                    </table>
                <a href="{{url('/generate-pdf-timetable/'.$timetable->tid)}}" class="btn btn-primary" role="button" style="float: right; margin-top: 10px; margin-bottom: 10px;">
                    Generate PDF
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
