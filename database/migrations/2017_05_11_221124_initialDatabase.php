<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student',function(Blueprint $table){
            $table->increments('sida');
            $table->string('sid')->unique();
            $table->integer('tid')->nullable();
            $table->text('sFname');
            $table->text('sSname');
            $table->text('sOname');
            $table->string('sEmail')->unique();
            $table->text('spassword');
            $table->string('phone')->unique();
            $table->integer('sLevel');
            $table->text('programme');
            $table->text('sSchool');
            $table->text('stream');
            $table->text('nationality');
            $table->timestamps();
        });

        Schema::create('result',function(Blueprint $table){
            $table->increments('rid');
            $table->integer('sida');
            // Level 1
            $table->integer('rLevel1')->nullable();
            $table->text('rSession1')->nullable();
            //Semester 1
            $table->text('rSemester11')->nullable();
            //Course 1
            $table->string('rCourseCode111')->nullable();
            $table->string('rCourseName111')->nullable();
            $table->decimal('mark111')->nullable();
            $table->char('grade111',1)->nullable();
            $table->integer('cr111')->nullable();
            //Course 2
            $table->string('rCourseCode112')->nullable();
            $table->string('rCourseName112')->nullable();
            $table->decimal('mark112')->nullable();
            $table->char('grade112',1)->nullable();
            $table->integer('cr112')->nullable();
            //Course 3
            $table->string('rCourseCode113')->nullable();
            $table->string('rCourseName113')->nullable();
            $table->decimal('mark113')->nullable();
            $table->char('grade113',1)->nullable();
            $table->integer('cr113')->nullable();
            //Course 4
            $table->text('rCourseCode114')->nullable();
            $table->text('rCourseName114')->nullable();
            $table->decimal('mark114')->nullable();
            $table->char('grade114',1)->nullable();
            $table->integer('cr114')->nullable();
            //Course 5
            $table->text('rCourseCode115')->nullable();
            $table->text('rCourseName115')->nullable();
            $table->decimal('mark115')->nullable();
            $table->char('grade115',1)->nullable();
            $table->integer('cr115')->nullable();
            //Course 6
            $table->text('rCourseCode116')->nullable();
            $table->text('rCourseName116')->nullable();
            $table->decimal('mark116')->nullable();
            $table->char('grade116',1)->nullable();
            $table->integer('cr116')->nullable();
            //swa for semester one
            $table->decimal('rSwa11')->nullable();
            //Semester 2
            $table->text('rSemester12')->nullable();
            //Course 1
            $table->text('rCourseCode121')->nullable();
            $table->text('rCourseName121')->nullable();
            $table->decimal('mark121')->nullable();
            $table->char('grade121',1)->nullable();
            $table->integer('cr121')->nullable();
            //Course 2
            $table->text('rCourseCode122')->nullable();
            $table->text('rCourseName122')->nullable();
            $table->decimal('mark122')->nullable();
            $table->char('grade122',1)->nullable();
            $table->integer('cr122')->nullable();
            //Course 3
            $table->text('rCourseCode123')->nullable();
            $table->text('rCourseName123')->nullable();
            $table->decimal('mark123')->nullable();
            $table->char('grade123',1)->nullable();
            $table->integer('cr123')->nullable();
            //Course 4
            $table->text('rCourseCode124')->nullable();
            $table->text('rCourseName124')->nullable();
            $table->decimal('mark124')->nullable();
            $table->char('grade124',1)->nullable();
            $table->integer('cr124')->nullable();
            //Course 5
            $table->text('rCourseCode125')->nullable();
            $table->text('rCourseName125')->nullable();
            $table->decimal('mark125')->nullable();
            $table->char('grade125',1)->nullable();
            $table->integer('cr125')->nullable();
            //Course 6
            $table->text('rCourseCode126')->nullable();
            $table->text('rCourseName126')->nullable();
            $table->decimal('mark126')->nullable();
            $table->char('grade126',1)->nullable();
            $table->integer('cr126')->nullable();
            //swa for semester Two
            $table->decimal('rSwa12')->nullable();

            // Level 2
            $table->integer('rLevel2')->nullable();
            $table->text('rSession2')->nullable();
            //Semester 1
            $table->text('rSemester21')->nullable();
            //Course 1
            $table->text('rCourseCode211')->nullable();
            $table->text('rCourseName211')->nullable();
            $table->decimal('mark211')->nullable();
            $table->char('grade211',1)->nullable();
            $table->integer('cr211')->nullable();
            //Course 2
            $table->text('rCourseCode212')->nullable();
            $table->text('rCourseName212')->nullable();
            $table->decimal('mark212')->nullable();
            $table->char('grade212',1)->nullable();
            $table->integer('cr212')->nullable();
            //Course 3
            $table->text('rCourseCode213')->nullable();
            $table->text('rCourseName213')->nullable();
            $table->decimal('mark213')->nullable();
            $table->char('grade213',1)->nullable();
            $table->integer('cr213')->nullable();
            //Course 4
            $table->text('rCourseCode214')->nullable();
            $table->text('rCourseName214')->nullable();
            $table->decimal('mark214')->nullable();
            $table->char('grade214',1)->nullable();
            $table->integer('cr214')->nullable();
            //Course 5
            $table->text('rCourseCode215')->nullable();
            $table->text('rCourseName215')->nullable();
            $table->decimal('mark215')->nullable();
            $table->char('grade215',1)->nullable();
            $table->integer('cr215')->nullable();
            //Course 6
            $table->text('rCourseCode216')->nullable();
            $table->text('rCourseName216')->nullable();
            $table->decimal('mark216')->nullable();
            $table->char('grade216',1)->nullable();
            $table->integer('cr216')->nullable();
            //swa for semester one
            $table->decimal('rSwa21')->nullable();
            //Semester 2
            $table->text('rSemester22')->nullable();
            //Course 1
            $table->text('rCourseCode221')->nullable();
            $table->text('rCourseName221')->nullable();
            $table->decimal('mark221')->nullable();
            $table->char('grade221',1)->nullable();
            $table->integer('cr221')->nullable();
            //Course 2
            $table->text('rCourseCode222')->nullable();
            $table->text('rCourseName222')->nullable();
            $table->decimal('mark222')->nullable();
            $table->char('grade222',1)->nullable();
            $table->integer('cr222')->nullable();
            //Course 3
            $table->text('rCourseCode223')->nullable();
            $table->text('rCourseName223')->nullable();
            $table->decimal('mark223')->nullable();
            $table->char('grade223',1)->nullable();
            $table->integer('cr223')->nullable();
            //Course 4
            $table->text('rCourseCode224')->nullable();
            $table->text('rCourseName224')->nullable();
            $table->decimal('mark224')->nullable();
            $table->char('grade224',1)->nullable();
            $table->integer('cr224')->nullable();
            //Course 5
            $table->text('rCourseCode225')->nullable();
            $table->text('rCourseName225')->nullable();
            $table->decimal('mark225')->nullable();
            $table->char('grade225',1)->nullable();
            $table->integer('cr225')->nullable();
            //Course 6
            $table->text('rCourseCode226')->nullable();
            $table->text('rCourseName226')->nullable();
            $table->decimal('mark226')->nullable();
            $table->char('grade226',1)->nullable();
            $table->integer('cr226')->nullable();
            //swa for semester two
            $table->decimal('rSwa22')->nullable();
            // Level 3
            $table->integer('rLevel3')->nullable();
            $table->text('rSession3')->nullable();
            //Semester 1
            $table->text('rSemester31')->nullable();
            //Course 1
            $table->text('rCourseCode311')->nullable();
            $table->text('rCourseName311')->nullable();
            $table->decimal('mark311')->nullable();
            $table->char('grade311',1)->nullable();
            $table->integer('cr311')->nullable();
            //Course 2
            $table->text('rCourseCode312')->nullable();
            $table->text('rCourseName312')->nullable();
            $table->decimal('mark312')->nullable();
            $table->char('grade312',1)->nullable();
            $table->integer('cr312')->nullable();
            //Course 3
            $table->text('rCourseCode313')->nullable();
            $table->text('rCourseName313')->nullable();
            $table->decimal('mark313')->nullable();
            $table->char('grade313',1)->nullable();
            $table->integer('cr313')->nullable();
            //Course 4
            $table->text('rCourseCode314')->nullable();
            $table->text('rCourseName314')->nullable();
            $table->decimal('mark314')->nullable();
            $table->char('grade314',1)->nullable();
            $table->integer('cr314')->nullable();
            //Course 5
            $table->text('rCourseCode315')->nullable();
            $table->text('rCourseName315')->nullable();
            $table->decimal('mark315')->nullable();
            $table->char('grade315',1)->nullable();
            $table->integer('cr315')->nullable();
            //Course 6
            $table->text('rCourseCode316')->nullable();
            $table->text('rCourseName316')->nullable();
            $table->decimal('mark316')->nullable();
            $table->char('grade316',1)->nullable();
            $table->integer('cr316')->nullable();
            //swa for semester one
            $table->decimal('rSwa31')->nullable();
            //Semester 2
            $table->text('rSemester32')->nullable();
            //Course 1
            $table->text('rCourseCode321')->nullable();
            $table->text('rCourseName321')->nullable();
            $table->decimal('mark321')->nullable();
            $table->char('grade321',1)->nullable();
            $table->integer('cr321')->nullable();
            //Course 2
            $table->text('rCourseCode322')->nullable();
            $table->text('rCourseName322')->nullable();
            $table->decimal('mark322')->nullable();
            $table->char('grade322',1)->nullable();
            $table->integer('cr322')->nullable();
            //Course 3
            $table->text('rCourseCode323')->nullable();
            $table->text('rCourseName323')->nullable();
            $table->decimal('mark323')->nullable();
            $table->char('grade323',1)->nullable();
            $table->integer('cr323')->nullable();
            //Course 4
            $table->text('rCourseCode324')->nullable();
            $table->text('rCourseName324')->nullable();
            $table->decimal('mark324')->nullable();
            $table->char('grade324',1)->nullable();
            $table->integer('cr324')->nullable();
            //Course 53
            $table->text('rCourseCode325')->nullable();
            $table->text('rCourseName325')->nullable();
            $table->decimal('mark325')->nullable();
            $table->char('grade325',1)->nullable();
            $table->integer('cr325')->nullable();
            //Course 6
            $table->text('rCourseCode326')->nullable();
            $table->text('rCourseName326')->nullable();
            $table->decimal('mark326')->nullable();
            $table->char('grade326',1)->nullable();
            $table->integer('cr326')->nullable();
            //swa for semester Two
            $table->decimal('rSwa32')->nullable();

            // Level 4
            $table->integer('rLevel4')->nullable();
            $table->text('rSession4')->nullable();
            //Semester 1
            $table->text('rSemester41')->nullable();
            //Course 1
            $table->text('rCourseCode411')->nullable();
            $table->text('rCourseName411')->nullable();
            $table->decimal('mark411')->nullable();
            $table->char('grade411',1)->nullable();
            $table->integer('cr411')->nullable();
            //Course 2
            $table->text('rCourseCode412')->nullable();
            $table->text('rCourseName412')->nullable();
            $table->decimal('mark412')->nullable();
            $table->char('grade412',1)->nullable();
            $table->integer('cr412')->nullable();
            //Course 3
            $table->text('rCourseCode413')->nullable();
            $table->text('rCourseName413')->nullable();
            $table->decimal('mark413')->nullable();
            $table->char('grade413',1)->nullable();
            $table->integer('cr413')->nullable();
            //Course 4
            $table->text('rCourseCode414')->nullable();
            $table->text('rCourseName414')->nullable();
            $table->decimal('mark414')->nullable();
            $table->char('grade414',1)->nullable();
            $table->integer('cr414')->nullable();
            //Course 5
            $table->text('rCourseCode415')->nullable();
            $table->text('rCourseName415')->nullable();
            $table->decimal('mark415')->nullable();
            $table->char('grade415',1)->nullable();
            $table->integer('cr415')->nullable();
            //Course 6
            $table->text('rCourseCode416')->nullable();
            $table->text('rCourseName416')->nullable();
            $table->decimal('mark416')->nullable();
            $table->char('grade416',1)->nullable();
            $table->integer('cr416')->nullable();
            //swa for semester one
            $table->decimal('rSwa41')->nullable();
            //Semester 2
            $table->text('rSemester42')->nullable();
            //Course 1
            $table->text('rCourseCode421')->nullable();
            $table->text('rCourseName421')->nullable();
            $table->decimal('mark421')->nullable();
            $table->char('grade421',1)->nullable();
            $table->integer('cr421')->nullable();
            //Course 2
            $table->text('rCourseCode422')->nullable();
            $table->text('rCourseName422')->nullable();
            $table->decimal('mark422')->nullable();
            $table->char('grade422',1)->nullable();
            $table->integer('cr422')->nullable();
            //Course 3
            $table->text('rCourseCode423')->nullable();
            $table->text('rCourseName423')->nullable();
            $table->decimal('mark423')->nullable();
            $table->char('grade423',1)->nullable();
            $table->integer('cr423')->nullable();
            //Course 4
            $table->text('rCourseCode424')->nullable();
            $table->text('rCourseName424')->nullable();
            $table->decimal('mark424')->nullable();
            $table->char('grade424',1)->nullable();
            $table->integer('cr424')->nullable();
            //Course 5
            $table->text('rCourseCode425')->nullable();
            $table->text('rCourseName425')->nullable();
            $table->decimal('mark425')->nullable();
            $table->char('grade425',1)->nullable();
            $table->integer('cr425')->nullable();
            //Course 6
            $table->text('rCourseCode426')->nullable();
            $table->text('rCourseName426')->nullable();
            $table->decimal('mark426')->nullable();
            $table->char('grade426',1)->nullable();
            $table->integer('cr426')->nullable();
            //SWA for 400 level second semester
            $table->decimal('rSwa42')->nullable();
            //CWA
            $table->decimal('rCwa');
            $table->timestamps();
        });

        Schema::create('invoice',function(Blueprint $table){
            $table->increments('invid');
            $table->integer('sida');
            $table->integer('iLevel');
            $table->integer('iCLevel');
            $table->text('iSemester');
            $table->text('iCSemester');
            $table->decimal('iFeesPayable');
            $table->decimal('iCFeesPayable');
            $table->decimal('iTotal');
            $table->text('status');
            $table->timestamps();
        });

        Schema::create('timetable',function(Blueprint $table){
            $table->increments('tid');
            $table->text('tSchool');
            $table->text('tProgramme');
            $table->text('tSemester');
            $table->text('tStream');
            $table->text('tLevel');

            $table->text('monday');

            $table->text('course1mc')->nullable();
            $table->text('course1mn')->nullable();
            $table->text('course1ml')->nullable();
            $table->text('course1mv')->nullable();

            $table->text('course2mc')->nullable();
            $table->text('course2mn')->nullable();
            $table->text('course2ml')->nullable();
            $table->text('course2mv')->nullable();

            $table->text('course3mc')->nullable();
            $table->text('course3mn')->nullable();
            $table->text('course3ml')->nullable();
            $table->text('course3mv')->nullable();

            $table->text('tuesday');

            $table->text('course1tc')->nullable();
            $table->text('course1tn')->nullable();
            $table->text('course1tl')->nullable();
            $table->text('course1tv')->nullable();

            $table->text('course2tc')->nullable();
            $table->text('course2tn')->nullable();
            $table->text('course2tl')->nullable();
            $table->text('course2tv')->nullable();

            $table->text('course3tc')->nullable();
            $table->text('course3tn')->nullable();
            $table->text('course3tl')->nullable();
            $table->text('course3tv')->nullable();

            $table->text('wednesday');

            $table->text('course1wc')->nullable();
            $table->text('course1wn')->nullable();
            $table->text('course1wl')->nullable();
            $table->text('course1wv')->nullable();

            $table->text('course2wc')->nullable();
            $table->text('course2wn')->nullable();
            $table->text('course2wl')->nullable();
            $table->text('course2wv')->nullable();

            $table->text('course3wc')->nullable();
            $table->text('course3wn')->nullable();
            $table->text('course3wl')->nullable();
            $table->text('course3wv')->nullable();

            $table->text('thursday');

            $table->text('course1thc')->nullable();
            $table->text('course1thn')->nullable();
            $table->text('course1thl')->nullable();
            $table->text('course1thv')->nullable();

            $table->text('course2thc')->nullable();
            $table->text('course2thn')->nullable();
            $table->text('course2thl')->nullable();
            $table->text('course2thv')->nullable();

            $table->text('course3thc')->nullable();
            $table->text('course3thn')->nullable();
            $table->text('course3thl')->nullable();
            $table->text('course3thv')->nullable();

            $table->text('friday');

            $table->text('course1fc')->nullable();
            $table->text('course1fn')->nullable();
            $table->text('course1fl')->nullable();
            $table->text('course1fv')->nullable();

            $table->text('course2fc')->nullable();
            $table->text('course2fn')->nullable();
            $table->text('course2fl')->nullable();
            $table->text('course2fv')->nullable();

            $table->text('course3fc')->nullable();
            $table->text('course3fn')->nullable();
            $table->text('course3fl')->nullable();
            $table->text('course3fv')->nullable();

            $table->timestamps();
        });

//        Schema::create('lecturer',function(Blueprint $table){
//            $table->increments('lida');
//            $table->text('lFname');
//            $table->text('lSname');
//            $table->text('lOname');
//            $table->integer('cid');
//            $table->timestamps();
//        });
//
//        Schema::create('course',function(Blueprint $table){
//            $table->increments('cid');
//            $table->text('cName');
//            $table->text('cCode');
//            $table->text('semester');
//            $table->integer('cLevel');
//            $table->timestamps();
//        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('student');
        Schema::dropIfExists('result');
        Schema::dropIfExists('invoice');
        Schema::dropIfExists('timetable');
//        Schema::dropIfExists('lecturer');
//        Schema::dropIfExists('course');
    }
}
