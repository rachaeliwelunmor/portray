<?php

namespace PORTRAY;

use Illuminate\Database\Eloquent\Model;

class timetable extends Model
{
    //primary key -- tid
    protected  $primaryKey = 'tid';

    public $table = 'timetable';

    public function student ()
    {
        return $this->hasMany('student');
    }
}
