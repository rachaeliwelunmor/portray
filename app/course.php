<?php

namespace PORTRAY;

use Illuminate\Database\Eloquent\Model;

class course extends Model
{
    //primary key -- cid
    protected  $primaryKey = 'cid';

    public $table = 'course';
}
