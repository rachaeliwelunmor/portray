<?php

namespace PORTRAY;

use Illuminate\Database\Eloquent\Model;

class result extends Model
{
    //primary key -- rid
    protected  $primaryKey = 'rid';

    public $table = 'result';

    public function student ()
    {
        return $this->belongsTo('PORTRAY\student','sida');
    }
}
