<?php

namespace PORTRAY;

use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    //primary key -- invid
    protected  $primaryKey = 'invid';

    public $table = 'invoice';


    public function student()
{
    return $this->belongsTo('PORTRAY\student','sida');
}
}
