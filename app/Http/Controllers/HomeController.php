<?php

namespace PORTRAY\Http\Controllers;

use Carbon\Carbon;
use Dompdf\Dompdf;
use Exception;
use PORTRAY\course;
use PORTRAY\invoice;
use PORTRAY\lecturer;
use PORTRAY\result;
use PORTRAY\student;
use PORTRAY\timetable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

//----------------STUDENT FUNCTIONS------------------
    /* Adding students to the DB*/
    public function getAddStudent()
    {
        return view( 'addStudent');
    }

    /* Viewing all the students in the DB*/
    public function getViewStudents()
    {
        $students = student::all();
        return view('viewStudent',[
            'students' => $students
        ]);

    }

    public function getViewStudent($sida)
    {
        $student = student::find($sida);
        return view('viewStudents',[
            'student' => $student
        ]);
    }

    /* selecting a student in the DB*/
    public function getSelectStudent()
    {
        $students = student::all();
        return view('selectStudent',[
            'students' => $students
        ]);
    }

    public function getStudentReport()
    {
        return view('studentReport');
    }

    /* Adding students to the DB*/
    public function postAddStudent(Request $request)
    {
        $student = new student();
        $student->sFname = $request->input('sFname');
        $student->sSname = $request->input('sSname');
        $student->sOname = $request->input('sOname');
        $student->sid    = $request->input('sid');
        $student->spassword = $request->input('spassword');
        $student->sEmail = $request->input('sEmail');
        $student->phone = $request->input('phone');
        $student->sLevel = $request->input('sLevel');
        $student->programme = $request->input('programme');
        $student->sSchool = $request->input('sSchool');
        $student->stream = $request->input('stream');
        $student->nationality = $request->input('nationality');
        $student->tid = $request->input('tid');
        $status =  $student->save();  // saves the record

        if($status)
            $request->session()->flash('success','Student added successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/add-student');
    }

    /* Adding students to the DB*/
    public function postUpdateStudent(Request $request)
    {
        $student = student::find($request->input('sida'));
        $student->sFname = $request->input('sFname');
        $student->sSname = $request->input('sSname');
        $student->sOname = $request->input('sOname');
        $student->sid    = $request->input('sid');
        $student->spassword = $request->input('spassword');
        $student->sEmail = $request->input('sEmail');
        $student->phone = $request->input('phone');
        $student->sLevel = $request->input('sLevel');
        $student->programme = $request->input('programme');
        $student->sSchool = $request->input('sSchool');
        $student->stream = $request->input('stream');
        $student->nationality = $request->input('nationality');
        $student->tid = $request->input('tid');
        $status =  $student->save();  // saves the record

        if($status)
            $request->session()->flash('success','Student updated successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/update-student/'.$student->sida);
    }


//----------------TIMETABLE FUNCTIONS------------------
    /* Adding timetables to the DB*/
    public function getAddTimetable()
    {
        return view( 'addTimetable');
    }

    /* Viewing all the timetables in the DB*/
    public function getViewTimetable()
    {
        $timetables = timetable::all();
        return view('viewTimetable',[
            'timetables' => $timetables
        ]);

    }

    public function getViewTimetables($tid)
    {
        $timetable = timetable::find($tid);
        return view('viewTimetables',[
            'timetable' => $timetable
        ]);
    }

    public function getViewTimetablePlain($tid)
    {
        $timetable = timetable::find($tid);
        return view('viewTimetablePlain',[
            'timetable' => $timetable
        ]);
    }

    /* Selecting a timetable in the DB*/
    public function getSelectTimetable()
    {
        $timetables = timetable::all();
        return view('selectTimetable',[
            'timetables' => $timetables
        ]);
    }

    public function getTimetableReport()
    {
        return view('timetableReport');
    }

    /* Adding timetable to the DB*/
    public function getPostTimetable(Request $request)
    {
        $timetable = new timetable();
        $timetable->tSchool = $request->input('tSchool');
        $timetable->tProgramme = $request->input('tProgramme');
        $timetable->tSemester = $request->input('tSemester');
        $timetable->tStream = $request->input('tStream');
        $timetable->tLevel = $request->input('tLevel');

        $timetable->monday = $request->input('monday');

        $timetable->course1mc = $request->input('course1mc');
        $timetable->course1mn = $request->input('course1mn');
        $timetable->course1ml = $request->input('course1ml');
        $timetable->course1mv = $request->input('course1mc');

        $timetable->course2mc = $request->input('course2mc');
        $timetable->course2mn = $request->input('course2mn');
        $timetable->course2ml = $request->input('course2ml');
        $timetable->course2mv = $request->input('course2mv');

        $timetable->course3mc = $request->input('course3mc');
        $timetable->course3mn = $request->input('course3mn');
        $timetable->course3ml = $request->input('course3ml');
        $timetable->course3mv = $request->input('course3mv');

        $timetable->tuesday = $request->input('tuesday');

        $timetable->course1tc = $request->input('course1tc');
        $timetable->course1tn = $request->input('course1tn');
        $timetable->course1tl = $request->input('course1tl');
        $timetable->course1tv = $request->input('course1tv');

        $timetable->course2tc = $request->input('course2tc');
        $timetable->course2tn = $request->input('course2tn');
        $timetable->course2tl = $request->input('course2tl');
        $timetable->course2tv = $request->input('course2tv');

        $timetable->course3tc = $request->input('course3tc');
        $timetable->course3tn = $request->input('course3tn');
        $timetable->course3tl = $request->input('course3tl');
        $timetable->course3tv = $request->input('course3tv');

        $timetable->wednesday = $request->input('wednesday');

        $timetable->course1wc = $request->input('course1wc');
        $timetable->course1wn = $request->input('course1wn');
        $timetable->course1wl = $request->input('course1wl');
        $timetable->course1wv = $request->input('course1wv');

        $timetable->course2wc = $request->input('course2wc');
        $timetable->course2wn = $request->input('course2wn');
        $timetable->course2wl = $request->input('course2wl');
        $timetable->course2wv = $request->input('course2wv');

        $timetable->course3wc = $request->input('course3wc');
        $timetable->course3wn = $request->input('course3wn');
        $timetable->course3wl = $request->input('course3wl');
        $timetable->course3wv = $request->input('course3wv');

        $timetable->thursday = $request->input('thursday');

        $timetable->course1thc = $request->input('course1thc');
        $timetable->course1thn = $request->input('course1thn');
        $timetable->course1thl = $request->input('course1thl');
        $timetable->course1thv = $request->input('course1thv');

        $timetable->course2thc = $request->input('course2thc');
        $timetable->course2thn = $request->input('course2thn');
        $timetable->course2thl = $request->input('course2thl');
        $timetable->course2thv = $request->input('course2thv');

        $timetable->course3thc = $request->input('course3thc');
        $timetable->course3thn = $request->input('course3thn');
        $timetable->course3thl = $request->input('course3thl');
        $timetable->course3thv = $request->input('course3thv');

        $timetable->friday = $request->input('friday');

        $timetable->course1fc = $request->input('course1fc');
        $timetable->course1fn = $request->input('course1fn');
        $timetable->course1fl = $request->input('course1fl');
        $timetable->course1fv = $request->input('course1fv');

        $timetable->course2fc = $request->input('course2fc');
        $timetable->course2fn = $request->input('course2fn');
        $timetable->course2fl = $request->input('course2fl');
        $timetable->course2fv = $request->input('course2fv');

        $timetable->course3fc = $request->input('course3fc');
        $timetable->course3fn = $request->input('course3fn');
        $timetable->course3fl = $request->input('course3fl');
        $timetable->course3fv = $request->input('course3fv');

        $status =  $timetable->save();  // saves the record

        if($status)
            $request->session()->flash('success','timetable added successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/add-timetable');
    }

    /* updating a particular timetable to the DB*/
    public function postUpdateTimetable($tid, Request $request)
    {
        $timetable = timetable::find($tid);

        $timetable->tSchool = $request->input('tSchool');
        $timetable->tProgramme = $request->input('tProgramme');
        $timetable->tSemester = $request->input('tSemester');
        $timetable->tStream = $request->input('tStream');
        $timetable->tLevel = $request->input('tLevel');

        $timetable->monday = $request->input('monday');

        $timetable->course1mc = $request->input('course1mc');
        $timetable->course1mn = $request->input('course1mn');
        $timetable->course1ml = $request->input('course1ml');
        $timetable->course1mv = $request->input('course1mc');

        $timetable->course2mc = $request->input('course2mc');
        $timetable->course2mn = $request->input('course2mn');
        $timetable->course2ml = $request->input('course2ml');
        $timetable->course2mv = $request->input('course2mv');

        $timetable->course3mc = $request->input('course3mc');
        $timetable->course3mn = $request->input('course3mn');
        $timetable->course3ml = $request->input('course3ml');
        $timetable->course3mv = $request->input('course3mv');

        $timetable->tuesday = $request->input('tuesday');

        $timetable->course1tc = $request->input('course1tc');
        $timetable->course1tn = $request->input('course1tn');
        $timetable->course1tl = $request->input('course1tl');
        $timetable->course1tv = $request->input('course1tv');

        $timetable->course2tc = $request->input('course2tc');
        $timetable->course2tn = $request->input('course2tn');
        $timetable->course2tl = $request->input('course2tl');
        $timetable->course2tv = $request->input('course2tv');

        $timetable->course3tc = $request->input('course3tc');
        $timetable->course3tn = $request->input('course3tn');
        $timetable->course3tl = $request->input('course3tl');
        $timetable->course3tv = $request->input('course3tv');

        $timetable->wednesday = $request->input('wednesday');

        $timetable->course1wc = $request->input('course1wc');
        $timetable->course1wn = $request->input('course1wn');
        $timetable->course1wl = $request->input('course1wl');
        $timetable->course1wv = $request->input('course1wv');

        $timetable->course2wc = $request->input('course2wc');
        $timetable->course2wn = $request->input('course2wn');
        $timetable->course2wl = $request->input('course2wl');
        $timetable->course2wv = $request->input('course2wv');

        $timetable->course3wc = $request->input('course3wc');
        $timetable->course3wn = $request->input('course3wn');
        $timetable->course3wl = $request->input('course3wl');
        $timetable->course3wv = $request->input('course3wv');

        $timetable->thursday = $request->input('thursday');

        $timetable->course1thc = $request->input('course1thc');
        $timetable->course1thn = $request->input('course1thn');
        $timetable->course1thl = $request->input('course1thl');
        $timetable->course1thv = $request->input('course1thv');

        $timetable->course2thc = $request->input('course2thc');
        $timetable->course2thn = $request->input('course2thn');
        $timetable->course2thl = $request->input('course2thl');
        $timetable->course2thv = $request->input('course2thv');

        $timetable->course3thc = $request->input('course3thc');
        $timetable->course3thn = $request->input('course3thn');
        $timetable->course3thl = $request->input('course3thl');
        $timetable->course3thv = $request->input('course3thv');

        $timetable->friday = $request->input('friday');

        $timetable->course1fc = $request->input('course1fc');
        $timetable->course1fn = $request->input('course1fn');
        $timetable->course1fl = $request->input('course1fl');
        $timetable->course1fv = $request->input('course1fv');

        $timetable->course2fc = $request->input('course2fc');
        $timetable->course2fn = $request->input('course2fn');
        $timetable->course2fl = $request->input('course2fl');
        $timetable->course2fv = $request->input('course2fv');

        $timetable->course3fc = $request->input('course3fc');
        $timetable->course3fn = $request->input('course3fn');
        $timetable->course3fl = $request->input('course3fl');
        $timetable->course3fv = $request->input('course3fv');

        $status =  $timetable->save();  // saves the record

        if($status)
            $request->session()->flash('success','timetable Updated successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/update-timetable/'. $timetable->tid);
    }

    public function getGenerateTimetablePdf($tid)
    {
        try {
            // instantiate and use the dompdf class
            $dompdf = new Dompdf();
            $dompdf->loadHtml($this->getViewTimetablePlain($tid));

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A3', 'portrait');
            $dompdf->set_option('isRemoteEnabled', 'true');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream("Student_Timetable_" . Carbon::now()->toDateString());
        }
        catch(Exception $e){
            echo $e->getMessage();
            echo "Something went wrong. Please try again.";
        }
    }

//----------------INVOICE FUNCTIONS------------------
    /* Adding Invoices to the DB*/
    public function getAddInvoice()
    {
        return view( 'addInvoice');
    }

    /* Viewing all the invoices in the DB*/
    public function getViewInvoices()
    {
        $invoices = invoice::all();

        $cleared = 0;
        $uncleared = 0;

        foreach($invoices as $item){
            if($item->status == "Not Cleared"){
                $cleared++;
            } else {
                $uncleared++;
            }
        }
        return view('viewInvoices',[
            'invoices' => $invoices,
            'cleared' => $cleared,
            'uncleared' => $uncleared
        ]);

    }

    public function getViewInvoice($invid)
    {
        $invoice = invoice::find($invid);
        return view('viewInvoice',[
            'invoice' => $invoice
        ]);
    }

    public function getSelectInvoice()
    {
        $invoices = invoice::all();
        return view('selectInvoice',[
            'invoices' => $invoices
        ]);
    }

    public function getViewClearedInv()
    {
        $invoices = invoice::all();
        $clearedinv = array();
        foreach($invoices as $item)
        {
            if ($item->status == "Cleared"){
                array_push($clearedinv,$item);
            }
        }
        return view('viewClearedInv',[
            'invoices' => $clearedinv
        ]);
    }

    public function getViewUnclearedInv()
    {
        $invoices = invoice::all();
        $unclearedinv = array();
        foreach($invoices as $item)
        {
            if ($item->status == "Not cleared"){
                array_push($unclearedinv,$item);
            }
        }
        return view('viewUnclearedInv',[
            'invoices' => $unclearedinv
        ]);
    }

    /* Adding Invoices to the DB*/
    public function getPostInvoice(Request $request)
    {
        $invoice = new invoice();
        $invoice->sida = $request->input('sida');
        $invoice->iLevel = $request->input('iLevel');
        $invoice->iCLevel = $request->input('iCLevel');
        $invoice->iSemester = $request->input('iSemester');
        $invoice->iCSemester = $request->input('iCSemester');
        $invoice->iFeesPayable = $request->input('iFeesPayable');
        $invoice->iCFeesPayable = $request->input('iCFeesPayable');
        $invoice->iTotal    = $request->input('iFeesPayable') + $request->input('iCFeesPayable');

        if($invoice->iTotal != 0)
            $invoice->status = ('Not cleared');
        else
            $invoice->status = ('Cleared');

        $status =  $invoice->save();  // saves the record

        if($status)
            $request->session()->flash('success','Invoice added successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/add-invoice');
    }

    /*generating the Invoice pdf*/
    public function getGenerateInvoicePdf($invid)
    {
        try {
            // instantiate and use the dompdf class
            $dompdf = new Dompdf();
            $dompdf->loadHtml($this->getViewInvoicePlain($invid));

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->set_option('isRemoteEnabled', 'true');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream("Student_Invoice_" . Carbon::now()->toDateString());
        }
        catch(Exception $e){
            echo $e->getMessage();
            echo "Something went wrong. Please try again.";
        }
    }

    public function getViewInvoicePlain($invid)
    {
        $invoice = invoice::find($invid);
        return view('viewInvoicePlain',[
            'invoice' => $invoice
        ]);
    }

    public function postUpdateInvoice($invid, Request $request)
    {
        $invoice = invoice::find($invid);
        $invoice->sida = $request->input('sida');
        $invoice->iLevel = $request->input('iLevel');
        $invoice->iCLevel = $request->input('iCLevel');
        $invoice->iSemester = $request->input('iSemester');
        $invoice->iCSemester = $request->input('iCSemester');
        $invoice->iFeesPayable = $request->input('iFeesPayable');
        $invoice->iCFeesPayable = $request->input('iCFeesPayable');
        $invoice->iTotal    = $request->input('iFeesPayable') + $request->input('iCFeesPayable');

        if($invoice->iTotal != 0)
            $invoice->status = ('Not cleared');
        else
            $invoice->status = ('Cleared');

        $status =  $invoice->save();  // saves the record

        if($status)
            $request->session()->flash('success','Invoice Updated successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/update-invoice/'. $invoice->invid);
    }


//----------------RESULT FUNCTIONS------------------
    /* Adding Results to the DB*/
    public function getAddResult()
    {
        return view( 'addResult');
    }

    /*Selecting a result from the DB*/
    public function getSelectResult()
    {
        $results = result::all();
        return view('selectResult',[
            'results' => $results
        ]);
    }

    /* Viewing all the results in the DB*/
    public function getViewResults()
    {
        $results = result::all();
        return view('viewResults',[
            'results' => $results
        ]);

    }

    public function getResultReport()
    {
        return view('resultReport');
    }

    public function getViewResult($rid)
    {
        $result = result::find($rid);
        return view('viewResult',[
            'result' => $result
        ]);
    }

    public function getViewResultPlain($rid)
    {
        $result = result::find($rid);
        return view('viewResultPlain',[
            'result' => $result
        ]);
    }

    /* Adding Results to the DB*/
    public function getPostResult(Request $request)
    {
        $result = new result();
        $result->sida = $request->input('sida');
        $result->rLevel1 = $request->input('rLevel1');
        $result->rSession1 = $request->input('rSession1');
        $result->rSemester11 = $request->input('rSemester11');

        $result->rCourseCode111 = $request->input('rCourseCode111');
        $result->rCourseName111 = $request->input('rCourseName111');
        $result->mark111 = $request->input('mark111');
        $result->grade111 = $request->input('grade111');
        $result->cr111 = $request->input('cr111');

        $result->rCourseCode112 = $request->input('rCourseCode112');
        $result->rCourseName112 = $request->input('rCourseName112');
        $result->mark112 = $request->input('mark112');
        $result->grade112 = $request->input('grade112');
        $result->cr112 = $request->input('cr112');

        $result->rCourseCode113 = $request->input('rCourseCode113');
        $result->rCourseName113 = $request->input('rCourseName113');
        $result->mark113 = $request->input('mark113');
        $result->grade113 = $request->input('grade113');
        $result->cr113 = $request->input('cr113');

        $result->rCourseCode114 = $request->input('rCourseCode114');
        $result->rCourseName114 = $request->input('rCourseName114');
        $result->mark114 = $request->input('mark114');
        $result->grade114 = $request->input('grade114');
        $result->cr114 = $request->input('cr114');

        $result->rCourseCode115 = $request->input('rCourseCode115');
        $result->rCourseName115 = $request->input('rCourseName115');
        $result->mark115 = $request->input('mark115');
        $result->grade115 = $request->input('grade115');
        $result->cr115 = $request->input('cr115');

        $result->rCourseCode116 = $request->input('rCourseCode116');
        $result->rCourseName116 = $request->input('rCourseName116');
        $result->mark116 = $request->input('mark116');
        $result->grade116 = $request->input('grade116');
        $result->cr116 = $request->input('cr116');

        $result->rSwa11 = $request->input('rSwa11');

        $result->rSemester12 = $request->input('rSemester12');

        $result->rCourseCode121 = $request->input('rCourseCode121');
        $result->rCourseName121 = $request->input('rCourseName121');
        $result->mark121 = $request->input('mark121');
        $result->grade121 = $request->input('grade121');
        $result->cr121 = $request->input('cr121');

        $result->rCourseCode122 = $request->input('rCourseCode122');
        $result->rCourseName122 = $request->input('rCourseName122');
        $result->mark122 = $request->input('mark122');
        $result->grade122 = $request->input('grade122');
        $result->cr122 = $request->input('cr122');

        $result->rCourseCode123 = $request->input('rCourseCode123');
        $result->rCourseName123 = $request->input('rCourseName123');
        $result->mark123 = $request->input('mark123');
        $result->grade123 = $request->input('grade123');
        $result->cr123 = $request->input('cr123');

        $result->rCourseCode124 = $request->input('rCourseCode124');
        $result->rCourseName124 = $request->input('rCourseName124');
        $result->mark124 = $request->input('mark124');
        $result->grade124 = $request->input('grade124');
        $result->cr124 = $request->input('cr124');

        $result->rCourseCode125 = $request->input('rCourseCode125');
        $result->rCourseName125 = $request->input('rCourseName125');
        $result->mark125 = $request->input('mark125');
        $result->grade125 = $request->input('grade125');
        $result->cr125 = $request->input('cr125');

        $result->rCourseCode126 = $request->input('rCourseCode126');
        $result->rCourseName126 = $request->input('rCourseName126');
        $result->mark126 = $request->input('mark126');
        $result->grade126 = $request->input('grade126');
        $result->cr126 = $request->input('cr126');

        $result->rSwa12 = $request->input('rSwa12');

        $result->rLevel2 = $request->input('rLevel2');
        $result->rSession2 = $request->input('rSession2');

        $result->rSemester21 = $request->input('rSemester21');

        $result->rCourseCode211 = $request->input('rCourseCode211');
        $result->rCourseName211 = $request->input('rCourseName211');
        $result->mark211 = $request->input('mark211');
        $result->grade211 = $request->input('grade211');
        $result->cr211 = $request->input('cr211');

        $result->rCourseCode212 = $request->input('rCourseCode212');
        $result->rCourseName212 = $request->input('rCourseName212');
        $result->mark212 = $request->input('mark212');
        $result->grade212 = $request->input('grade212');
        $result->cr212 = $request->input('cr212');

        $result->rCourseCode213 = $request->input('rCourseCode213');
        $result->rCourseName213 = $request->input('rCourseName213');
        $result->mark213 = $request->input('mark213');
        $result->grade213 = $request->input('grade213');
        $result->cr213 = $request->input('cr213');

        $result->rCourseCode214 = $request->input('rCourseCode214');
        $result->rCourseName214 = $request->input('rCourseName214');
        $result->mark214 = $request->input('mark214');
        $result->grade214 = $request->input('grade214');
        $result->cr214 = $request->input('cr214');

        $result->rCourseCode215 = $request->input('rCourseCode215');
        $result->rCourseName215 = $request->input('rCourseName215');
        $result->mark215 = $request->input('mark215');
        $result->grade215 = $request->input('grade215');
        $result->cr215 = $request->input('cr215');

        $result->rCourseCode216 = $request->input('rCourseCode216');
        $result->rCourseName216 = $request->input('rCourseName216');
        $result->mark216 = $request->input('mark216');
        $result->grade216 = $request->input('grade216');
        $result->cr216 = $request->input('cr216');

        $result->rSwa21 = $request->input('rSwa21');

        $result->rSemester22 = $request->input('rSemester22');

        $result->rCourseCode221 = $request->input('rCourseCode221');
        $result->rCourseName221 = $request->input('rCourseName221');
        $result->mark221 = $request->input('mark221');
        $result->grade221 = $request->input('grade221');
        $result->cr221 = $request->input('cr221');

        $result->rCourseCode222 = $request->input('rCourseCode222');
        $result->rCourseName222 = $request->input('rCourseName222');
        $result->mark222 = $request->input('mark222');
        $result->grade222 = $request->input('grade222');
        $result->cr222 = $request->input('cr222');

        $result->rCourseCode223 = $request->input('rCourseCode223');
        $result->rCourseName223 = $request->input('rCourseName223');
        $result->mark223 = $request->input('mark223');
        $result->grade223 = $request->input('grade223');
        $result->cr223 = $request->input('cr223');

        $result->rCourseCode224 = $request->input('rCourseCode224');
        $result->rCourseName224 = $request->input('rCourseName224');
        $result->mark224 = $request->input('mark224');
        $result->grade224 = $request->input('grade224');
        $result->cr224 = $request->input('cr224');

        $result->rCourseCode225 = $request->input('rCourseCode225');
        $result->rCourseName225 = $request->input('rCourseName225');
        $result->mark225 = $request->input('mark225');
        $result->grade225 = $request->input('grade225');
        $result->cr225 = $request->input('cr225');

        $result->rCourseCode226 = $request->input('rCourseCode226');
        $result->rCourseName226 = $request->input('rCourseName226');
        $result->mark226 = $request->input('mark226');
        $result->grade226 = $request->input('grade226');
        $result->cr226 = $request->input('cr226');

        $result->rSwa22 = $request->input('rSwa22');

        $result->rLevel3 = $request->input('rLevel3');
        $result->rSession3 = $request->input('rSession3');

        $result->rSemester31 = $request->input('rSemester31');

        $result->rCourseCode311 = $request->input('rCourseCode311');
        $result->rCourseName311 = $request->input('rCourseName311');
        $result->mark311 = $request->input('mark311');
        $result->grade311 = $request->input('grade311');
        $result->cr311 = $request->input('cr311');

        $result->rCourseCode312 = $request->input('rCourseCode312');
        $result->rCourseName312 = $request->input('rCourseName312');
        $result->mark312 = $request->input('mark312');
        $result->grade312 = $request->input('grade312');
        $result->cr312 = $request->input('cr312');

        $result->rCourseCode313 = $request->input('rCourseCode313');
        $result->rCourseName313 = $request->input('rCourseName313');
        $result->mark313 = $request->input('mark313');
        $result->grade313 = $request->input('grade313');
        $result->cr313 = $request->input('cr313');

        $result->rCourseCode314 = $request->input('rCourseCode314');
        $result->rCourseName314 = $request->input('rCourseName314');
        $result->mark314 = $request->input('mark314');
        $result->grade314 = $request->input('grade314');
        $result->cr314 = $request->input('cr314');

        $result->rCourseCode315 = $request->input('rCourseCode315');
        $result->rCourseName315 = $request->input('rCourseName315');
        $result->mark315 = $request->input('mark315');
        $result->grade315 = $request->input('grade315');
        $result->cr315 = $request->input('cr315');

        $result->rCourseCode316 = $request->input('rCourseCode316');
        $result->rCourseName316 = $request->input('rCourseName316');
        $result->mark316 = $request->input('mark316');
        $result->grade316 = $request->input('grade316');
        $result->cr316 = $request->input('cr316');

        $result->rSwa31 = $request->input('rSwa31');

        $result->rSemester32 = $request->input('rSemester32');

        $result->rCourseCode321 = $request->input('rCourseCode321');
        $result->rCourseName321 = $request->input('rCourseName321');
        $result->mark321 = $request->input('mark321');
        $result->grade321 = $request->input('grade321');
        $result->cr321 = $request->input('cr321');

        $result->rCourseCode322 = $request->input('rCourseCode322');
        $result->rCourseName322 = $request->input('rCourseName322');
        $result->mark322 = $request->input('mark322');
        $result->grade322 = $request->input('grade322');
        $result->cr322 = $request->input('cr322');

        $result->rCourseCode323 = $request->input('rCourseCode323');
        $result->rCourseName323 = $request->input('rCourseName323');
        $result->mark323 = $request->input('mark323');
        $result->grade323 = $request->input('grade323');
        $result->cr323 = $request->input('cr323');

        $result->rCourseCode324 = $request->input('rCourseCode324');
        $result->rCourseName324 = $request->input('rCourseName324');
        $result->mark324 = $request->input('mark324');
        $result->grade324 = $request->input('grade324');
        $result->cr324 = $request->input('cr324');

        $result->rCourseCode325 = $request->input('rCourseCode325');
        $result->rCourseName325 = $request->input('rCourseName325');
        $result->mark325 = $request->input('mark325');
        $result->grade325 = $request->input('grade325');
        $result->cr325 = $request->input('cr325');

        $result->rCourseCode326 = $request->input('rCourseCode326');
        $result->rCourseName326 = $request->input('rCourseName326');
        $result->mark326 = $request->input('mark326');
        $result->grade326 = $request->input('grade326');
        $result->cr326 = $request->input('cr326');

        $result->rSwa32 = $request->input('rSwa32');

        $result->rLevel4 = $request->input('rLevel4');
        $result->rSession4 = $request->input('rSession4');
        $result->rSemester41 = $request->input('rSemester41');

        $result->rCourseCode411 = $request->input('rCourseCode411');
        $result->rCourseName411 = $request->input('rCourseName411');
        $result->mark411 = $request->input('mark411');
        $result->grade411 = $request->input('grade411');
        $result->cr411 = $request->input('cr411');

        $result->rCourseCode412 = $request->input('rCourseCode412');
        $result->rCourseName412 = $request->input('rCourseName412');
        $result->mark412 = $request->input('mark412');
        $result->grade412 = $request->input('grade412');
        $result->cr412 = $request->input('cr412');

        $result->rCourseCode413 = $request->input('rCourseCode413');
        $result->rCourseName413 = $request->input('rCourseName413');
        $result->mark413 = $request->input('mark413');
        $result->grade413 = $request->input('grade413');
        $result->cr413 = $request->input('cr413');

        $result->rCourseCode414 = $request->input('rCourseCode414');
        $result->rCourseName414 = $request->input('rCourseName414');
        $result->mark414 = $request->input('mark414');
        $result->grade414 = $request->input('grade414');
        $result->cr414 = $request->input('cr414');

        $result->rCourseCode415 = $request->input('rCourseCode415');
        $result->rCourseName415 = $request->input('rCourseName415');
        $result->mark415 = $request->input('mark415');
        $result->grade415 = $request->input('grade415');
        $result->cr415 = $request->input('cr415');

        $result->rCourseCode416 = $request->input('rCourseCode416');
        $result->rCourseName416 = $request->input('rCourseName416');
        $result->mark416 = $request->input('mark416');
        $result->grade416 = $request->input('grade416');
        $result->cr416 = $request->input('cr416');

        $result->rSwa41 = $request->input('rSwa41');

        $result->rSemester42 = $request->input('rSemester42');

        $result->rCourseCode421 = $request->input('rCourseCode421');
        $result->rCourseName421 = $request->input('rCourseName421');
        $result->mark421 = $request->input('mark421');
        $result->grade421 = $request->input('grade421');
        $result->cr421 = $request->input('cr421');

        $result->rCourseCode422 = $request->input('rCourseCode422');
        $result->rCourseName422 = $request->input('rCourseName422');
        $result->mark422 = $request->input('mark422');
        $result->grade422 = $request->input('grade422');
        $result->cr422 = $request->input('cr422');

        $result->rCourseCode423 = $request->input('rCourseCode423');
        $result->rCourseName423 = $request->input('rCourseName423');
        $result->mark423 = $request->input('mark423');
        $result->grade423 = $request->input('grade423');
        $result->cr423 = $request->input('cr423');

        $result->rCourseCode424 = $request->input('rCourseCode424');
        $result->rCourseName424 = $request->input('rCourseName424');
        $result->mark424 = $request->input('mark424');
        $result->grade424 = $request->input('grade424');
        $result->cr424 = $request->input('cr424');

        $result->rCourseCode425 = $request->input('rCourseCode425');
        $result->rCourseName425 = $request->input('rCourseName425');
        $result->mark425 = $request->input('mark425');
        $result->grade425 = $request->input('grade425');
        $result->cr425 = $request->input('cr425');

        $result->rCourseCode426 = $request->input('rCourseCode426');
        $result->rCourseName426 = $request->input('rCourseName426');
        $result->mark426 = $request->input('mark426');
        $result->grade426 = $request->input('grade426');
        $result->cr426 = $request->input('cr426');

        $result->rSwa42 = $request->input('rSwa42');

        $result->rCwa    = $request->input('rCwa');
        $status =  $result->save();  // saves the record

        if($status)
            $request->session()->flash('success','Result added successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/add-result');
    }

    /*generating the result pdf*/
    public function getGenerateResultPdf($rid)
    {

        try {
            // instantiate and use the dompdf class
            $dompdf = new Dompdf();
            $dompdf->loadHtml($this->getViewResultPlain($rid));

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A3', 'portrait');
            $dompdf->set_option('isRemoteEnabled', 'true');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream("Student_Result_" . Carbon::now()->toDateString());
        }
        catch(Exception $e){
            echo $e->getMessage();
            echo "Something went wrong. Please try again.";
        }
    }

    public function postUpdateResult($rid,Request $request)
    {
        $result = result::find($rid);
        $result->sida = $request->input('sida');
        $result->rLevel1 = $request->input('rLevel1');
        $result->rSession1 = $request->input('rSession1');
        $result->rSemester11 = $request->input('rSemester11');

        $result->rCourseCode111 = $request->input('rCourseCode111');
        $result->rCourseName111 = $request->input('rCourseName111');
        $result->mark111 = $request->input('mark111');
        $result->grade111 = $request->input('grade111');
        $result->cr111 = $request->input('cr111');

        $result->rCourseCode112 = $request->input('rCourseCode112');
        $result->rCourseName112 = $request->input('rCourseName112');
        $result->mark112 = $request->input('mark112');
        $result->grade112 = $request->input('grade112');
        $result->cr112 = $request->input('cr112');

        $result->rCourseCode113 = $request->input('rCourseCode113');
        $result->rCourseName113 = $request->input('rCourseName113');
        $result->mark113 = $request->input('mark113');
        $result->grade113 = $request->input('grade113');
        $result->cr113 = $request->input('cr113');

        $result->rCourseCode114 = $request->input('rCourseCode114');
        $result->rCourseName114 = $request->input('rCourseName114');
        $result->mark114 = $request->input('mark114');
        $result->grade114 = $request->input('grade114');
        $result->cr114 = $request->input('cr114');

        $result->rCourseCode115 = $request->input('rCourseCode115');
        $result->rCourseName115 = $request->input('rCourseName115');
        $result->mark115 = $request->input('mark115');
        $result->grade115 = $request->input('grade115');
        $result->cr115 = $request->input('cr115');

        $result->rCourseCode116 = $request->input('rCourseCode116');
        $result->rCourseName116 = $request->input('rCourseName116');
        $result->mark116 = $request->input('mark116');
        $result->grade116 = $request->input('grade116');
        $result->cr116 = $request->input('cr116');

        $result->rSwa11 = $request->input('rSwa11');

        $result->rSemester12 = $request->input('rSemester12');

        $result->rCourseCode121 = $request->input('rCourseCode121');
        $result->rCourseName121 = $request->input('rCourseName121');
        $result->mark121 = $request->input('mark121');
        $result->grade121 = $request->input('grade121');
        $result->cr121 = $request->input('cr121');

        $result->rCourseCode122 = $request->input('rCourseCode122');
        $result->rCourseName122 = $request->input('rCourseName122');
        $result->mark122 = $request->input('mark122');
        $result->grade122 = $request->input('grade122');
        $result->cr122 = $request->input('cr122');

        $result->rCourseCode123 = $request->input('rCourseCode123');
        $result->rCourseName123 = $request->input('rCourseName123');
        $result->mark123 = $request->input('mark123');
        $result->grade123 = $request->input('grade123');
        $result->cr123 = $request->input('cr123');

        $result->rCourseCode124 = $request->input('rCourseCode124');
        $result->rCourseName124 = $request->input('rCourseName124');
        $result->mark124 = $request->input('mark124');
        $result->grade124 = $request->input('grade124');
        $result->cr124 = $request->input('cr124');

        $result->rCourseCode125 = $request->input('rCourseCode125');
        $result->rCourseName125 = $request->input('rCourseName125');
        $result->mark125 = $request->input('mark125');
        $result->grade125 = $request->input('grade125');
        $result->cr125 = $request->input('cr125');

        $result->rCourseCode126 = $request->input('rCourseCode126');
        $result->rCourseName126 = $request->input('rCourseName126');
        $result->mark126 = $request->input('mark126');
        $result->grade126 = $request->input('grade126');
        $result->cr126 = $request->input('cr126');

        $result->rSwa12 = $request->input('rSwa12');

        $result->rLevel2 = $request->input('rLevel2');
        $result->rSession2 = $request->input('rSession2');

        $result->rSemester21 = $request->input('rSemester21');

        $result->rCourseCode211 = $request->input('rCourseCode211');
        $result->rCourseName211 = $request->input('rCourseName211');
        $result->mark211 = $request->input('mark211');
        $result->grade211 = $request->input('grade211');
        $result->cr211 = $request->input('cr211');

        $result->rCourseCode212 = $request->input('rCourseCode212');
        $result->rCourseName212 = $request->input('rCourseName212');
        $result->mark212 = $request->input('mark212');
        $result->grade212 = $request->input('grade212');
        $result->cr212 = $request->input('cr212');

        $result->rCourseCode213 = $request->input('rCourseCode213');
        $result->rCourseName213 = $request->input('rCourseName213');
        $result->mark213 = $request->input('mark213');
        $result->grade213 = $request->input('grade213');
        $result->cr213 = $request->input('cr213');

        $result->rCourseCode214 = $request->input('rCourseCode214');
        $result->rCourseName214 = $request->input('rCourseName214');
        $result->mark214 = $request->input('mark214');
        $result->grade214 = $request->input('grade214');
        $result->cr214 = $request->input('cr214');

        $result->rCourseCode215 = $request->input('rCourseCode215');
        $result->rCourseName215 = $request->input('rCourseName215');
        $result->mark215 = $request->input('mark215');
        $result->grade215 = $request->input('grade215');
        $result->cr215 = $request->input('cr215');

        $result->rCourseCode216 = $request->input('rCourseCode216');
        $result->rCourseName216 = $request->input('rCourseName216');
        $result->mark216 = $request->input('mark216');
        $result->grade216 = $request->input('grade216');
        $result->cr216 = $request->input('cr216');

        $result->rSwa21 = $request->input('rSwa21');

        $result->rSemester22 = $request->input('rSemester22');

        $result->rCourseCode221 = $request->input('rCourseCode221');
        $result->rCourseName221 = $request->input('rCourseName221');
        $result->mark221 = $request->input('mark221');
        $result->grade221 = $request->input('grade221');
        $result->cr221 = $request->input('cr221');

        $result->rCourseCode222 = $request->input('rCourseCode222');
        $result->rCourseName222 = $request->input('rCourseName222');
        $result->mark222 = $request->input('mark222');
        $result->grade222 = $request->input('grade222');
        $result->cr222 = $request->input('cr222');

        $result->rCourseCode223 = $request->input('rCourseCode223');
        $result->rCourseName223 = $request->input('rCourseName223');
        $result->mark223 = $request->input('mark223');
        $result->grade223 = $request->input('grade223');
        $result->cr223 = $request->input('cr223');

        $result->rCourseCode224 = $request->input('rCourseCode224');
        $result->rCourseName224 = $request->input('rCourseName224');
        $result->mark224 = $request->input('mark224');
        $result->grade224 = $request->input('grade224');
        $result->cr224 = $request->input('cr224');

        $result->rCourseCode225 = $request->input('rCourseCode225');
        $result->rCourseName225 = $request->input('rCourseName225');
        $result->mark225 = $request->input('mark225');
        $result->grade225 = $request->input('grade225');
        $result->cr225 = $request->input('cr225');

        $result->rCourseCode226 = $request->input('rCourseCode226');
        $result->rCourseName226 = $request->input('rCourseName226');
        $result->mark226 = $request->input('mark226');
        $result->grade226 = $request->input('grade226');
        $result->cr226 = $request->input('cr226');

        $result->rSwa22 = $request->input('rSwa22');

        $result->rLevel3 = $request->input('rLevel3');
        $result->rSession3 = $request->input('rSession3');

        $result->rSemester31 = $request->input('rSemester31');

        $result->rCourseCode311 = $request->input('rCourseCode311');
        $result->rCourseName311 = $request->input('rCourseName311');
        $result->mark311 = $request->input('mark311');
        $result->grade311 = $request->input('grade311');
        $result->cr311 = $request->input('cr311');

        $result->rCourseCode312 = $request->input('rCourseCode312');
        $result->rCourseName312 = $request->input('rCourseName312');
        $result->mark312 = $request->input('mark312');
        $result->grade312 = $request->input('grade312');
        $result->cr312 = $request->input('cr312');

        $result->rCourseCode313 = $request->input('rCourseCode313');
        $result->rCourseName313 = $request->input('rCourseName313');
        $result->mark313 = $request->input('mark313');
        $result->grade313 = $request->input('grade313');
        $result->cr313 = $request->input('cr313');

        $result->rCourseCode314 = $request->input('rCourseCode314');
        $result->rCourseName314 = $request->input('rCourseName314');
        $result->mark314 = $request->input('mark314');
        $result->grade314 = $request->input('grade314');
        $result->cr314 = $request->input('cr314');

        $result->rCourseCode315 = $request->input('rCourseCode315');
        $result->rCourseName315 = $request->input('rCourseName315');
        $result->mark315 = $request->input('mark315');
        $result->grade315 = $request->input('grade315');
        $result->cr315 = $request->input('cr315');

        $result->rCourseCode316 = $request->input('rCourseCode316');
        $result->rCourseName316 = $request->input('rCourseName316');
        $result->mark316 = $request->input('mark316');
        $result->grade316 = $request->input('grade316');
        $result->cr316 = $request->input('cr316');

        $result->rSwa31 = $request->input('rSwa31');

        $result->rSemester32 = $request->input('rSemester32');

        $result->rCourseCode321 = $request->input('rCourseCode321');
        $result->rCourseName321 = $request->input('rCourseName321');
        $result->mark321 = $request->input('mark321');
        $result->grade321 = $request->input('grade321');
        $result->cr321 = $request->input('cr321');

        $result->rCourseCode322 = $request->input('rCourseCode322');
        $result->rCourseName322 = $request->input('rCourseName322');
        $result->mark322 = $request->input('mark322');
        $result->grade322 = $request->input('grade322');
        $result->cr322 = $request->input('cr322');

        $result->rCourseCode323 = $request->input('rCourseCode323');
        $result->rCourseName323 = $request->input('rCourseName323');
        $result->mark323 = $request->input('mark323');
        $result->grade323 = $request->input('grade323');
        $result->cr323 = $request->input('cr323');

        $result->rCourseCode324 = $request->input('rCourseCode324');
        $result->rCourseName324 = $request->input('rCourseName324');
        $result->mark324 = $request->input('mark324');
        $result->grade324 = $request->input('grade324');
        $result->cr324 = $request->input('cr324');

        $result->rCourseCode325 = $request->input('rCourseCode325');
        $result->rCourseName325 = $request->input('rCourseName325');
        $result->mark325 = $request->input('mark325');
        $result->grade325 = $request->input('grade325');
        $result->cr325 = $request->input('cr325');

        $result->rCourseCode326 = $request->input('rCourseCode326');
        $result->rCourseName326 = $request->input('rCourseName326');
        $result->mark326 = $request->input('mark326');
        $result->grade326 = $request->input('grade326');
        $result->cr326 = $request->input('cr326');

        $result->rSwa32 = $request->input('rSwa32');

        $result->rLevel4 = $request->input('rLevel4');
        $result->rSession4 = $request->input('rSession4');
        $result->rSemester41 = $request->input('rSemester41');

        $result->rCourseCode411 = $request->input('rCourseCode411');
        $result->rCourseName411 = $request->input('rCourseName411');
        $result->mark411 = $request->input('mark411');
        $result->grade411 = $request->input('grade411');
        $result->cr411 = $request->input('cr411');

        $result->rCourseCode412 = $request->input('rCourseCode412');
        $result->rCourseName412 = $request->input('rCourseName412');
        $result->mark412 = $request->input('mark412');
        $result->grade412 = $request->input('grade412');
        $result->cr412 = $request->input('cr412');

        $result->rCourseCode413 = $request->input('rCourseCode413');
        $result->rCourseName413 = $request->input('rCourseName413');
        $result->mark413 = $request->input('mark413');
        $result->grade413 = $request->input('grade413');
        $result->cr413 = $request->input('cr413');

        $result->rCourseCode414 = $request->input('rCourseCode414');
        $result->rCourseName414 = $request->input('rCourseName414');
        $result->mark414 = $request->input('mark414');
        $result->grade414 = $request->input('grade414');
        $result->cr414 = $request->input('cr414');

        $result->rCourseCode415 = $request->input('rCourseCode415');
        $result->rCourseName415 = $request->input('rCourseName415');
        $result->mark415 = $request->input('mark415');
        $result->grade415 = $request->input('grade415');
        $result->cr415 = $request->input('cr415');

        $result->rCourseCode416 = $request->input('rCourseCode416');
        $result->rCourseName416 = $request->input('rCourseName416');
        $result->mark416 = $request->input('mark416');
        $result->grade416 = $request->input('grade416');
        $result->cr416 = $request->input('cr416');

        $result->rSwa41 = $request->input('rSwa41');

        $result->rSemester42 = $request->input('rSemester42');

        $result->rCourseCode421 = $request->input('rCourseCode421');
        $result->rCourseName421 = $request->input('rCourseName421');
        $result->mark421 = $request->input('mark421');
        $result->grade421 = $request->input('grade421');
        $result->cr421 = $request->input('cr421');

        $result->rCourseCode422 = $request->input('rCourseCode422');
        $result->rCourseName422 = $request->input('rCourseName422');
        $result->mark422 = $request->input('mark422');
        $result->grade422 = $request->input('grade422');
        $result->cr422 = $request->input('cr422');

        $result->rCourseCode423 = $request->input('rCourseCode423');
        $result->rCourseName423 = $request->input('rCourseName423');
        $result->mark423 = $request->input('mark423');
        $result->grade423 = $request->input('grade423');
        $result->cr423 = $request->input('cr423');

        $result->rCourseCode424 = $request->input('rCourseCode424');
        $result->rCourseName424 = $request->input('rCourseName424');
        $result->mark424 = $request->input('mark424');
        $result->grade424 = $request->input('grade424');
        $result->cr424 = $request->input('cr424');

        $result->rCourseCode425 = $request->input('rCourseCode425');
        $result->rCourseName425 = $request->input('rCourseName425');
        $result->mark425 = $request->input('mark425');
        $result->grade425 = $request->input('grade425');
        $result->cr425 = $request->input('cr425');

        $result->rCourseCode426 = $request->input('rCourseCode426');
        $result->rCourseName426 = $request->input('rCourseName426');
        $result->mark426 = $request->input('mark426');
        $result->grade426 = $request->input('grade426');
        $result->cr426 = $request->input('cr426');

        $result->rSwa42 = $request->input('rSwa42');

        $result->rCwa    = $request->input('rCwa');
        $status =  $result->save();  // saves the record

        if($status)
            $request->session()->flash('success','Result Updated successfully');
        else
            $request->session()->flash('error','Something went wrong. Please try again');

        return redirect('/update-result/'. $result->rid );
    }

//----------------POST FUNCTIONS------------------


    public function getUpdateStudent($sida)
    {
        $students = student::find($sida);
        return view('updateStudent',[
            'student' => $students
        ]);
    }

    public function getUpdateInvoice($invid)
    {
        $invoices = invoice::find($invid);
        return view('updateInvoice',[
            'invoice' => $invoices
        ]);
    }

    public function getUpdateTimetable($tid)
    {
        $timetables = timetable::find($tid);
        return view('updateTimetable',[
            'timetable' => $timetables
        ]);
    }

    public function getUpdateResult($rid)
    {
        $results = result::find($rid);
        return view('updateResult',[
            'result' => $results
        ]);
    }

}
