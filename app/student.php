<?php

namespace PORTRAY;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    //primary key -- sida

    protected $primaryKey = 'sida';

    public $table = 'student';


    public function student ()
    {
        return $this->hasOne('result');
        return $this->hasOne('invoice');
        return $this->belongsTo('PORTRAY\timetable','tid');
    }

}
