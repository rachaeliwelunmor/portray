<?php

namespace PORTRAY;

use Illuminate\Database\Eloquent\Model;

class lecturer extends Model
{
    //primary key -- lid
    protected  $primaryKey = 'lid';

    public $table = 'lecturer';
}
